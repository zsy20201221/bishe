#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/lhash.h>

typedef struct Student_st
{
    char name[20];
    int age;
    char otherInfo[200];
} Student;

static int Student_cmp(const void *a, const void *b)
{
    char *namea = ((Student *)a)->name;
    char *nameb = ((Student *)b)->name;
    return strcmp(namea, nameb);
}

static void PrintValue(Student *a)
{
    printf("name: %s\n", a->name);
    printf("age: %d\n", a->age);
    printf("otherInfo: %s\n", a->otherInfo);
}

static void PrintValue_arg(Student *a, void *b)
{
    int flag = *(int *)b;
    printf("用户输入参数为: %d\n", flag);
    printf("name: %s\n", a->name);
    printf("age: %d\n", a->age);
    printf("otherInfo: %s\n", a->otherInfo);
}

int init_suite(void)
{
    // 初始化测试套件
    return 0;
}

int clean_suite(void)
{
    // 清理测试套件
    return 0;
}

void testProcessData()
{
    OPENSSL_LHASH *h;
    Student s1 = {"zcp", 28, "hu bei"};
    Student s2 = {"forxy", 28, "no info"};
    Student s3 = {"skp", 24, "student"};
    Student s4 = {"zhao_zcp", 28, "zcp's name"};
    Student *s5;
    void *data;

    h = lh_new(NULL, Student_cmp);
    if (h == NULL)
    {
        printf("无法创建哈希表");
        return;
    }

    data = &s1;
    lh_insert(h, data);
    data = &s2;
    lh_insert(h, data);
    data = &s3;
    lh_insert(h, data);
    data = &s4;
    lh_insert(h, data);

    // 在这里添加适当的断言来验证函数的行为和输出

    // 示例断言：验证哈希表中的元素数量是否等于预期值
    CU_ASSERT_EQUAL(lh_num_items(h), 4);

    // 示例断言：验证查找到的元素与预期值是否匹配
    data = lh_retrieve(h, (const void *)"skp");
    CU_ASSERT_PTR_NOT_NULL(data);
    s5 = (Student *)data;
    CU_ASSERT_STRING_EQUAL(s5->name, "skp");
    CU_ASSERT_EQUAL(s5->age, 24);
    CU_ASSERT_STRING_EQUAL(s5->otherInfo, "student");

    // 如果有错误，使用 CUnit 的错误消息输出
    if (CU_get_number_of_failures() > 0) {
        printf("测试失败");
    }

    lh_free(h);
}

int main()
{
    CU_pSuite suite = NULL;

    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // 创建一个测试套件
    suite = CU_add_suite("ProcessDataSuite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 添加测试用例到套件中
    if (CU_add_test(suite, "processData Test", testProcessData) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 使用基本的 CUnit 运行测试
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 相关资源
    CU_cleanup_registry();

    return CU_get_error();
}
