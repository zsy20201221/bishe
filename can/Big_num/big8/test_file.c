#include <CUnit/Basic.h>
#include <stdio.h>



void testSubtractAndPrintResult() {
    const char* hexA = "100";
    const char* hexB = "32";

    // 假设期望的输出是 "bn 100 - 32 = 0x6e"
    // 你可能需要根据你的实际实现进行调整
    const char* expectedOutput = "bn 100 - 32 = 0x6e\n";

    // 重定向 stdout 到文件
    FILE* fp = freopen("test_output.txt", "w", stdout);

    // 调用函数
    int result = subtractAndPrintResult(hexA, hexB);

    // 关闭重定向
    fclose(fp);

    // 从文件中读取实际的输出
    fp = fopen("test_output.txt", "r");
    if (!fp) {
        CU_FAIL("无法打开测试输出文件。");
        return;
    }

    // 读取整个文件内容
    fseek(fp, 0, SEEK_END);
    long fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* actualOutput = malloc(fsize + 1);
    fread(actualOutput, 1, fsize, fp);
    fclose(fp);

    // 添加字符串结尾标志
    actualOutput[fsize] = '\0';

    // 检查实际输出是否与期望相符
    CU_ASSERT_STRING_EQUAL(actualOutput, expectedOutput);

    // 清理
    free(actualOutput);
}

int main() {
    // 初始化测试注册表
    CU_initialize_registry();

    // 向注册表添加一个测试套件
    CU_pSuite suite = CU_add_suite("SubtractAndPrintResult Suite", NULL, NULL);

    // 将测试用例添加到套件中
    CU_add_test(suite, "testSubtractAndPrintResult", testSubtractAndPrintResult);

    // 运行测试
    CU_basic_set_mode(CU_BRM_VERBOSE); // 设置为详细模式
    CU_basic_run_tests();

    // 清理
    CU_cleanup_registry();

    return CU_get_error();
}

