#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int subtractAndPrintResult(const char* hexA, const char* hexB) {
    BIGNUM *a, *b, *sub;
    BIO *out;
    int ret;

    a = BN_new();
    ret = BN_hex2bn(&a, hexA);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        return -1;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, hexB);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a); // Free allocated memory before returning
        return -1;
    }

    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        return -1;
    }

    sub = BN_new();
    ret = BN_sub(sub, a, b);
    if (ret != 1) {
        printf("Error subtracting BIGNUMs.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        return -1;
    }

    BIO_puts(out, "bn ");
    BIO_puts(out, hexA);
    BIO_puts(out, " - ");
    BIO_puts(out, hexB);
    BIO_puts(out, " = 0x");
    BN_print(out, sub);
    BIO_puts(out, "\n");

    // Free allocated memory
    BN_free(a);
    BN_free(b);
    BN_free(sub);
    BIO_free(out);

    return 0;
}

/*int main() {
    const char* hexA = "100";
    const char* hexB = "32";

    int result = subtractAndPrintResult(hexA, hexB);
    return result;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int main() {
    BIGNUM *a, *b, *sub;
    BIO *out;
    char c[20], d[20];
    int ret;

    a = BN_new();
    strcpy(c, "100");
    ret = BN_hex2bn(&a, c);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        return -1;
    }

    b = BN_new();
    strcpy(d, "32");
    ret = BN_hex2bn(&b, d);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a); // Free allocated memory before exiting
        return -1;
    }

    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        return -1;
    }

    sub = BN_new();
    ret = BN_sub(sub, a, b);
    if (ret != 1) {
        printf("Error subtracting BIGNUMs.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        return -1;
    }

    BIO_puts(out, "bn 0x100 - 0x32 = 0x");
    BN_print(out, sub);
    BIO_puts(out, "\n");

    // Free allocated memory
    BN_free(a);
    BN_free(b);
    BN_free(sub);
    BIO_free(out);

    return 0;
}*/

/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *a,*b,*sub;
BIO *out;
char c[20],d[20];
int ret;
a=BN_new();
strcpy(c,"100");
ret=BN_hex2bn(&a,c);
b=BN_new();
strcpy(d,"32");
ret=BN_hex2bn(&b,d);
out=BIO_new(BIO_s_file());
ret=BIO_set_fp(out,stdout,BIO_NOCLOSE);
sub=BN_new();
ret=BN_sub(sub,a,b);
if(ret!=1)
{
printf("err.\n");
return -1;
}
BIO_puts(out,"bn 0x100 - 0x32 = 0x");
BN_print(out,sub);
BIO_puts(out,"\n");
BN_free(a);
BN_free(b);
BN_free(sub);
BIO_free(out);
return 0;
}
*/
