#include <openssl/bn.h>
#include <openssl/crypto.h>
#include <stdio.h>

void performBNOperations() {
    BIGNUM *ret1 = NULL;
    char *p = NULL;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    p = BN_bn2dec(ret1);
    printf("%s\n", p);

    BN_free(ret1);
    OPENSSL_free(p);
}

/*int main() {
    performBNOperations();

    getchar();
    return 0;
}*/
/*#include <openssl/bn.h>
#include <openssl/crypto.h>
#include <stdio.h>

int main()
{
    BIGNUM *ret1 = NULL;
    char *p = NULL;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    p = BN_bn2dec(ret1);
    printf("%s\n", p);

    BN_free(ret1);
    OPENSSL_free(p);

    getchar();
    return 0;
}*/
/*#include <openssl/bn.h>
#include <openssl/crypto.h>
int main()
{
BIGNUM *ret1=NULL;
char *p=NULL;
int len=0;
ret1=BN_bin2bn("242424ab",8, NULL);
p=BN_bn2dec(ret1);
printf("%s\n",p); // 3617571600447332706 
BN_free(ret1);
OPENSSL_free(p);
getchar();
return 0;
}
*/
