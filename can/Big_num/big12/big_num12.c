#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

void performExponentiation(const char* hexValueA, const char* hexValueB) {
    BIGNUM *a, *exp, *b;
    BN_CTX *ctx;
    BIO *out;
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, hexValueA);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, hexValueB);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return;
    }

    exp = BN_new();
    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        BN_CTX_free(ctx);
        return;
    }

    ret = BN_exp(exp, a, b, ctx);
    if (ret != 1) {
        printf("Error performing exponentiation.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        BN_CTX_free(ctx);
        return;
    }

    BIO_puts(out, "bn ");
    BIO_puts(out, hexValueA);
    BIO_puts(out, " exp ");
    BIO_puts(out, hexValueB);
    BIO_puts(out, " = 0x");
    BN_print(out, exp);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(b);
    BN_free(exp);
    BIO_free(out);
    BN_CTX_free(ctx);
}

/*int main() {
    performExponentiation("100", "3");
    return 0;
}*/

/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int main() {
    BIGNUM *a, *exp, *b;
    BN_CTX *ctx;
    BIO *out;
    char c[] = "100";
    char d[] = "3";
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return -1;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, c);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return -1;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, d);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return -1;
    }

    exp = BN_new();
    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        BN_CTX_free(ctx);
        return -1;
    }

    ret = BN_exp(exp, a, b, ctx);
    if (ret != 1) {
        printf("Error performing exponentiation.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        BN_CTX_free(ctx);
        return -1;
    }

    BIO_puts(out, "bn 0x100 exp 0x3 = 0x");
    BN_print(out, exp);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(b);
    BN_free(exp);
    BIO_free(out);
    BN_CTX_free(ctx);

    return 0;
}
*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *a,*exp,*b;
BN_CTX*ctx;
BIO *out;
char c[20],d[20];
int ret;
ctx=BN_CTX_new();
a=BN_new();
strcpy(c,"100");
ret=BN_hex2bn(&a,c);
b=BN_new();
strcpy(d,"3");
ret=BN_hex2bn(&b,d);
exp=BN_new();
out=BIO_new(BIO_s_file());
ret=BIO_set_fp(out,stdout,BIO_NOCLOSE);
ret=BN_exp(exp,a,b,ctx);
if(ret!=1)
{
printf("err.\n");
return -1;
}
BIO_puts(out,"bn 0x100 exp 0x3 =0x");
BN_print(out,exp);
BIO_puts(out,"\n");
BN_free(a);
BN_free(b);
BN_free(exp);
BIO_free(out);
BN_CTX_free(ctx);
return 0;
}*/
