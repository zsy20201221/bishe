// test_exponentiation.c
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>  // 添加这行以包含 FILE 的定义


// 声明测试函数
void testPerformExponentiation();

// 注册测试套件和测试用例
int main() {
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("Suite", NULL, NULL);
    CU_add_test(suite, "testPerformExponentiation", testPerformExponentiation);

    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}


// 编写测试函数
void testPerformExponentiation() {
    // 提供测试输入
    const char* hexValueA1 = "100";
    const char* hexValueB1 = "3";

    // 保存原始的 stdout，以便在测试后进行还原
    FILE* original_stdout = stdout;

    // 创建一个临时文件用于捕获输出
    FILE* tmp_stdout = tmpfile();
    if (tmp_stdout == NULL) {
        perror("Error creating temporary file for stdout");
        return;
    }

    // 将 stdout 重定向到临时文件
    stdout = tmp_stdout;

    // 调用被测试的函数，并执行测试用例
    performExponentiation(hexValueA1, hexValueB1);

    // 将 stdout 还原为原始 stdout
    stdout = original_stdout;

    // 回到文件开头，以便读取文件内容
    rewind(tmp_stdout);

    // 在这里添加断言
    // 例如，使用 CU_ASSERT_STRING_EQUAL 来比较预期的输出和实际输出
    char buffer[256];
    fgets(buffer, sizeof(buffer), tmp_stdout);
    CU_ASSERT_STRING_EQUAL(buffer, "bn 100 exp 3 = 0x1000000\n");

    // 关闭临时文件
    fclose(tmp_stdout);
}

