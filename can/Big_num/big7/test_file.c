#include <CUnit/Basic.h>
#include <openssl/bn.h>
#include <string.h>



void testPerformBNAddition() {
    // 创建一个临时文件对象
    FILE* tempFile = tmpfile();
    if (tempFile == NULL) {
        perror("Error creating temporary file");
        CU_FAIL("Error creating temporary file");
        return;
    }

    // 将文件对象传递给 BIGNUM 相加函数
    performBNAddition("10", "20", tempFile);  // 用你的测试值替换 "10" 和 "20"
    // 根据 performBNAddition 函数的预期输出添加断言

    // 关闭并删除临时文件
    fclose(tempFile);

    // ... 根据需要添加更多测试用例
}

int main() {
    // 初始化测试注册表
    CU_initialize_registry();

    // 向注册表添加一个测试套件
    CU_pSuite suite = CU_add_suite("BN Addition Suite", NULL, NULL);

    // 将测试用例添加到套件中
    CU_add_test(suite, "testPerformBNAddition", testPerformBNAddition);

    // 运行测试
    CU_basic_run_tests();

    // 清理
    CU_cleanup_registry();

    return CU_get_error();
}

