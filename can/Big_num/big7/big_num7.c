#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

void performBNAddition(const char* input1, const char* input2, BIO* out) {
    BIGNUM *a, *b, *add;
    int ret;

    a = BN_new();
    ret = BN_hex2bn(&a, input1);
    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        BIO_puts(out, "Error converting input1 to BIGNUM.\n");
        return;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, input2);
    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        BN_free(b);
        BIO_puts(out, "Error converting input2 to BIGNUM.\n");
        return;
    }

    add = BN_new();
    ret = BN_add(add, a, b);
    if (ret != 1) {
        // 加法操作失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        BN_free(b);
        BN_free(add);
        BIO_puts(out, "Error performing addition.\n");
        return;
    }

    BIO_puts(out, "bn ");
    BN_print(out, a);
    BIO_puts(out, " + ");
    BN_print(out, b);
    BIO_puts(out, " = ");
    BN_print(out, add);
    BIO_puts(out, "\n");

    BN_free(a);
    BN_free(b);
    BN_free(add);
}

/*int main() {
    char input1[256];
    char input2[256];
    BIO *out = BIO_new_fp(stdout, BIO_NOCLOSE);

    // 获取用户输入
    printf("Enter input1: ");
    fgets(input1, sizeof(input1), stdin);
    input1[strcspn(input1, "\n")] = '\0';  // 去掉换行符

    printf("Enter input2: ");
    fgets(input2, sizeof(input2), stdin);
    input2[strcspn(input2, "\n")] = '\0';  // 去掉换行符

    // 执行大数相加操作
    performBNAddition(input1, input2, out);

    BIO_free(out);
    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

void performBNAddition(const char* input1, const char* input2) {
    BIGNUM *a, *b, *add;
    BIO *out;
    int ret;

    a = BN_new();
    ret = BN_hex2bn(&a, input1);
    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        return;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, input2);
    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        BN_free(b);
        return;
    }

    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);

    add = BN_new();
    ret = BN_add(add, a, b);
    if (ret != 1) {
        // 加法操作失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(a);
        BN_free(b);
        BN_free(add);
        BIO_free(out);
        return;
    }

    BIO_puts(out, "bn ");
    BN_print(out, a);
    BIO_puts(out, " + ");
    BN_print(out, b);
    BIO_puts(out, " = ");
    BN_print(out, add);
    BIO_puts(out, "\n");

    BN_free(a);
    BN_free(b);
    BN_free(add);
    BIO_free(out);
}

int main() {
    const char* input1 = "32";
    const char* input2 = "100";
    performBNAddition(input1, input2);

    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *a,*b,*add;
BIO *out;
char c[20],d[20];
int ret;
a=BN_new();
strcpy(c,"32");
ret=BN_hex2bn(&a,c);
b=BN_new();
strcpy(d,"100");
ret=BN_hex2bn(&b,d);
out=BIO_new(BIO_s_file());
ret=BIO_set_fp(out,stdout,BIO_NOCLOSE);
add=BN_new();
ret=BN_add(add,a,b);
if(ret!=1)
{
printf("err.\n");
return -1;
}
BIO_puts(out,"bn 0x32 + 0x100 = 0x");
BN_print(out,add);
BIO_puts(out,"\n");
BN_free(a);
BN_free(b);
BN_free(add);
BIO_free(out);
return 0;
}*/
