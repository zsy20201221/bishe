#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int squareHexNumber(const char* hexString) {
    BIGNUM *a, *sqr;
    BN_CTX *ctx;
    BIO *out;
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return -1;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, hexString);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return -1;
    }

    sqr = BN_new();
    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return -1;
    }

    ret = BN_sqr(sqr, a, ctx);
    if (ret != 1) {
        printf("Error squaring BIGNUM.\n");
        BN_free(a);
        BIO_free(out);
        BN_CTX_free(ctx);
        return -1;
    }

    BIO_puts(out, "bn ");
    BIO_puts(out, hexString);
    BIO_puts(out, " sqr = 0x");
    BN_print(out, sqr);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(sqr);
    BIO_free(out);
    BN_CTX_free(ctx);

    return 0;
}

/*int main() {
    const char hexString[] = "100";

    int result = squareHexNumber(hexString);
    if (result != 0) {
        printf("Error squaring hex number.\n");
        return -1;
    }

    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int main() {
    BIGNUM *a, *sqr;
    BN_CTX *ctx;
    BIO *out;
    char c[] = "100";
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return -1;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, c);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return -1;
    }

    sqr = BN_new();
    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return -1;
    }

    ret = BN_sqr(sqr, a, ctx);
    if (ret != 1) {
        printf("Error squaring BIGNUM.\n");
        BN_free(a);
        BIO_free(out);
        BN_CTX_free(ctx);
        return -1;
    }

    BIO_puts(out, "bn 0x100 sqr = 0x");
    BN_print(out, sqr);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(sqr);
    BIO_free(out);
    BN_CTX_free(ctx);

    return 0;
}
*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *a,*sqr;
BN_CTX*ctx;
BIO *out;
char c[20];
int ret;
ctx=BN_CTX_new();
a=BN_new();
strcpy(c,"100");
ret=BN_hex2bn(&a,c);
sqr=BN_new();
out=BIO_new(BIO_s_file());
ret=BIO_set_fp(out,stdout,BIO_NOCLOSE);
ret=BN_sqr(sqr,a,ctx);
if(ret!=1)
{
printf("err.\n");
return -1;
}
BIO_puts(out,"bn 0x100 sqr =0x");
BN_print(out,sqr);
BIO_puts(out,"\n");
BN_free(a);
BN_free(sqr);
BIO_free(out);
BN_CTX_free(ctx);
return 0;
}*/

