// test_divide_hex_numbers.c
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>


// 声明测试函数
void testDivideHexNumbers();

// 注册测试套件和测试用例
int main() {
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("Suite", NULL, NULL);
    CU_add_test(suite, "testDivideHexNumbers", testDivideHexNumbers);

    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}

// 编写测试函数
void testDivideHexNumbers() {
    // 提供测试输入
    const char* hexString1 = "100";
    const char* hexString2 = "17";

    // 调用被测试的函数
    int result = divideHexNumbers(hexString1, hexString2);

    // 在这里添加你的断言
    // 例如，使用 CU_ASSERT_EQUAL 来比较预期值和实际值
    CU_ASSERT_EQUAL(result, 0);
}

