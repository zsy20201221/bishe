#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int divideHexNumbers(const char* hexString1, const char* hexString2) {
    BIGNUM *a, *b, *div, *rem;
    BN_CTX *ctx;
    BIO *out;
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return -1;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, hexString1);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return -1;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, hexString2);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return -1;
    }

    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        BN_CTX_free(ctx);
        return -1;
    }

    div = BN_new();
    rem = BN_new();
    ret = BN_div(div, rem, a, b, ctx);
    if (ret != 1) {
        printf("Error dividing BIGNUMs.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        BN_CTX_free(ctx);
        return -1;
    }

    BIO_puts(out, "bn ");
    BIO_puts(out, hexString1);
    BIO_puts(out, " / ");
    BIO_puts(out, hexString2);
    BIO_puts(out, " = 0x");
    BN_print(out, div);
    BIO_puts(out, "\n");

    BIO_puts(out, "bn ");
    BIO_puts(out, hexString1);
    BIO_puts(out, " % ");
    BIO_puts(out, hexString2);
    BIO_puts(out, " = 0x");
    BN_print(out, rem);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(b);
    BN_free(div);
    BN_free(rem);
    BIO_free(out);
    BN_CTX_free(ctx);

    return 0;
}

/*int main() {
    const char hexString1[] = "100";
    const char hexString2[] = "17";

    int result = divideHexNumbers(hexString1, hexString2);
    if (result != 0) {
        printf("Error dividing hex numbers.\n");
        return -1;
    }

    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int main() {
    BIGNUM *a, *b, *div, *rem;
    BN_CTX *ctx;
    BIO *out;
    char c[] = "100";
    char d[] = "17";
    int ret;

    ctx = BN_CTX_new();
    if (ctx == NULL) {
        printf("Error creating BN_CTX.\n");
        return -1;
    }

    a = BN_new();
    ret = BN_hex2bn(&a, c);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for a.\n");
        BN_CTX_free(ctx);
        return -1;
    }

    b = BN_new();
    ret = BN_hex2bn(&b, d);
    if (ret == 0) {
        printf("Error converting hex to BIGNUM for b.\n");
        BN_free(a);
        BN_CTX_free(ctx);
        return -1;
    }

    out = BIO_new(BIO_s_file());
    ret = BIO_set_fp(out, stdout, BIO_NOCLOSE);
    if (ret != 1) {
        printf("Error setting output file.\n");
        BN_free(a);
        BN_free(b);
        BN_CTX_free(ctx);
        return -1;
    }

    div = BN_new();
    rem = BN_new();
    ret = BN_div(div, rem, a, b, ctx);
    if (ret != 1) {
        printf("Error dividing BIGNUMs.\n");
        BN_free(a);
        BN_free(b);
        BIO_free(out);
        BN_CTX_free(ctx);
        return -1;
    }

    BIO_puts(out, "bn 0x100 / 0x17 = 0x");
    BN_print(out, div);
    BIO_puts(out, "\n");
    BIO_puts(out, "bn 0x100 % 0x17 = 0x");
    BN_print(out, rem);
    BIO_puts(out, "\n");

    // Free allocated memory in reverse order
    BN_free(a);
    BN_free(b);
    BN_free(div);
    BN_free(rem);
    BIO_free(out);
    BN_CTX_free(ctx);

    return 0;
}
*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *a,*b,*div,*rem;
BN_CTX*ctx;
BIO *out;
char c[20],d[20];
int ret;
ctx=BN_CTX_new();
a=BN_new();
strcpy(c,"100");
ret=BN_hex2bn(&a,c);
b=BN_new();
strcpy(d,"17");
ret=BN_hex2bn(&b,d);
out=BIO_new(BIO_s_file());
ret=BIO_set_fp(out,stdout,BIO_NOCLOSE);
div=BN_new();
rem=BN_new();
ret=BN_div(div,rem,a,b,ctx);
if(ret!=1)
{
printf("err.\n");
return -1;
}
BIO_puts(out,"bn 0x100 / 0x17 =0x");
BN_print(out,div);
BIO_puts(out,"\n");
BIO_puts(out,"bn 0x100 % 0x17 =0x");
BN_print(out,rem);
BIO_puts(out,"\n");
BN_free(a);
BN_free(b);
BN_free(div);
BN_free(rem);
BIO_free(out);
BN_CTX_free(ctx);
return 0;
}*/
