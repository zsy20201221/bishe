#include <openssl/bn.h>
#include <stdio.h>
#include <CUnit/Basic.h>

void test_performBNOperations()
{
    BIGNUM *a = BN_new();

    CU_ASSERT_PTR_NOT_NULL(a);

    int ret;
    BN_ULONG w;

    BN_one(a);
    w = 2685550010;
    ret = BN_add_word(a, w);

    CU_ASSERT_EQUAL(ret, 1);

    // 进行其他断言，例如验证预期的结果

    // 释放资源
    if (a != NULL) {
        BN_free(a);
    }
}

int main()
{
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("BNOperationsSuite", NULL, NULL);
    CU_add_test(suite, "test_performBNOperations", test_performBNOperations);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}
