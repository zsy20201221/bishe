#include <openssl/bn.h>
#include <stdio.h>

int performBNOperations(BIGNUM *a) {
    int ret;
    BN_ULONG w;

    BN_one(a);
    w = 2685550010;
    ret = BN_add_word(a, w);

    if (ret != 1) {
        printf("BN_add_word failed!\n");
        return -1;
    }

    // 输出结果
    printf("Result: ");
    BN_print_fp(stdout, a);
    printf("\n");

    return 0;
}

/*int main() {
    BIGNUM *a = BN_new();

    if (a == NULL) {
        printf("BN_new failed!\n");
        return -1;
    }

    int result = performBNOperations(a);

    if (a != NULL) {
        BN_free(a);
    }

    return result;
}*/
/*#include <openssl/bn.h>
#include <stdio.h>

int main() {
    int ret;
    BIGNUM *a;
    BN_ULONG w;
    
    // 分配内存并检查分配是否成功
    if ((a = BN_new()) == NULL) {
        printf("BN_new failed!\n");
        return -1;
    }

    BN_one(a);
    w = 2685550010;
    ret = BN_add_word(a, w);

    if (ret != 1) {
        printf("BN_add_word failed!\n");
        // 不要在这里释放 a，因为它是由 BN_new 分配的内存
        // BN_free(a);
        return -1;
    }

    // 注意：不要在这里释放 a，因为 a 可能被 BN_add_word 内部使用

    // 在这里检查 a 是否为 NULL，然后释放
    if (a != NULL) {
        BN_free(a);
    }

    return 0;
}
*/
/*#include <openssl/bn.h>
int main()
{
int ret;
BIGNUM *a;
BN_ULONG w;
a=BN_new();
BN_one(a);
w=2685550010;
ret=BN_add_word(a,w);
if(ret!=1)
{
printf("a+=w err!\n");
BN_free(a);
return -1;
}
BN_free(a);
return 0;
}*/
