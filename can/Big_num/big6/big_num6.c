#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

void performBNOperations(const char* input) {
    BIGNUM *bn;
    BIO *b;
    int ret;

    bn = BN_new();
    ret = BN_hex2bn(&bn, input);

    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(bn);
        return;
    }

    b = BIO_new(BIO_s_file());
    ret = BIO_set_fp(b, stdout, BIO_NOCLOSE);
    BIO_write(b, "aaa", 3);
    BN_print(b, bn);

    BN_free(bn);
    BIO_free(b);
}

/*int main()
{
    const char* input = "32";
    performBNOperations(input);

    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>

int main()
{
    BIGNUM *bn;
    BIO *b;
    char a[20];
    int ret;

    bn = BN_new();
    strcpy(a, "32");
    ret = BN_hex2bn(&bn, a);

    if (ret == 0) {
        // 转换失败，进行错误处理
        // 例如，打印错误消息或释放相关资源
        BN_free(bn);
        return 1;
    }

    b = BIO_new(BIO_s_file());
    ret = BIO_set_fp(b, stdout, BIO_NOCLOSE);
    BIO_write(b, "aaa", 3);
    BN_print(b, bn);

    BN_free(bn);
    BIO_free(b);

    return 0;
}*/
/*#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
int main()
{
BIGNUM *bn;
BIO *b;
char a[20];
int ret;
bn=BN_new();
strcpy(a,"32");
ret=BN_hex2bn(&bn,a);
b=BIO_new(BIO_s_file());
ret=BIO_set_fp(b,stdout,BIO_NOCLOSE);
BIO_write(b,"aaa",3);
BN_print(b,bn);
BN_free(bn);
return 0;
}*/

