#include <openssl/bn.h>
#include <string.h>
#include <openssl/bio.h>
#include <CUnit/Basic.h>

void test_performBNOperations()
{
    const char* input = "32";
    performBNOperations(input);
    // 在这里无法直接验证函数是否成功执行，因此可以使用 CU_ASSERT 来表示测试通过
    CU_ASSERT(1);
}

int main()
{
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("BNOperationsSuite", NULL, NULL);
    CU_add_test(suite, "test_performBNOperations", test_performBNOperations);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}
