#include <openssl/bn.h>
#include <stdlib.h>
#include <stdio.h>

void performBNOperations() {
    BIGNUM *ret1 = NULL;
    char bin[50], *buf = NULL;
    int len;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    len = BN_num_bytes(ret1);
    buf = malloc(len);
    BN_bn2bin(ret1, buf);

    // 输出结果
    printf("ret1: ");
    BN_print_fp(stdout, ret1);
    printf("\n");

    printf("Binary representation: ");
    for (int i = 0; i < len; i++) {
        printf("%02X", (unsigned char)buf[i]);
    }
    printf("\n");

    // 进行其他操作...

    free(buf);
    BN_free(ret1);
}

/*int main() {
    performBNOperations();

    return 0;
}*/
/*#include <openssl/bn.h>
#include <stdlib.h>

int main()
{
    BIGNUM *ret1 = NULL;
    char bin[50], *buf = NULL;
    int len;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    len = BN_num_bytes(ret1);
    buf = malloc(len);
    BN_bn2bin(ret1, buf);

    free(buf);
    BN_free(ret1);

    return 0;
}*/
/*#include <openssl/bn.h>
int main()
{
BIGNUM *ret1=NULL;
char bin[50],*buf=NULL;
int len;
ret1=BN_bin2bn("242424ab",8, NULL);
len=BN_bn2bin(ret1,bin);
len=BN_num_bytes(ret1);
buf=malloc(len);
len=BN_bn2bin(ret1,buf);
free(buf);
BN_free(ret1);
return 0;
}*/
