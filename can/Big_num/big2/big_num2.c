#include <openssl/bn.h>
#include <stdio.h>

void performBNOperations() {
    BIGNUM *ret1 = NULL;
    BIGNUM *ret2 = NULL;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    ret2 = BN_new();
    BN_bin2bn("242424ab", 8, ret2);

    // 输出结果
    printf("ret1: ");
    BN_print_fp(stdout, ret1);
    printf("\n");

    printf("ret2: ");
    BN_print_fp(stdout, ret2);
    printf("\n");

    // 进行其他操作...

    BN_free(ret1);
    BN_free(ret2);
}

/*int main() {
    performBNOperations();

    return 0;
}*/
/*#include <openssl/bn.h>

int main()
{
    BIGNUM *ret1 = NULL;
    BIGNUM *ret2 = NULL;

    ret1 = BN_new();
    BN_bin2bn("242424ab", 8, ret1);

    ret2 = BN_new();
    BN_bin2bn("242424ab", 8, ret2);

    BN_free(ret1);
    BN_free(ret2);

    return 0;
}*/
/*#include <openssl/bn.h>
int main()
{
BIGNUM *ret1,*ret2;
ret1=BN_new();
ret1=BN_bin2bn("242424ab",8, ret1);
ret2=BN_bin2bn("242424ab",8,NULL);
BN_free(ret1);
BN_free(ret2);
return 0;
}*/
