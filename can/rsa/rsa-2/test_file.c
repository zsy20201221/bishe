#include <CUnit/Basic.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>

// 测试用例: 检查 generateAndTestRSA 函数是否能够成功生成密钥对并进行测试
void test_generateAndTestRSA()
{
    int ret = generateAndTestRSA();

    // 断言 generateAndTestRSA 的返回值为 0
    CU_ASSERT_EQUAL(ret, 0);
}

// 注册测试用例
void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("RSAKeyPair", NULL, NULL);
    CU_add_test(suite, "test_generateAndTestRSA", test_generateAndTestRSA);
}

int main()
{
    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 注册测试套件
    add_test_suite();

    // 运行所有测试套件
    CU_basic_run_tests();

    // 清理 CUnit 资源
    CU_cleanup_registry();

    return CU_get_error();
}
