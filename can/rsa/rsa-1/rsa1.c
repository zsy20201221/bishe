#include <stdio.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>

int generateRSAKeyPair(int bits, unsigned long e, RSA **publicKey, RSA **privateKey)
{
    int ret;
    BIGNUM *bne = NULL;

    // 创建 RSA 结构体
    RSA *rsa = RSA_new();
    if (rsa == NULL)
    {
        printf("Failed to create RSA structure.\n");
        return -1;
    }

    // 设置公钥指数
    bne = BN_new();
    ret = BN_set_word(bne, e);
    if (ret != 1)
    {
        printf("Failed to set public exponent.\n");
        RSA_free(rsa);
        BN_free(bne);
        return -1;
    }

    // 生成密钥对
    ret = RSA_generate_key_ex(rsa, bits, bne, NULL);
    if (ret != 1)
    {
        printf("Failed to generate RSA key pair.\n");
        RSA_free(rsa);
        BN_free(bne);
        return -1;
    }

    *publicKey = rsa;
    *privateKey = RSAPrivateKey_dup(rsa);
    BN_free(bne);

    return 0;
}

/*int main()
{
    RSA *publicKey = NULL;
    RSA *privateKey = NULL;
    int bits = 512;
    unsigned long e = RSA_F4;

    // 生成 RSA 密钥对
    int ret = generateRSAKeyPair(bits, e, &publicKey, &privateKey);
    if (ret != 0)
    {
        printf("Failed to generate RSA key pair.\n");
        return -1;
    }

    // 打印公钥信息
    printf("Public Key:\n");
    RSA_print_fp(stdout, publicKey, 11);

    // 打印私钥信息
    printf("\nPrivate Key:\n");
    RSA_print_fp(stdout, privateKey, 11);

    // 释放资源
    RSA_free(publicKey);
    RSA_free(privateKey);

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>

int main()
{
    RSA *r;
    int bits = 512, ret;
    unsigned long e = RSA_F4; // 使用常量 RSA_F4 作为公钥指数
    BIGNUM *bne;

    // 生成密钥对
    r = RSA_new();
    bne = BN_new();
    ret = BN_set_word(bne, e);
    ret = RSA_generate_key_ex(r, bits, bne, NULL);
    if (ret != 1)
    {
        printf("RSA_generate_key_ex err!\n");
        return -1;
    }

    // 打印密钥对信息
    RSA_print_fp(stdout, r, 11);

    // 释放资源
    RSA_free(r);
    BN_free(bne);

    return 0;
}*/
/*#include <openssl/rsa.h>
int main()
{
RSA *r;
int bits=512,ret;
unsigned long e=RSA_3;
BIGNUM *bne;
r=RSA_generate_key(bits,e,NULL,NULL);
RSA_print_fp(stdout,r,11);
RSA_free(r);
bne=BN_new();
ret=BN_set_word(bne,e);
r=RSA_new();
ret=RSA_generate_key_ex(r,bits,bne,NULL);
if(ret!=1)
{
printf("RSA_generate_key_ex err!\n");
return -1;
}
RSA_free(r);
return 0;
}
*/
