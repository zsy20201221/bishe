#include <CUnit/Basic.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>

// 测试用例: 检查 generateRSAKeyPair 函数是否能够成功生成密钥对
void test_generateRSAKeyPair()
{
    RSA *publicKey = NULL;
    RSA *privateKey = NULL;
    int bits = 512;
    unsigned long e = RSA_F4;

    int ret = generateRSAKeyPair(bits, e, &publicKey, &privateKey);

    // 断言生成密钥对的返回值为 0
    CU_ASSERT_EQUAL(ret, 0);
    // 断言生成的公钥和私钥不为空
    CU_ASSERT_PTR_NOT_NULL(publicKey);
    CU_ASSERT_PTR_NOT_NULL(privateKey);

    // 释放资源
    RSA_free(publicKey);
    RSA_free(privateKey);
}

// 注册测试用例
void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("RSAKeyPair", NULL, NULL);
    CU_add_test(suite, "test_generateRSAKeyPair", test_generateRSAKeyPair);
}

int main()
{
    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 注册测试套件
    add_test_suite();

    // 运行所有测试套件
    CU_basic_run_tests();

    // 清理 CUnit 资源
    CU_cleanup_registry();

    return CU_get_error();
}
