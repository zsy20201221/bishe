#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <string.h>
#include <openssl/objects.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>

int init_suite(void) {
    return 0;
}

int clean_suite(void) {
    return 0;
}

void test_RSA_operations(void) {
    // Perform RSA operations for NID_md5
    int result_md5 = performRSAOperations(1024, RSA_3, 1);
    CU_ASSERT_EQUAL(result_md5, 0);

    // Perform RSA operations for NID_sha1
    int result_sha1 = performRSAOperations(1024, RSA_3, 2);
    CU_ASSERT_EQUAL(result_sha1, 0);

    // Perform RSA operations for NID_sha256
    int result_sha256 = performRSAOperations(1024, RSA_3, 3);
    CU_ASSERT_EQUAL(result_sha256, 0);
}

int main() {
    CU_pSuite pSuite = NULL;

    // Initialize the CUnit test registry
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // Add a suite to the registry
    pSuite = CU_add_suite("Suite_1", init_suite, clean_suite);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the tests to the suite
    if (CU_add_test(pSuite, "test_RSA_operations", test_RSA_operations) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run all tests using the CUnit Basic interface
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // Clean up registry and return
    CU_cleanup_registry();
    return CU_get_error();
}

