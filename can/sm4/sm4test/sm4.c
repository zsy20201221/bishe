#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

int my_sm4encrpt(unsigned char *keyStr, unsigned char *surbuf, int surlen, unsigned char *enbuf);
int dencryptStr(unsigned char *sm4PriKey, unsigned char *cEnStr, int cEnstrlen, unsigned char *deStr);

void performEncryptionDecryption(unsigned char *source, unsigned char *keyStr, int sourceLen, int keyLen) {
    unsigned char sm4_en[512], sm4_de[512];
    int sm4enStrLen, sm4deStrLen;

    sm4enStrLen = my_sm4encrpt(keyStr, source, sourceLen, sm4_en);
    printf("sm4enStrLen: %d\n", sm4enStrLen);
    for (int i = 0; i < sm4enStrLen; ++i) {
        printf("0x%02x ", sm4_en[i]);
    }
    printf("\n");

    sm4deStrLen = dencryptStr(keyStr, sm4_en, sm4enStrLen, sm4_de);
    printf("sm4deStrLen: %d\n", sm4deStrLen);
    for (int i = 0; i < sm4deStrLen; ++i) {
        printf("0x%02x ", sm4_de[i]);
    }
    printf("\n");
}

int my_sm4encrpt(unsigned char *keyStr, unsigned char *surbuf, int surlen, unsigned char *enbuf)
{
    unsigned char *out_buf = enbuf;
    int out_len;
    int out_padding_len;
    int i;
    unsigned char iv[16] = {0}; // 初始化iv为全零
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit(ctx, EVP_sm4_ecb(), keyStr, iv);
    if (0 == surlen % 16) {
        EVP_CIPHER_CTX_set_padding(ctx, 0);
    }
    out_len = 0;
    EVP_EncryptUpdate(ctx, out_buf, &out_len, surbuf, surlen);
    out_padding_len = 0;
    EVP_EncryptFinal(ctx, out_buf + out_len, &out_padding_len);

    EVP_CIPHER_CTX_free(ctx);
    return out_len + out_padding_len;
}


int dencryptStr(unsigned char *sm4PriKey, unsigned char *cEnStr, int cEnstrlen, unsigned char *deStr)
{
    unsigned char iv[16] = {0}; // 初始化iv为全零
    EVP_CIPHER_CTX *ctx;
    int len;
    int temlen;
    int deStrLen;

    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) {
        printf("EVP_CIPHER_CTX_new failed\n");
        return -1;
    }

    if (1 != EVP_DecryptInit(ctx, EVP_sm4_ecb(), sm4PriKey, iv)) {
        printf("EVP_DecryptInit_ex failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if (1 != EVP_DecryptUpdate(ctx, deStr, &len, cEnStr, cEnstrlen)) {
        printf("EVP_DecryptUpdate failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if (0 == len % 16) {
        EVP_CIPHER_CTX_set_padding(ctx, 0);
        len += 16;
    }

    if (!EVP_DecryptFinal(ctx, deStr + len, &temlen)) {
        printf("EVP_DecryptFinal failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    deStrLen = len + temlen;
    printf("解密数据：%d\n",deStrLen);
    for (int i = 0;i < deStrLen;i++) {
        printf("0x%02x ",*(deStr + i));
    }
    printf("\n");
    EVP_CIPHER_CTX_free(ctx);
    return deStrLen;
}

/*int main(int argc, char *argv[]) {
    unsigned char source[20] = {0x41, 0x12, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20};
    unsigned char keyStr[16] = {0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30};

    performEncryptionDecryption(source, keyStr, sizeof(source), sizeof(keyStr));

    return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

int my_sm4encrpt(unsigned char *keyStr, unsigned char *surbuf, int surlen, unsigned char *enbuf);
int dencryptStr(unsigned char *sm4PriKey, unsigned char *cEnStr, int cEnstrlen, unsigned char *deStr);

int main(int argc, char *argv[])
{
    unsigned char sm4_en[512], sm4_de[512];
    int sm4enStrLen, sm4deStrLen;
    unsigned char source[20] = {0x41, 0x12, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20};
    unsigned char keyStr[16] = {0x15, 0x67, 0x28, 0xe1, 0x5f, 0x9a, 0xfc, 0x01, 0xd4, 0xb6, 0x1b, 0x4e, 0x44, 0x5d, 0xbb, 0x26};

    sm4enStrLen = my_sm4encrpt(keyStr, source, 20, sm4_en);
    printf("sm4enStrLen: %d\n", sm4enStrLen);
    for (int i = 0; i < sm4enStrLen; ++i) {
        printf("0x%02x ", sm4_en[i]);
    }
    printf("\n");

    sm4deStrLen = dencryptStr(keyStr, sm4_en, sm4enStrLen, sm4_de);
    printf("sm4deStrLen: %d\n", sm4deStrLen);
    for (int i = 0; i < sm4deStrLen; ++i) {
        printf("0x%02x ", sm4_de[i]);
    }
    printf("\n");

    return 0;
}

int my_sm4encrpt(unsigned char *keyStr, unsigned char *surbuf, int surlen, unsigned char *enbuf)
{
    unsigned char *out_buf = enbuf;
    int out_len;
    int out_padding_len;
    int i;
    unsigned char iv[16] = {0}; // 初始化iv为全零
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit(ctx, EVP_sm4_ecb(), keyStr, iv);
    if (0 == surlen % 16) {
        EVP_CIPHER_CTX_set_padding(ctx, 0);
    }
    out_len = 0;
    EVP_EncryptUpdate(ctx, out_buf, &out_len, surbuf, surlen);
    out_padding_len = 0;
    EVP_EncryptFinal(ctx, out_buf + out_len, &out_padding_len);

    EVP_CIPHER_CTX_free(ctx);
    return out_len + out_padding_len;
}


int dencryptStr(unsigned char *sm4PriKey, unsigned char *cEnStr, int cEnstrlen, unsigned char *deStr)
{
    unsigned char iv[16] = {0}; // 初始化iv为全零
    EVP_CIPHER_CTX *ctx;
    int len;
    int temlen;
    int deStrLen;

    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) {
        printf("EVP_CIPHER_CTX_new failed\n");
        return -1;
    }

    if (1 != EVP_DecryptInit(ctx, EVP_sm4_ecb(), sm4PriKey, iv)) {
        printf("EVP_DecryptInit_ex failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if (1 != EVP_DecryptUpdate(ctx, deStr, &len, cEnStr, cEnstrlen)) {
        printf("EVP_DecryptUpdate failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if (0 == len % 16) {
        EVP_CIPHER_CTX_set_padding(ctx, 0);
        len += 16;
    }

    if (!EVP_DecryptFinal(ctx, deStr + len, &temlen)) {
        printf("EVP_DecryptFinal failed\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    deStrLen = len + temlen;
    printf("解密数据：%d\n",deStrLen);
    for (int i = 0;i < deStrLen;i++) {
        printf("0x%02x ",*(deStr + i));
    }
    printf("\n");
    EVP_CIPHER_CTX_free(ctx);
    return deStrLen;
}*/
