#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/evp.h>

void testPerformEncryptionDecryption(void) {
    unsigned char source[20] = {0x41, 0x12, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20};
    unsigned char keyStr[16] = {0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30};

    unsigned char expectedSource[20];
    memcpy(expectedSource, source, sizeof(source));

    unsigned char encrypted[512];
    int encryptedLen = my_sm4encrpt(keyStr, source, sizeof(source), encrypted);
    // 验证加密后的长度是否符合预期
    CU_ASSERT_TRUE(encryptedLen > 0);

    unsigned char decrypted[512];
    int decryptedLen = dencryptStr(keyStr, encrypted, encryptedLen, decrypted);
    // 验证解密后的长度是否符合预期
    CU_ASSERT_TRUE(decryptedLen > 0);

   
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("PerformEncryptionDecryptionSuite", NULL, NULL);

    // 将测试用例添加到测试套件中
    CU_add_test(suite, "testPerformEncryptionDecryption", testPerformEncryptionDecryption);

    // 设置 CUnit 基本模式并运行测试套件
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return 0;
}
