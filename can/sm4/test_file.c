#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/evp.h>


void testPerformSm4EncryptionDecryption(void) {
    const unsigned char key[] = "0123456789abcdef";
    const char *plaintext = "Hello, World!";
    int plaintext_len = strlen(plaintext);

    int block_size = EVP_CIPHER_block_size(EVP_sm4_ecb());
    int ciphertext_len = ((plaintext_len + block_size - 1) / block_size) * block_size;

    unsigned char *ciphertext = malloc(ciphertext_len);
    unsigned char *decryptedtext = malloc(plaintext_len);

    CU_ASSERT_PTR_NOT_NULL(ciphertext);
    CU_ASSERT_PTR_NOT_NULL(decryptedtext);

    sm4_encrypt((const unsigned char *)plaintext, plaintext_len, key, ciphertext);
    sm4_decrypt(ciphertext, ciphertext_len, key, decryptedtext);

    // 验证解密后的明文与原始明文是否一致
    CU_ASSERT_NSTRING_EQUAL((const char *)decryptedtext, plaintext, plaintext_len);

    free(ciphertext);
    free(decryptedtext);
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("PerformSm4EncryptionDecryptionSuite", NULL, NULL);

    // 将测试用例添加到测试套件中
    CU_add_test(suite, "testPerformSm4EncryptionDecryption", testPerformSm4EncryptionDecryption);

    // 设置 CUnit 基本模式并运行测试套件
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return 0;
}
