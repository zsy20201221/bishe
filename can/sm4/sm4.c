#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

void sm4_encrypt(const unsigned char *plaintext, int plaintext_len, const unsigned char *key, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    if (1 != EVP_EncryptInit_ex(ctx, EVP_sm4_ecb(), NULL, key, NULL))
        handleErrors();

    // Set padding mode to PKCS#7
    if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
        handleErrors();

    int len;
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();

    int ciphertext_len = len;

    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();

    ciphertext_len += len;

    EVP_CIPHER_CTX_free(ctx);

    printf("Encrypted text: ");
    for (int i = 0; i < ciphertext_len; i++)
    {
        printf("%02x", ciphertext[i]);
    }
    printf("\n");
}

void sm4_decrypt(const unsigned char *ciphertext, int ciphertext_len, const unsigned char *key, unsigned char *decryptedtext)
{
    EVP_CIPHER_CTX *ctx;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    if (1 != EVP_DecryptInit_ex(ctx, EVP_sm4_ecb(), NULL, key, NULL))
        handleErrors();

    // Set padding mode to PKCS#7
    if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
        handleErrors();

    int len;
    if (1 != EVP_DecryptUpdate(ctx, decryptedtext, &len, ciphertext, ciphertext_len))
        handleErrors();

    int decryptedtext_len = len;

    if (1 != EVP_DecryptFinal_ex(ctx, decryptedtext + len, &len))
        handleErrors();

    decryptedtext_len += len;

    // Remove padding
    int padding = decryptedtext[decryptedtext_len - 1];
    decryptedtext_len -= padding;

    EVP_CIPHER_CTX_free(ctx);

    printf("Decrypted text: %.*s\n", decryptedtext_len, decryptedtext);
}

void perform_sm4_encryption_decryption()
{
    const unsigned char key[] = "0123456789abcdef"; // 128-bit key in hexadecimal format

    char *plaintext = NULL;
    size_t bufsize = 0;

    printf("Enter the text to be encrypted: ");
    getline(&plaintext, &bufsize, stdin);
    plaintext[strcspn(plaintext, "\n")] = '\0'; // Remove newline character if present

    printf("Original text: %s\n", plaintext);

    int plaintext_len = strlen(plaintext);
    int block_size = EVP_CIPHER_block_size(EVP_sm4_ecb()); // Get block size
    int ciphertext_len = ((plaintext_len + block_size - 1) / block_size) * block_size; // Ensure ciphertext length is a multiple of block size

    unsigned char *ciphertext = malloc(ciphertext_len);
    unsigned char *decryptedtext = malloc(plaintext_len);

    if (!ciphertext || !decryptedtext)
    {
        fprintf(stderr, "Memory allocation error\n");
        return;
    }

    sm4_encrypt(plaintext, plaintext_len, key, ciphertext);

    sm4_decrypt(ciphertext, ciphertext_len, key, decryptedtext);

    free(plaintext);
    free(ciphertext);
    free(decryptedtext);
}

/*int main()
{
    OpenSSL_add_all_algorithms();

    perform_sm4_encryption_decryption();

    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

void sm4_encrypt(const unsigned char *plaintext, int plaintext_len, const unsigned char *key, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    if (1 != EVP_EncryptInit_ex(ctx, EVP_sm4_ecb(), NULL, key, NULL))
        handleErrors();

    // Set padding mode to PKCS#7
    if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
        handleErrors();

    int len;
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();

    int ciphertext_len = len;

    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();

    ciphertext_len += len;

    EVP_CIPHER_CTX_free(ctx);

    printf("Encrypted text: ");
    for (int i = 0; i < ciphertext_len; i++)
    {
        printf("%02x", ciphertext[i]);
    }
    printf("\n");
}

void sm4_decrypt(const unsigned char *ciphertext, int ciphertext_len, const unsigned char *key, unsigned char *decryptedtext)
{
    EVP_CIPHER_CTX *ctx;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    if (1 != EVP_DecryptInit_ex(ctx, EVP_sm4_ecb(), NULL, key, NULL))
        handleErrors();

    // Set padding mode to PKCS#7
    if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
        handleErrors();

    int len;
    if (1 != EVP_DecryptUpdate(ctx, decryptedtext, &len, ciphertext, ciphertext_len))
        handleErrors();

    int decryptedtext_len = len;

    if (1 != EVP_DecryptFinal_ex(ctx, decryptedtext + len, &len))
        handleErrors();

    decryptedtext_len += len;

    // Remove padding
    int padding = decryptedtext[decryptedtext_len - 1];
    decryptedtext_len -= padding;

    EVP_CIPHER_CTX_free(ctx);

    printf("Decrypted text: %.*s\n", decryptedtext_len, decryptedtext);
}

int main()
{
    OpenSSL_add_all_algorithms();

    const unsigned char key[] = "0123456789abcdef"; // 128-bit key in hexadecimal format

    char *plaintext = NULL;
    size_t bufsize = 0;

    printf("Enter the text to be encrypted: ");
    getline(&plaintext, &bufsize, stdin);
    plaintext[strcspn(plaintext, "\n")] = '\0'; // Remove newline character if present

    printf("Original text: %s\n", plaintext);

    int plaintext_len = strlen(plaintext);
    int block_size = EVP_CIPHER_block_size(EVP_sm4_ecb()); // Get block size
    int ciphertext_len = ((plaintext_len + block_size - 1) / block_size) * block_size; // Ensure ciphertext length is a multiple of block size

    unsigned char *ciphertext = malloc(ciphertext_len);
    unsigned char *decryptedtext = malloc(plaintext_len);

    if (!ciphertext || !decryptedtext)
    {
        fprintf(stderr, "Memory allocation error\n");
        return 1;
    }

    sm4_encrypt(plaintext, plaintext_len, key, ciphertext);

    sm4_decrypt(ciphertext, ciphertext_len, key, decryptedtext);

    free(plaintext);
    free(ciphertext);
    free(decryptedtext);

    return 0;
}
*/
