#include <stdio.h>
#include <openssl/pem.h>
#include <openssl/bio.h>

int processPEMFile(const char *filename)
{
    BIO *bp;
    char *name = NULL, *header = NULL;
    unsigned char *data = NULL;
    int len, ret, ret2;
    EVP_CIPHER_INFO cipher;

    OpenSSL_add_all_algorithms();

    bp = BIO_new_file(filename, "r");
    if (bp == NULL) {
        printf("Failed to open file.\n");
        return -1;
    }

    while (1) {
        ret2 = PEM_read_bio(bp, &name, &header, &data, &len);
        if (ret2 == 0)
            break;

        if (strlen(header) > 0) {
            ret = PEM_get_EVP_CIPHER_INFO(header, &cipher);
            if (ret == 0) {
                printf("PEM_get_EVP_CIPHER_INFO error!\n");
                return -1;
            }

            ret = PEM_do_header(&cipher, data, &len, NULL, NULL);
            if (ret == 0) {
                printf("PEM_do_header error!\n");
                return -1;
            }
        }

        OPENSSL_free(name);
        OPENSSL_free(header);
        OPENSSL_free(data);
        name = NULL;
        header = NULL;
        data = NULL;
    }

    BIO_free(bp);

    return 0;
}

/*int main()
{
    const char *filename = "server2.pem";
    int result = processPEMFile(filename);

    if (result == 0) {
        printf("Test successful.\n");
    } else {
        printf("Test failed.\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/pem.h>
#include <openssl/bio.h>

int main()
{
    BIO *bp;
    char *name = NULL, *header = NULL;
    unsigned char *data = NULL;
    int len, ret, ret2;
    EVP_CIPHER_INFO cipher;

    OpenSSL_add_all_algorithms();

    bp = BIO_new_file("server2.pem", "r");
    if (bp == NULL) {
        printf("Failed to open file.\n");
        return -1;
    }

    while (1) {
        ret2 = PEM_read_bio(bp, &name, &header, &data, &len);
        if (ret2 == 0)
            break;

        if (strlen(header) > 0) {
            ret = PEM_get_EVP_CIPHER_INFO(header, &cipher);
            if (ret == 0) {
                printf("PEM_get_EVP_CIPHER_INFO error!\n");
                return -1;
            }

            ret = PEM_do_header(&cipher, data, &len, NULL, NULL);
            if (ret == 0) {
                printf("PEM_do_header error!\n");
                return -1;
            }
        }

        OPENSSL_free(name);
        OPENSSL_free(header);
        OPENSSL_free(data);
        name = NULL;
        header = NULL;
        data = NULL;
    }

    printf("Test successful.\n");

    BIO_free(bp);

    return 0;
}*/
/*#include <openssl/pem.h>
#include <openssl/bio.h>
int main()
{
BIO *bp;
char *name=NULL,*header=NULL;
unsigned char *data=NULL;
int len,ret,ret2;
EVP_CIPHER_INFO cipher;
OpenSSL_add_all_algorithms();
bp=BIO_new_file("server2.pem","r");
while(1)
{
ret2=PEM_read_bio(bp,&name,&header,&data,&len);
if(ret2==0)
break;
if(strlen(header)>0)
{
ret=PEM_get_EVP_CIPHER_INFO(header,&cipher);
ret=PEM_do_header(&cipher,data,&len,NULL,NULL);
if(ret==0)
{
printf("PEM_do_header err!\n");
return -1;
}
}
OPENSSL_free(name);
OPENSSL_free(header);
OPENSSL_free(data);
}
printf("test ok.\n");
BIO_free(bp);
return 0;
}

*/
