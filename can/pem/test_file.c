#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/pem.h>
#include <openssl/bio.h>

void testProcessPEMFile()
{
    const char *filename = "server2.pem";
    int result = processPEMFile(filename);
    CU_ASSERT_EQUAL(result, 0);
}

int main()
{
    CU_pSuite suite = NULL;

    // Initialize CUnit test framework
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // Create a test suite
    suite = CU_add_suite("ProcessPEMFileSuite", NULL, NULL);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (CU_add_test(suite, "processPEMFile Test", testProcessPEMFile) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Set output mode to verbose
    CU_basic_set_mode(CU_BRM_VERBOSE);

    // Run the tests
    CU_basic_run_tests();

    // Clean up CUnit resources
    CU_cleanup_registry();

    return CU_get_error();
}
