#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <openssl/conf.h>

void test_processConfiguration()
{
    CONF* conf;
    long eline = -1;
    int ret = 0;
    char* p;
    BIO* bp;

    conf = NCONF_new(NULL);
    CU_ASSERT_PTR_NOT_NULL_FATAL(conf); // 断言创建的 CONF 对象不为空

    ret = NCONF_load(conf, "/usr/src/openssl-3.2.0/apps/openssl.cnf", &eline);
    CU_ASSERT_EQUAL(ret, 1); // 断言加载配置文件成功

    p = NCONF_get_string(conf, "CA_default", "certs");
    CU_ASSERT_PTR_NOT_NULL(p); // 断言获取 "CA_default" 部分的 "certs" 键的字符串值不为空

    p = NCONF_get_string(conf, "CA_default", "default_days");
    CU_ASSERT_PTR_NOT_NULL(p); // 断言获取 "CA_default" 部分的 "default_days" 键的字符串值不为空

    ret = NCONF_get_number_e(conf, "CA_default", "default_days", &eline);
    CU_ASSERT_EQUAL(ret, 1); // 断言成功获取 "CA_default" 部分的 "default_days" 键的数值

    ret = NCONF_get_number(conf, "CA_default", "default_days", &eline);
    CU_ASSERT_EQUAL(ret, 1); // 断言成功获取 "CA_default" 部分的 "default_days" 键的数值

    NCONF_free(conf);
}

int initialize_suite(void)
{
    return 0;
}

int cleanup_suite(void)
{
    return 0;
}

void add_tests()
{
    CU_pSuite pSuite = NULL;

    pSuite = CU_add_suite("ProcessConfigurationTestSuite", initialize_suite, cleanup_suite);
    if (pSuite == NULL)
    {
        return;
    }

    CU_ADD_TEST(pSuite, test_processConfiguration);
}

int main()
{
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    add_tests();

    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_basic_run_tests();

    CU_basic_show_failures(CU_get_failure_list());

    CU_cleanup_registry();

    return CU_get_error();
}
