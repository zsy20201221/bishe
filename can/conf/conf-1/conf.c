#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/conf.h>

void processConfiguration()
{
    CONF* conf;
    long eline = -1;
    int ret = 0;
    char* p;
    BIO* bp;

    conf = NCONF_new(NULL);
    if (conf == NULL)
    {
        fprintf(stderr, "Error creating CONF object\n");
        return;
    }

#if 0
    bp = BIO_new_file("openssl.cnf", "r");
    NCONF_load_bio(conf, bp, &eline);
#else
    ret = NCONF_load(conf, "/usr/src/openssl-3.2.0/apps/openssl.cnf", &eline);
    if (ret != 1)
    {
        fprintf(stderr, "Error loading configuration file\n");
        NCONF_free(conf);
        return;
    }
#endif

    p = NCONF_get_string(conf, "CA_default", "certs");
    if (p == NULL)
        printf("No global certs info\n");
    else
        printf("%s\n", p);

    p = NCONF_get_string(conf, "CA_default", "default_days");
    if (p == NULL)
        printf("No default_days info\n");
    else
        printf("%s\n", p);

    ret = NCONF_get_number_e(conf, "CA_default", "default_days", &eline);
    if (ret != 1)
        printf("Error getting number\n");
    else
        printf("%ld\n", eline);

    ret = NCONF_get_number(conf, "CA_default", "default_days", &eline);
    if (ret != 1)
        printf("Error getting number\n");
    else
        printf("%ld\n", eline);

    NCONF_free(conf);
}

/*int main()
{
    processConfiguration();
    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/conf.h>

int main()
{
    CONF* conf;
    long eline = -1;
    int ret = 0;
    char* p;
    BIO* bp;

    conf = NCONF_new(NULL);
    if (conf == NULL)
    {
        fprintf(stderr, "Error creating CONF object\n");
        return -1;
    }

#if 0
    bp = BIO_new_file("openssl.cnf", "r");
    NCONF_load_bio(conf, bp, &eline);
#else
    ret = NCONF_load(conf, "/usr/src/openssl-3.2.0/apps/openssl.cnf", &eline);
    if (ret != 1)
    {
        fprintf(stderr, "Error loading configuration file\n");
        NCONF_free(conf);
        return -1;
    }
#endif

    p = NCONF_get_string(conf, "CA_default", "certs");
    if (p == NULL)
        printf("No global certs info\n");
    else
        printf("%s\n", p);

    p = NCONF_get_string(conf, "CA_default", "default_days");
    if (p == NULL)
        printf("No default_days info\n");
    else
        printf("%s\n", p);

    ret = NCONF_get_number_e(conf, "CA_default", "default_days", &eline);
    if (ret != 1)
        printf("Error getting number\n");
    else
        printf("%ld\n", eline);

    ret = NCONF_get_number(conf, "CA_default", "default_days", &eline);
    if (ret != 1)
        printf("Error getting number\n");
    else
        printf("%ld\n", eline);

    NCONF_free(conf);
    return 0;
}*/
/*#include <openssl/conf.h>
int main()
{
	CONF* conf;
	long eline, result;
	int ret;
	char* p;
	BIO* bp;
	conf = NCONF_new(NULL);
#if 0
	bp = BIO_new_file("openssl.cnf", "r");
	NCONF_load_bio(conf, bp, &eline);
#else
	ret = NCONF_load(conf, "/usr/src/openssl-3.2.0/apps/openssl.cnf", &eline);
	if (ret != 1)
	{
		printf("err!\n");
		return -1;
	}
#endif
	p = NCONF_get_string(conf, NULL, "certs");
	if (p == NULL)
		printf("no global certs info\n");
	p = NCONF_get_string(conf, "CA_default", "certs");
	printf("%s\n", p);
	p = NCONF_get_string(conf, "CA_default", "default_days");
	printf("%s\n", p);
	ret = NCONF_get_number_e(conf, "CA_default", "default_days", &result);
	printf("%d\n", result);
	ret = NCONF_get_number(conf, "CA_default", "default_days", &result);
	printf("%d\n", result);
	NCONF_free(conf);
	return 0;
}
*/
