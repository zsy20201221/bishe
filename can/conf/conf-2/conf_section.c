#include <stdio.h>
#include <openssl/conf.h>

void processConfiguration(const char* filename)
{
    CONF* conf;
    BIO* bp;
    STACK_OF(CONF_VALUE)* v;
    CONF_VALUE* one;
    int i, num;
    long eline;

    conf = NCONF_new(NULL);
    if (conf == NULL)
    {
        fprintf(stderr, "Error creating CONF object\n");
        return;
    }

    bp = BIO_new_file(filename, "r");
    if (bp == NULL)
    {
        fprintf(stderr, "Error opening file\n");
        NCONF_free(conf);
        return;
    }

    if (!NCONF_load_bio(conf, bp, &eline))
    {
        fprintf(stderr, "Error loading configuration file\n");
        BIO_free(bp);
        NCONF_free(conf);
        return;
    }

    v = NCONF_get_section(conf, "CA_default");
    if (v == NULL)
    {
        fprintf(stderr, "Error getting section\n");
        BIO_free(bp);
        NCONF_free(conf);
        return;
    }

    num = sk_CONF_VALUE_num(v);
    printf("section CA_default:\n");

    for (i = 0; i < num; i++)
    {
        one = sk_CONF_VALUE_value(v, i);
        printf("%s = %s\n", one->name, one->value);
    }

    BIO_free(bp);
    NCONF_free(conf);
}

/*int main()
{
    const char* filename = "/usr/src/openssl-3.2.0/apps/openssl.cnf";
    processConfiguration(filename);
    return 0;
}*/
/*#include <stdio.h>
#include <openssl/conf.h>

int main()
{
    CONF* conf;
    BIO* bp;
    STACK_OF(CONF_VALUE)* v;
    CONF_VALUE* one;
    int i, num;
    long eline;

    conf = NCONF_new(NULL);
    if (conf == NULL)
    {
        fprintf(stderr, "Error creating CONF object\n");
        return -1;
    }

    bp = BIO_new_file("/usr/src/openssl-3.2.0/apps/openssl.cnf", "r");
    if (bp == NULL)
    {
        fprintf(stderr, "Error opening file\n");
        NCONF_free(conf);
        return -1;
    }

    if (!NCONF_load_bio(conf, bp, &eline))
    {
        fprintf(stderr, "Error loading configuration file\n");
        BIO_free(bp);
        NCONF_free(conf);
        return -1;
    }

    v = NCONF_get_section(conf, "CA_default");
    if (v == NULL)
    {
        fprintf(stderr, "Error getting section\n");
        BIO_free(bp);
        NCONF_free(conf);
        return -1;
    }

    num = sk_CONF_VALUE_num(v);
    printf("section CA_default:\n");

    for (i = 0; i < num; i++)
    {
        one = sk_CONF_VALUE_value(v, i);
        printf("%s = %s\n", one->name, one->value);
    }

    BIO_free(bp);
    NCONF_free(conf);
    return 0;
}*/
/*#include <openssl/conf.h>
int main()
{
CONF *conf;
BIO *bp;
STACK_OF(CONF_VALUE) *v;
CONF_VALUE *one;
int i,num;
long eline;
conf=NCONF_new(NULL);
bp=BIO_new_file("/usr/src/openssl-3.2.0/apps/openssl.cnf","r");
if(bp==NULL)
{
printf("err!\n");
return -1;
}
NCONF_load_bio(conf,bp,&eline);
v=NCONF_get_section(conf,"CA_default");
num=sk_CONF_VALUE_num(v);
printf("section CA_default :\n");
for(i=0;i<num;i++)
{
one=sk_CONF_VALUE_value(v,i);
printf("%s = %s\n",one->name,one->value);
}
BIO_free(bp);
printf("\n");
return 0;
}
*/
