#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <openssl/conf.h>

void test_processConfiguration()
{
    const char* filename = "test_openssl.cnf"; // 假设测试使用的配置文件名为 test_openssl.cnf
    CONF* conf;
    BIO* bp;
    STACK_OF(CONF_VALUE)* v;
    CONF_VALUE* one;
    int i, num;
    long eline;

    conf = NCONF_new(NULL);
    CU_ASSERT_PTR_NOT_NULL_FATAL(conf); // 断言创建的 CONF 对象不为空

    bp = BIO_new_file(filename, "r");
    CU_ASSERT_PTR_NOT_NULL_FATAL(bp); // 断言成功打开配置文件

    CU_ASSERT_TRUE(NCONF_load_bio(conf, bp, &eline)); // 断言成功加载配置文件

    v = NCONF_get_section(conf, "CA_default");
    CU_ASSERT_PTR_NOT_NULL(v); // 断言成功获取 "CA_default" 部分

    num = sk_CONF_VALUE_num(v);
    CU_ASSERT_TRUE(num > 0); // 断言获取的部分中有配置项

    printf("section CA_default:\n");
    for (i = 0; i < num; i++)
    {
        one = sk_CONF_VALUE_value(v, i);
        printf("%s = %s\n", one->name, one->value);
    }

    BIO_free(bp);
    NCONF_free(conf);
}

int initialize_suite(void)
{
    return 0;
}

int cleanup_suite(void)
{
    return 0;
}

void add_tests()
{
    CU_pSuite pSuite = NULL;

    pSuite = CU_add_suite("ProcessConfigurationTestSuite", initialize_suite, cleanup_suite);
    if (pSuite == NULL)
    {
        return;
    }

    CU_ADD_TEST(pSuite, test_processConfiguration);
}

int main()
{
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    add_tests();

    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_basic_run_tests();

    CU_basic_show_failures(CU_get_failure_list());

    CU_cleanup_registry();

    return CU_get_error();
}
