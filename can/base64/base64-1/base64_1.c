#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

void base64EncodeDecode(const unsigned char *input, int inputLength, unsigned char *output, int *outputLength) {
    EVP_ENCODE_CTX *ectx, *dctx;

    ectx = EVP_ENCODE_CTX_new();
    dctx = EVP_ENCODE_CTX_new();

    // 编码
    EVP_EncodeInit(ectx);
    int total = 0;

    EVP_EncodeUpdate(ectx, output, outputLength, input, inputLength);
    total += *outputLength;

    EVP_EncodeFinal(ectx, output + total, outputLength);
    total += *outputLength;

    printf("Encoded: %.*s\n", total, output);

    // 解码
    EVP_DecodeInit(dctx);
    *outputLength = 800;  // 修改为解码后的最大输出长度

    int total2 = 0;
    int ret = EVP_DecodeUpdate(dctx, output, outputLength, output, total);

    if (ret < 0) {
        printf("EVP_DecodeUpdate err!\n");
        goto cleanup;
    }

    total2 += *outputLength;

    ret = EVP_DecodeFinal(dctx, output + total2, outputLength);

    if (ret < 0) {
        printf("EVP_DecodeFinal err!\n");
        goto cleanup;
    }

    total2 += *outputLength;

    printf("Decoded: %.*s\n", total2, output);

cleanup:
    EVP_ENCODE_CTX_free(ectx);
    EVP_ENCODE_CTX_free(dctx);
}

/*int main() {
    unsigned char in[500], out[800];
    int inl = 500, outl = 800;

    for (int i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    base64EncodeDecode(in, inl, out, &outl);

    return 0;
}*/

/*#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

int main()
{
    EVP_ENCODE_CTX *ectx, *dctx;
    unsigned char in[500], out[800], d[500];
    int inl, outl, i, total, ret, total2;

    ectx = EVP_ENCODE_CTX_new();
    dctx = EVP_ENCODE_CTX_new();

    EVP_EncodeInit(ectx);

    for (i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    inl = 500;
    total = 0;

    EVP_EncodeUpdate(ectx, out, &outl, in, inl);
    total += outl;

    EVP_EncodeFinal(ectx, out + total, &outl);
    total += outl;

    printf("%.*s\n", total, out);

    EVP_DecodeInit(dctx);

    outl = 800;  // 修改为解码后的最大输出长度

    total2 = 0;

    ret = EVP_DecodeUpdate(dctx, d, &outl, out, total);
    if (ret < 0)
    {
        printf("EVP_DecodeUpdate err!\n");
        EVP_ENCODE_CTX_free(ectx);  // 释放内存
        EVP_ENCODE_CTX_free(dctx);  // 释放内存
        return -1;
    }

    total2 += outl;

    ret = EVP_DecodeFinal(dctx, d + total2, &outl);
    if (ret < 0)
    {
        printf("EVP_DecodeFinal err!\n");
        EVP_ENCODE_CTX_free(ectx);  // 释放内存
        EVP_ENCODE_CTX_free(dctx);  // 释放内存
        return -1;
    }

    total2 += outl;

    printf("%.*s\n", total2, d);

    EVP_ENCODE_CTX_free(ectx);  // 释放内存
    EVP_ENCODE_CTX_free(dctx);  // 释放内存

    return 0;
}*/
/*#include <string.h>
#include <openssl/evp.h>
int main()
{

EVP_ENCODE_CTX  *ectx , *dctx;
unsigned char in[500],out[800],d[500];
int inl,outl,i,total,ret,total2;
ectx = EVP_ENCODE_CTX_new();
dctx = EVP_ENCODE_CTX_new();
EVP_EncodeInit(ectx);
for(i=0;i<500;i++)
memset(&in[i],i,1);
inl=500;
total=0;
EVP_EncodeUpdate(ectx,out,&outl,in,inl);
total+=outl;
EVP_EncodeFinal(ectx,out+total,&outl);
total+=outl;
printf("%s\n",out);
EVP_DecodeInit(dctx);
outl=500;
total2=0;
ret=EVP_DecodeUpdate(dctx,d,&outl,out,total);
if(ret<0)
{
printf("EVP_DecodeUpdate err!\n");
return -1;
}
total2+=outl;
ret=EVP_DecodeFinal(dctx,d,&outl);
total2+=outl;
return 0;
}*/

