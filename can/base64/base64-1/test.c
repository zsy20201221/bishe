#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

void base64EncodeDecode(const unsigned char *input, int inputLength, unsigned char *output, int *outputLength) {
    EVP_ENCODE_CTX *ectx, *dctx;

    ectx = EVP_ENCODE_CTX_new();
    dctx = EVP_ENCODE_CTX_new();

    // 编码
    EVP_EncodeInit(ectx);
    int total = 0;

    EVP_EncodeUpdate(ectx, output, outputLength, input, inputLength);
    total += *outputLength;

    EVP_EncodeFinal(ectx, output + total, outputLength);
    total += *outputLength;

    printf("Encoded: %.*s\n", total, output);

    // 解码
    EVP_DecodeInit(dctx);
    *outputLength = 800;  // 修改为解码后的最大输出长度

    int total2 = 0;
    int ret = EVP_DecodeUpdate(dctx, output, outputLength, output, total);

    if (ret < 0) {
        printf("EVP_DecodeUpdate err!\n");
        goto cleanup;
    }

    total2 += *outputLength;

    ret = EVP_DecodeFinal(dctx, output + total2, outputLength);

    if (ret < 0) {
        printf("EVP_DecodeFinal err!\n");
        goto cleanup;
    }

    total2 += *outputLength;

    printf("Decoded: %.*s\n", total2, output);

    // 将解码后的数据写入文件
    FILE *file = fopen("decoded.txt", "wb");
    if (file == NULL) {
        printf("Failed to open file for writing.\n");
        goto cleanup;
    }

    fwrite(output, 1, total2, file);
    fclose(file);

cleanup:
    EVP_ENCODE_CTX_free(ectx);
    EVP_ENCODE_CTX_free(dctx);
}

int main() {
    unsigned char in[500], out[800];
    int inl = 500, outl = 800;

    for (int i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    base64EncodeDecode(in, inl, out, &outl);

    return 0;
}
