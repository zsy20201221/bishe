#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <string.h>


// 声明测试函数
void testBase64EncodeDecode();

// 注册测试套件和测试用例
int main() {
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("Suite", NULL, NULL);
    CU_add_test(suite, "testBase64EncodeDecode", testBase64EncodeDecode);

    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}

// 编写测试函数
void testBase64EncodeDecode() {
    unsigned char in[500], out[800];
    int inl = 500, outl = 800;

    // 创建测试数据，可以是你认为适合的输入
    for (int i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    // 调用被测试的函数
    base64EncodeDecode(in, inl, out, &outl);

    // 在这里添加你的断言
    // 例如，使用 CU_ASSERT_EQUAL 来比较预期值和实际值
    CU_ASSERT_EQUAL(memcmp(in, out, inl), 0);
}

