#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <CUnit/Basic.h>

void base64EncodeAndDecode(unsigned char* in, int inLength);

void test_base64EncodeAndDecode()
{
    unsigned char in[500] = {0};
    int inLength;

    for (int i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    inLength = 100;  // 设置示例输入长度

    base64EncodeAndDecode(in, inLength);
}

int main()
{
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("Base64Suite", NULL, NULL);
    CU_add_test(suite, "test_base64EncodeAndDecode", test_base64EncodeAndDecode);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}
