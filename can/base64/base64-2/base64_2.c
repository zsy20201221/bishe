#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

void base64EncodeAndDecode(unsigned char* in, int inLength)
{
    unsigned char out[800], d[500], *p;
    int i, len, pad;

    printf("Input:\n");
    for (int i = 0; i < inLength; i++)
        printf("%02X ", in[i]);
    printf("\n");

    len = EVP_EncodeBlock(out, in, inLength);
    printf("Base64 Encoded:\n%.*s\n", len, out);

    p = out + len - 1;
    pad = 0;

    for (i = 0; i < 4; i++)
    {
        if (*p == '=')
            pad++;
        p--;
    }

    len = EVP_DecodeBlock(d, out, len);
    len -= pad;

    printf("Decoded:\n");
    for (i = 0; i < len; i++)
        printf("%02X ", d[i]);
    printf("\n");

    if ((len != inLength) || (memcmp(in, d, len) != 0))
        printf("Error!\n");
    else
        printf("Test passed.\n");
}

/*int main()
{
    unsigned char in[500] = {0};  // 输入字符串
    int inLength;

    for (int i = 0; i < 500; i++)
        memset(&in[i], i, 1);

    printf("Please input how much (<500) to base64: \n");
    if (scanf("%d", &inLength) != 1 || inLength <= 0 || inLength > 500) {
        printf("Invalid input.\n");
        return 1;
    }

    base64EncodeAndDecode(in, inLength);

    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

int main()
{
	unsigned char in[500], out[800], d[500], *p;
	int inl, i, len, pad;
	for (i = 0; i < 500; i++)
		memset(&in[i], i, 1);
	printf("please input how much(<=500) to base64 : \n");
	if (scanf("%d", &inl) != 1 || inl <= 0 || inl > 500) {
		printf("Invalid input.\n");
		return 1;
	}
	len = EVP_EncodeBlock(out, in, inl);
	printf("%.*s\n", len, out);
	p = out + len - 1;
	pad = 0;
	for (i = 0; i < 4; i++)
	{
		if (*p == '=')
			pad++;
		p--;
	}
	len = EVP_DecodeBlock(d, out, len);
	len -= pad;
	if ((len != inl) || (memcmp(in, d, len) != 0))
		printf("err!\n");
	else
		printf("test ok.\n");
	return 0;
}*/
/*#include <string.h>
#include <openssl/evp.h>
int main()
{
	unsigned char in[500], out[800], d[500], * p;
	int inl, i, len, pad;
	for (i = 0; i < 500; i++)
		memset(&in[i], i, 1);
	printf("please input how much(<500) to base64 : \n");
	scanf("%d", &inl);
	len = EVP_EncodeBlock(out, in, inl);
	printf("%s\n", out);
	p = out + len - 1;
	pad = 0;
	for (i = 0; i < 4; i++)
	{
		if (*p == '=')
			pad++;
		p--;
	}
	len = EVP_DecodeBlock(d, out, len);
	len -= pad;
	if ((len != inl) || (memcmp(in, d, len)))
		printf("err!\n");
	printf("test ok.\n");
	return 0;
}
*/
