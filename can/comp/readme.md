可以尝试使用 OpenSSL 提供的其他调试工具，例如 openssl 命令行工具，以确保您的库正确安装和配置。例如，您可以使用以下命令检查您的 zlib 压缩是否有效：

echo "Hello, world!" | openssl zlib
如果不能执行，则需要重新编译
在安装openssl的位置重新执行编译和安装
./config zlib
make
make test
sudo make install
之后方可实现功能
