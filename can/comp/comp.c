#include <stdio.h>
#include <string.h>
#include <openssl/comp.h>

void handleErrors(const char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    ERR_print_errors_fp(stderr);
}

int compressAndExpandData(unsigned char *in, int ilen, unsigned char *out, int olen, unsigned char *expend, int elen) {
    COMP_CTX *ctx;
    int len, total = 0;

    // 初始化 OpenSSL 错误处理
    ERR_load_COMP_strings();

#ifdef _WIN32
    ctx = COMP_CTX_new(COMP_rle());
#else
    /* for linux */
    ctx = COMP_CTX_new(COMP_zlib());
#endif

    if (!ctx) {
        handleErrors("Failed to create compression context");
        return -1;
    }

    total = COMP_compress_block(ctx, out, olen, in, ilen);
    if (total < 0) {
        handleErrors("Compression failed");
        COMP_CTX_free(ctx);
        return -1;
    }

    len = COMP_expand_block(ctx, expend, elen, out, total);
    if (len < 0) {
        handleErrors("Expansion failed");
        COMP_CTX_free(ctx);
        return -1;
    }

    COMP_CTX_free(ctx);

    return len;
}

/*int main() {
    int ilen = 50, olen = 100, elen = 200;
    unsigned char in[50], out[100], expend[200];

    for (int i = 0; i < ilen; i++)
        memset(&in[i], i, 1);

    int result = compressAndExpandData(in, ilen, out, olen, expend, elen);
    if (result < 0) {
        printf("Compression and Expansion failed!\n");
        return -1;
    }

    printf("Compression and Expansion successful!\n");

    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <openssl/comp.h>

void handleErrors(const char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    ERR_print_errors_fp(stderr);
}

int main() {
    COMP_CTX *ctx;
    int len, olen = 100, ilen = 50, i, total = 0;
    unsigned char in[50], out[100];
    unsigned char expend[200];

    // 初始化 OpenSSL 错误处理
    ERR_load_COMP_strings();


     for linux 
    ctx = COMP_CTX_new(COMP_zlib());


    if (!ctx) {
        handleErrors("Failed to create compression context");
        return -1;
    }

    for (i = 0; i < 50; i++)
        memset(&in[i], i, 1);

    total = COMP_compress_block(ctx, out, olen, in, ilen);
    if (total < 0) {
        handleErrors("Compression failed");
        COMP_CTX_free(ctx);
        return -1;
    }

    len = COMP_expand_block(ctx, expend, 200, out, total);
    if (len < 0) {
        handleErrors("Expansion failed");
        COMP_CTX_free(ctx);
        return -1;
    }

    COMP_CTX_free(ctx);

    printf("Compression and Expansion successful!\n");

    return 0;
}
*/
/*#include <string.h>
#include <openssl/comp.h>

int compressAndExpandData(unsigned char *input, int inputLength, unsigned char *compressedOutput, int maxCompressedLength,
                          unsigned char *expandedOutput, int maxExpandedLength) {
    COMP_CTX *ctx;
    int compressedLength, expandedLength;

#ifdef _WIN32
    ctx = COMP_CTX_new(COMP_rle());
#else
    // for linux
    ctx = COMP_CTX_new(COMP_zlib());
#endif

    if (!ctx) {
        // 处理内存分配失败的情况
        return -1;
    }

    // 初始化数组
    memset(input, 0, inputLength);

    for (int i = 0; i < inputLength; i++)
        memset(&input[i], i, 1);

    compressedLength = COMP_compress_block(ctx, compressedOutput, maxCompressedLength, input, inputLength);

    if (compressedLength < 0) {
        // 处理压缩失败的情况
        COMP_CTX_free(ctx);
        return -1;
    }

    expandedLength = COMP_expand_block(ctx, expandedOutput, maxExpandedLength, compressedOutput, compressedLength);

    if (expandedLength < 0) {
        // 处理解压失败的情况
        COMP_CTX_free(ctx);
        return -1;
    }

    COMP_CTX_free(ctx);

    return expandedLength;
}

int main() {
    int inputLength = 50;
    int maxCompressedLength = 100;
    int maxExpandedLength = 200;

    unsigned char input[50];
    unsigned char compressedOutput[100];
    unsigned char expandedOutput[200];

    int result = compressAndExpandData(input, inputLength, compressedOutput, maxCompressedLength,
                                       expandedOutput, maxExpandedLength);

    if (result >= 0) {
        // 成功处理压缩和解压缩逻辑，可以使用 expandedOutput 数据
    } else {
        // 处理错误情况
    }

    return 0;
}*/
/*#include <string.h>
#include <openssl/comp.h>

int main() {
    COMP_CTX *ctx;
    int len, olen = 100, ilen = 50, i, total = 0;
    unsigned char in[50], out[100];
    unsigned char expend[200];

#ifdef _WIN32
    ctx = COMP_CTX_new(COMP_rle());
#else
    // for linux 
    ctx = COMP_CTX_new(COMP_zlib());
#endif

    if (!ctx) {
        // 处理内存分配失败的情况
        return -1;
    }

    // 初始化数组
    memset(in, 0, sizeof(in));

    for (i = 0; i < 50; i++)
        memset(&in[i], i, 1);

    total = COMP_compress_block(ctx, out, olen, in, ilen);
    
    if (total < 0) {
        // 处理压缩失败的情况
        COMP_CTX_free(ctx);
        return -1;
    }

    len = COMP_expand_block(ctx, expend, sizeof(expend), out, total);

    if (len < 0) {
        // 处理解压失败的情况
        COMP_CTX_free(ctx);
        return -1;
    }

    COMP_CTX_free(ctx);

    // 在使用 expend 之后，如果需要，可以添加相应的处理逻辑来释放 expend 的内存

    return 0;
}
*/
/*#include <string.h>
#include <openssl/comp.h>
int main()
{
COMP_CTX *ctx;
int len,olen=100,ilen=50,i,total=0;
unsigned char in[50],out[100];
unsigned char expend[200];
#ifdef _WIN32
ctx=COMP_CTX_new(COMP_rle());
#else
// for linux 
ctx=COMP_CTX_new(COMP_zlib());
#endif
for(i=0;i<50;i++)
memset(&in[i],i,1);
total=COMP_compress_block(ctx,out,olen,in,50);
len=COMP_expand_block(ctx,expend,200,out,total);
COMP_CTX_free(ctx);
return 0;
}*/

