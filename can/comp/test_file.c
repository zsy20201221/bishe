#include <CUnit/Basic.h>
#include <openssl/comp.h>

// 测试用例: 检查 compressAndExpandData 函数是否能够成功压缩和展开数据
void test_compressAndExpandData()
{
    int ilen = 50, olen = 100, elen = 200;
    unsigned char in[50], out[100], expend[200];

    for (int i = 0; i < ilen; i++)
        memset(&in[i], i, 1);

    int result = compressAndExpandData(in, ilen, out, olen, expend, elen);

    // 断言压缩和展开的结果长度大于0
    CU_ASSERT_TRUE(result > 0);
}

// 注册测试用例
void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("CompressExpand", NULL, NULL);
    CU_add_test(suite, "test_compressAndExpandData", test_compressAndExpandData);
}

int main()
{
    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 注册测试套件
    add_test_suite();

    // 运行所有测试套件
    CU_basic_run_tests();

    // 清理 CUnit 资源
    CU_cleanup_registry();

    return CU_get_error();
}
