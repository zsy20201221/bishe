#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/txt_db.h>

void testPerformDatabaseOperations()
{
    const char *inputFilename = "txtdb.dat";
    const char *outputFilename = "txtdb2.dat";

    int ret = performDatabaseOperations();
    CU_ASSERT_EQUAL(ret, 0);

    // Check if the output file exists
    FILE *file = fopen(outputFilename, "r");
    CU_ASSERT_PTR_NOT_NULL(file);
    fclose(file);

    // Clean up the output file
    remove(outputFilename);
}

int main()
{
    CU_pSuite suite = NULL;

    // Initialize CUnit test framework
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // Create a test suite
    suite = CU_add_suite("DatabaseSuite", NULL, NULL);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (CU_add_test(suite, "performDatabaseOperations Test", testPerformDatabaseOperations) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Set output mode to verbose
    CU_basic_set_mode(CU_BRM_VERBOSE);

    // Run the tests
    CU_basic_run_tests();

    // Clean up CUnit resources
    CU_cleanup_registry();

    return CU_get_error();
}
