#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

int performStringConversion(const char* input, int inputLength, int inform, int nid, char* output, int* outputLength)
{
    int ret = 0;
    char in[100], *p;
    ASN1_STRING *a;

    /* 拷贝输入字符串到本地缓冲区 */
    memcpy(in, input, inputLength);

    /* 如果调用下面两个函数，生成的ASN1_STRING类型将是ASN1_UTF8而不是ASN1_BMPSTRING */
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    ret = ASN1_STRING_set_default_mask_asc("utf8only");
    if (ret != 1)
    {
        printf("ASN1_STRING_set_default_mask_asc err.\n");
        return 0;
    }

    a = ASN1_STRING_set_by_NID(NULL, in, inputLength, inform, nid);
    if (a == NULL)
    {
        printf("ASN1_STRING_set_by_NID failed.\n");
        return 0;
    }

    p = output;
    *outputLength = i2d_ASN1_BMPSTRING(a, &p);
    if (*outputLength < 0)
    {
        printf("i2d_ASN1_BMPSTRING failed.\n");
        ASN1_STRING_free(a);
        return 0;
    }

    ASN1_STRING_free(a);

    return 1;
}

/*int main()
{
    char input[] = { 0xEF, 0xBB, 0xBF, 0xE8, 0xB5, 0xB5 }; // 汉字“赵”的UTF8值
    int inputLength = sizeof(input);
    int inform = MBSTRING_UTF8;
    int nid = NID_commonName;
    char output[100];
    int outputLength;

    if (performStringConversion(input, inputLength, inform, nid, output, &outputLength))
    {
        FILE* fp = fopen("a.cer", "w");
        if (fp != NULL)
        {
            fwrite(output, 1, outputLength, fp);
            fclose(fp);
            printf("String conversion successful!\n");
        }
        else
        {
            printf("File open failed.\n");
        }
    }
    else
    {
        printf("String conversion failed.\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

int main()
{
    int inlen = 6, nid = NID_commonName, inform = MBSTRING_UTF8, len, ret = 0;
    char in[100], out[100], *p;
    ASN1_STRING *a;
    FILE *fp;

    //汉字“赵”的UTF8值,可以用UltraEdit获取
    memset(&in[0], 0xEF, 1);
    memset(&in[1], 0xBB, 1);
    memset(&in[2], 0xBF, 1);
    memset(&in[3], 0xE8, 1);
    memset(&in[4], 0xB5, 1);
    memset(&in[5], 0xB5, 1);

    // 如果调用下面两个函数，生成的ASN1_STRING类型将是ASN1_UTF8而不是ASN1_BMPSTRING 
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    ret = ASN1_STRING_set_default_mask_asc("utf8only");
    if (ret != 1)
    {
        printf("ASN1_STRING_set_default_mask_asc err.\n");
        return 0;
    }

    a = ASN1_STRING_set_by_NID(NULL, in, inlen, inform, nid);
    if (a == NULL)
    {
        printf("ASN1_STRING_set_by_NID failed.\n");
        return 0;
    }

    p = out;
    len = i2d_ASN1_BMPSTRING(a, &p);
    if (len < 0)
    {
        printf("i2d_ASN1_BMPSTRING failed.\n");
        ASN1_STRING_free(a);
        return 0;
    }

    fp = fopen("a.cer", "w");
    if (fp == NULL)
    {
        printf("File open failed.\n");
        ASN1_STRING_free(a);
        return 0;
    }

    fwrite(out, 1, len, fp);
    fclose(fp);
    ASN1_STRING_free(a);

    return 0;
}*/
