#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

// 声明要测试的函数
int performStringConversion(const char* input, int inputLength, int inform, int nid, char* output, int* outputLength);

// 测试用例
void testPerformStringConversion(void)
{
    const char input[] = { 0xEF, 0xBB, 0xBF, 0xE8, 0xB5, 0xB5 }; // 汉字“赵”的UTF8值
    int inputLength = sizeof(input);
    int inform = MBSTRING_UTF8;
    int nid = NID_commonName;
    char output[100];
    int outputLength;

    // 调用要测试的函数
    int result = performStringConversion(input, inputLength, inform, nid, output, &outputLength);

    // 使用断言进行验证
    CU_ASSERT_TRUE(result);
    CU_ASSERT_TRUE(outputLength > 0);
}

// 初始化测试套件
int init_suite(void)
{
    return 0;
}

// 清理测试套件
int clean_suite(void)
{
    return 0;
}

int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 在测试套件中添加测试用例
    if (CU_add_test(suite, "testPerformStringConversion", testPerformStringConversion) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
