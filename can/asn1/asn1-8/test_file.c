#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/objects.h>

typedef struct Student_st {
    int age;
} Student;

int cmp_func(const void *a, const void *b);


void findAgeInSortedArray(Student *a, int arraySize, int targetAge);

void testFindAgeInSortedArray()
{
    Student a[6];
    a[0].age = 3;
    a[1].age = 56;
    a[2].age = 5;
    a[3].age = 1;
    a[4].age = 3;
    a[5].age = 6;

    // 使用 CUnit 断言函数进行测试
    CU_ASSERT_NOT_EQUAL(bsearch(&a[4], a, 6, sizeof(Student), cmp_func), NULL);

    // 添加更多的测试断言...

    // 如果有错误，使用 CUnit 的错误消息输出
    if (CU_get_number_of_failures() > 0)
    {
        CU_FAIL("测试失败");
    }
}

int main()
{
    // 初始化 CUnit 测试套件
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("FindAgeInSortedArray Suite", NULL, NULL);
    if (suite == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 添加测试用例到套件中
    if (CU_add_test(suite, "findAgeInSortedArray Test", testFindAgeInSortedArray) == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 使用基本的 CUnit 运行测试
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 相关资源
    CU_cleanup_registry();

    return CU_get_error();
}
