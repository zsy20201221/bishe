#include <stdio.h>
#include <stdlib.h>
#include <openssl/objects.h>

typedef struct Student_st {
    int age;
} Student;

int cmp_func(const void *a, const void *b)
{
    const Student *x, *y;
    x = (const Student *)a;
    y = (const Student *)b;

    if (x->age < y->age)
        return -1;
    else if (x->age > y->age)
        return 1;
    else
        return 0;
}

void findAgeInSortedArray(Student *a, int arraySize, int targetAge)
{
    ASN1_OBJECT *obj = NULL;
    Student *sort = (Student *)OPENSSL_malloc(arraySize * sizeof(Student));

    if (sort == NULL)
    {
        fprintf(stderr, "内存分配失败。\n");
        exit(1);
    }

    for (int i = 0; i < arraySize; ++i)
    {
        sort[i] = a[i];
    }

    qsort(sort, arraySize, sizeof(Student), cmp_func);

    obj = OBJ_nid2obj(NID_rsa);
    if (obj == NULL)
    {
        fprintf(stderr, "OBJ_nid2obj 失败。\n");
        OPENSSL_free(sort);
        exit(1);
    }

    int ret = OBJ_obj2nid(obj);
    if (ret == NID_undef)
    {
        fprintf(stderr, "OBJ_nid2obj 返回的对象无效。\n");
        OPENSSL_free(sort);
        exit(1);
    }

    // 在排序后的数组中查找元素
    Student key;
    key.age = targetAge;
    Student *addr = bsearch(&key, sort, arraySize, sizeof(Student), cmp_func);

    if (addr == NULL)
    {
        fprintf(stderr, "查找失败。\n");
    }
    else
    {
        printf("%d == %d\n", targetAge, addr->age);
    }

    OPENSSL_free(sort);
}

/*int main()
{
    Student a[6];
    a[0].age = 3;
    a[1].age = 56;
    a[2].age = 5;
    a[3].age = 1;
    a[4].age = 3;
    a[5].age = 6;

    findAgeInSortedArray(a, 6, a[4].age);

    return 0;
}
*/
/*#include <stdio.h>
#include <stdlib.h>
#include <openssl/objects.h>

typedef struct Student_st {
    int age;
} Student;

int cmp_func(const void *a, const void *b)
{
    const Student *x, *y;
    x = (const Student *)a;
    y = (const Student *)b;

    if (x->age < y->age)
        return -1;
    else if (x->age > y->age)
        return 1;
    else
        return 0;
}

int main()
{
    int ret;
    ASN1_OBJECT *obj = NULL;
    Student a[6], *sort;
    a[0].age = 3;
    a[1].age = 56;
    a[2].age = 5;
    a[3].age = 1;
    a[4].age = 3;
    a[5].age = 6;

    sort = (Student *)OPENSSL_malloc(6 * sizeof(Student));
    if (sort == NULL)
    {
        fprintf(stderr, "内存分配失败。\n");
        exit(1);
    }

    for (int i = 0; i < 6; ++i)
    {
        sort[i] = a[i];
    }

    qsort(sort, 6, sizeof(Student), cmp_func);

    obj = OBJ_nid2obj(NID_rsa);
    if (obj == NULL)
    {
        fprintf(stderr, "OBJ_nid2obj 失败。\n");
        OPENSSL_free(sort);
        exit(1);
    }

    ret = OBJ_obj2nid(obj); // 使用 OBJ_obj2nid 检查对象是否有效
    if (ret == NID_undef)
    {
        fprintf(stderr, "OBJ_nid2obj 返回的对象无效。\n");
        OPENSSL_free(sort);
        exit(1);
    }

    // 在排序后的数组中查找元素
    Student key;
    key.age = a[4].age;
    Student *addr = bsearch(&key, sort, 6, sizeof(Student), cmp_func);

    if (addr == NULL)
    {
        fprintf(stderr, "查找失败。\n");
        OPENSSL_free(sort);
        exit(1);
    }

    printf("%d == %d\n", a[4].age, addr->age);

    OPENSSL_free(sort);
    return 0;
}*/
/*ASN1_OBJECT 类型的对象 obj 并没有被正确初始化。OBJ_nid2obj 返回的是一个指向已存在的 ASN1_OBJECT 对象的指针，而不是创建一个新的对象。*/
