#include <stdio.h>
#include <stdlib.h>
#include <openssl/asn1.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>

int main()
{
    int ret;
    ASN1_INTEGER *a;
    EVP_MD *md;
    EVP_PKEY *pkey;
    char *data;
    ASN1_BIT_STRING *signature = NULL;
    RSA *r;
    int i, bits = 1024;
    unsigned long e = RSA_3;
    BIGNUM *bne;

    /* Initialize OpenSSL */
    OpenSSL_add_all_algorithms();

    bne = BN_new();
    ret = BN_set_word(bne, e);
    if (ret != 1) {
        printf("BN_set_word err!\n");
        BN_free(bne);
        return -1;
    }

    r = RSA_new();
    ret = RSA_generate_key_ex(r, bits, bne, NULL);
    if (ret != 1) {
        printf("RSA_generate_key_ex err!\n");
        RSA_free(r);
        BN_free(bne);
        return -1;
    }

    pkey = EVP_PKEY_new();
    EVP_PKEY_assign_RSA(pkey, r);

    a = ASN1_INTEGER_new();
    ASN1_INTEGER_set(a, 100);

    md = EVP_md5();
    data = (char *)a;

    signature = ASN1_BIT_STRING_new();
    ret = ASN1_sign(i2d_ASN1_INTEGER, NULL, NULL, signature, data, pkey, md);
    if (ret <= 0) {
        printf("ASN1_sign err!\n");
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        ASN1_BIT_STRING_free(signature);
        BN_free(bne);
        return -1;
    }

    printf("signature len: %d\n", ret);

    EVP_PKEY_free(pkey);
    ASN1_INTEGER_free(a);
    ASN1_BIT_STRING_free(signature);
    BN_free(bne);

    /* Cleanup OpenSSL */
    EVP_cleanup();

    return 0;
}

/*#include <openssl/asn1.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
int main()
{
    int ret;
    ASN1_INTEGER *a;
    EVP_MD *md;
    EVP_PKEY *pkey;
    char *data;
    ASN1_BIT_STRING *signature = NULL;
    RSA *r;
    int i, bits = 1024;
    unsigned long e = RSA_3;
    BIGNUM *bne;
    bne = BN_new();
    ret = BN_set_word(bne, e);
    r = RSA_new();
    ret = RSA_generate_key_ex(r, bits, bne, NULL);
    if (ret != 1) {
	printf("RSA_generate_key_ex err!\n");
	return -1;
    }
    pkey = EVP_PKEY_new();
    EVP_PKEY_assign_RSA(pkey, r);
    a = ASN1_INTEGER_new();
    ASN1_INTEGER_set(a, 100);
    md = EVP_md5();
    data = (char *) a;
    signature = ASN1_BIT_STRING_new();
    ret =
	ASN1_sign(i2d_ASN1_INTEGER, NULL, NULL, signature, data, pkey, md);
    printf("signature len : %d\n", ret);
    EVP_PKEY_free(pkey);
    ASN1_INTEGER_free(a);
    free(signature);
    return 0;
}*/
