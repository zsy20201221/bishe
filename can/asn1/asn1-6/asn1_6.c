#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/bio.h>

int performASN1Conversion(long v, char* output, int outputSize)
{
    ASN1_INTEGER *i;
    BIO *bp;
    int result = 0;

    i = ASN1_INTEGER_new();
    if (i == NULL)
    {
        printf("ASN1_INTEGER_new 失败。\n");
        return result;
    }

    if (!ASN1_INTEGER_set(i, v))
    {
        printf("ASN1_INTEGER_set 失败。\n");
        ASN1_INTEGER_free(i);
        return result;
    }

    bp = BIO_new(BIO_s_mem());
    if (bp == NULL)
    {
        printf("BIO_new 失败。\n");
        ASN1_INTEGER_free(i);
        return result;
    }

    if (i2a_ASN1_INTEGER(bp, i) <= 0)
    {
        printf("i2a_ASN1_INTEGER 失败。\n");
    }
    else
    {
        int len = BIO_read(bp, output, outputSize - 1);
        if (len > 0)
        {
            output[len] = '\0';
            result = 1;
        }
    }

    BIO_free(bp);
    ASN1_INTEGER_free(i);

    return result;
}

/*int main()
{
    long v;
    char output[100];

    printf("输入 v 的值:\n");
    if (scanf("%ld", &v) != 1)
    {
        printf("输入值无效。\n");
        return 0;
    }

    if (performASN1Conversion(v, output, sizeof(output)))
    {
        printf("转换结果：%s\n", output);
    }
    else
    {
        printf("转换失败。\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/bio.h>

int main()
{
    ASN1_INTEGER *i;
    long v;
    BIO *bp;
    char buf[100];

    printf("输入 v 的值:\n");
    if (fgets(buf, sizeof(buf), stdin) == NULL)
    {
        printf("输入错误或达到文件结束。\n");
        return 0;
    }
    if (sscanf(buf, "%ld", &v) != 1)
    {
        printf("输入值无效。\n");
        return 0;
    }

    i = ASN1_INTEGER_new();
    if (i == NULL)
    {
        printf("ASN1_INTEGER_new 失败。\n");
        return 0;
    }

    if (!ASN1_INTEGER_set(i, v))
    {
        printf("ASN1_INTEGER_set 失败。\n");
        ASN1_INTEGER_free(i);
        return 0;
    }

    bp = BIO_new(BIO_s_file());
    if (bp == NULL)
    {
        printf("BIO_new 失败。\n");
        ASN1_INTEGER_free(i);
        return 0;
    }

    BIO_set_fp(bp, stdout, BIO_NOCLOSE);
    if (i2a_ASN1_INTEGER(bp, i) <= 0)
    {
        printf("i2a_ASN1_INTEGER 失败。\n");
    }

    BIO_free(bp);
    ASN1_INTEGER_free(i);
    printf("\n");
    return 0;
}*/
