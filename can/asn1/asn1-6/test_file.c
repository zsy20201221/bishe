#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/bio.h>
#include <string.h>

int performASN1Conversion(long v, char* output, int outputSize);

// 测试用例1：测试正常转换
void testPerformASN1Conversion_Normal(void)
{
    long v = 1234567890;
    char output[256];

    int result = performASN1Conversion(v, output, sizeof(output));

    CU_ASSERT_TRUE(result);
    CU_ASSERT_STRING_EQUAL(output, "499602D2");
}

// 测试用例2：测试负数转换
void testPerformASN1Conversion_Negative(void)
{
    long v = -987654321;
    char output[256];

    int result = performASN1Conversion(v, output, sizeof(output));

    CU_ASSERT_TRUE(result);
    CU_ASSERT_STRING_EQUAL(output, "-3ADE68B1");
}

// 测试用例3：测试边界值转换
void testPerformASN1Conversion_Boundary(void)
{
    long v = 0;
    char output[256];

    int result = performASN1Conversion(v, output, sizeof(output));

    CU_ASSERT_TRUE(result);
    CU_ASSERT_STRING_EQUAL(output, "00");
}

// 初始化测试套件
int init_suite(void)
{
    return 0;
}

// 清理测试套件
int clean_suite(void)
{
    return 0;
}

int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 在测试套件中添加测试用例
    if (CU_add_test(suite, "testPerformASN1Conversion_Normal", testPerformASN1Conversion_Normal) == NULL ||
        CU_add_test(suite, "testPerformASN1Conversion_Negative", testPerformASN1Conversion_Negative) == NULL ||
        CU_add_test(suite, "testPerformASN1Conversion_Boundary", testPerformASN1Conversion_Boundary) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
