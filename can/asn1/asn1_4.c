#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>
int main()
{
    int inlen, nid, inform, len, ret;
    char in[100], out[100], *p;
    ASN1_STRING *a;
    FILE *fp;
/* 汉字“赵”的UTF8值,可以用UltraEdit获取*/
    memset(&in[0], 0xEF, 1);
    memset(&in[1], 0xBB, 1);
    memset(&in[2], 0xBF, 1);
    memset(&in[3], 0xE8, 1);
    memset(&in[4], 0xB5, 1);
    memset(&in[5], 0xB5, 1);
    inlen = 6;
    inform = MBSTRING_UTF8;
    nid = NID_commonName;
/* 如果调用下面两个函数，生成的ASN1_STRING类型将是ASN1_UTF8而不是
ASN1_BMPSTRING */
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    ret = ASN1_STRING_set_default_mask_asc("utf8only");
    if (ret != 1) {
	printf("ASN1_STRING_set_default_mask_asc err.\n");
	return 0;
    }
    a = ASN1_STRING_set_by_NID(NULL, in, inlen, inform, nid);
    p = out;
    len = i2d_ASN1_BMPSTRING(a, &p);
    fp = fopen("a.cer", "w");
    fwrite(out, 1, len, fp);
    fclose(fp);
    ASN1_STRING_free(a);
    return 0;
}
