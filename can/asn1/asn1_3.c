#include <openssl/asn1.h>
int main()
{
    int ret;
    ASN1_STRING *a, *b, *c;
    a = ASN1_STRING_new();
    b = ASN1_STRING_new();
    ASN1_STRING_set(a, "abc", 3);
    ASN1_STRING_set(b, "def", 3);
    ret = ASN1_STRING_cmp(a, b);
    printf("%d\n", ret);
    c = ASN1_STRING_dup(a);
    ret = ASN1_STRING_cmp(a, c);
    printf("%d\n", ret);
    ASN1_STRING_free(a);
    ASN1_STRING_free(b);
    ASN1_STRING_free(c);
    return 0;
}
