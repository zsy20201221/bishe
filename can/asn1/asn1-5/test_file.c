#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

void testProcessASN1String()
{
    const char *input = "ab";
    int inputLength = 2;
    char output[100];
    int outputLength, type;

    processASN1String(input, inputLength, output, &outputLength, &type);

    // Perform assertions
    CU_ASSERT_EQUAL(type, V_ASN1_UTF8STRING);
    CU_ASSERT_EQUAL(outputLength, inputLength);
    CU_ASSERT_NSTRING_EQUAL(output, input, inputLength);
}

int main()
{
    CU_pSuite suite = NULL;

    // Initialize CUnit test framework
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // Create a test suite
    suite = CU_add_suite("ASN1Suite", NULL, NULL);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (CU_add_test(suite, "processASN1String Test", testProcessASN1String) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Set output mode to verbose
    CU_basic_set_mode(CU_BRM_VERBOSE);

    // Run the tests
    CU_basic_run_tests();

    // Clean up CUnit resources
    CU_cleanup_registry();

    return CU_get_error();
}
