#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

void processASN1String(const char *input, int inputLength, char *output, int *outputLength, int *type) {
    ASN1_STRING *a;

    /* 设置生成的ASN1_STRING类型 */
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    a = ASN1_STRING_set_by_NID(NULL, input, inputLength, MBSTRING_ASC, NID_commonName);

    // 获取实际的字符串类型
    *type = ASN1_STRING_type(a);

    *outputLength = ASN1_STRING_length(a);
    memcpy(output, ASN1_STRING_get0_data(a), *outputLength);

    ASN1_STRING_free(a);
}

/*int main() {
    int inlen = 2, len, type;
    char in[100], out[100];

    strncpy(in, "ab", sizeof(in) - 1);  // 使用strncpy避免缓冲区溢出

    processASN1String(in, inlen, out, &len, &type);

    switch (type) {
    case V_ASN1_T61STRING:
        printf("V_ASN1_T61STRING\n");
        break;
    case V_ASN1_IA5STRING:
        printf("V_ASN1_IA5STRING\n");
        break;
    case V_ASN1_PRINTABLESTRING:
        printf("V_ASN1_PRINTABLESTRING\n");
        break;
    case V_ASN1_BMPSTRING:
        printf("V_ASN1_BMPSTRING\n");
        break;
    case V_ASN1_UNIVERSALSTRING:
        printf("V_ASN1_UNIVERSALSTRING\n");
        break;
    case V_ASN1_UTF8STRING:
        printf("V_ASN1_UTF8STRING\n");
        break;
    default:
        printf("Unknown type: %d\n", type);
        break;
    }

    FILE *fp = fopen("a.cer", "w");
    fwrite(out, 1, len, fp);
    fclose(fp);

    getchar();
    return 0;
}
*/
/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>

int main() {
    int inlen, nid, inform, len;
    char in[100], out[100], *p;
    ASN1_STRING *a;
    FILE *fp;

    strncpy(in, "ab", sizeof(in) - 1);  // 使用strncpy避免缓冲区溢出
    inlen = 2;
    inform = MBSTRING_ASC;
    nid = NID_commonName;

    // 设置生成的ASN1_STRING类型 
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    a = ASN1_STRING_set_by_NID(NULL, in, inlen, inform, nid);

    // 获取实际的字符串类型
    int type = ASN1_STRING_type(a);
    switch (type) {
    case V_ASN1_T61STRING:
        printf("V_ASN1_T61STRING\n");
        break;
    case V_ASN1_IA5STRING:
        printf("V_ASN1_IA5STRING\n");
        break;
    case V_ASN1_PRINTABLESTRING:
        printf("V_ASN1_PRINTABLESTRING\n");
        break;
    case V_ASN1_BMPSTRING:
        printf("V_ASN1_BMPSTRING\n");
        break;
    case V_ASN1_UNIVERSALSTRING:
        printf("V_ASN1_UNIVERSALSTRING\n");
        break;
    case V_ASN1_UTF8STRING:
        printf("V_ASN1_UTF8STRING\n");
        break;
    default:
        printf("Unknown type: %d\n", type);
        break;
    }

    p = out;
    len = ASN1_STRING_length(a);
    memcpy(p, ASN1_STRING_get0_data(a), len);

    fp = fopen("a.cer", "w");
    fwrite(out, 1, len, fp);
    fclose(fp);
    ASN1_STRING_free(a);
    getchar();
    return 0;
}*/


/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/obj_mac.h>
int main()
{
    int inlen, nid, inform, len;
    char in[100], out[100], *p;
    ASN1_STRING *a;
    FILE *fp;
    strcpy(in, "ab");
    inlen = 2;
    inform = MBSTRING_ASC;
    nid = NID_commonName;
// 设置生成的ASN1_STRING类型 
    ASN1_STRING_set_default_mask(B_ASN1_UTF8STRING);
    a = ASN1_STRING_set_by_NID(NULL, in, inlen, inform, nid);
    switch (a->type) {
    case V_ASN1_T61STRING:
	printf("V_ASN1_T61STRING\n");
	break;
    case V_ASN1_IA5STRING:
	printf("V_ASN1_IA5STRING\n");
	break;
    case V_ASN1_PRINTABLESTRING:
	printf("V_ASN1_PRINTABLESTRING\n");
	break;
    case V_ASN1_BMPSTRING:
	printf("V_ASN1_BMPSTRING\n");
	break;
    case V_ASN1_UNIVERSALSTRING:
	printf("V_ASN1_UNIVERSALSTRING\n");
	break;
    case V_ASN1_UTF8STRING:
	printf("V_ASN1_UTF8STRING\n");
	break;
    default:
	printf("err");
	break;
    }
    p = out;
//len=i2d_ASN1_bytes(a,&p,a->type,V_ASN1_UNIVERSAL);
//len = i2d_X509(a, &p);
    len = i2d_X509_CINF(a->type, &p);
    fp = fopen("a.cer", "w");
    fwrite(out, 1, len, fp);
    fclose(fp);
    ASN1_STRING_free(a);
    getchar();
    return 0;
}*/
