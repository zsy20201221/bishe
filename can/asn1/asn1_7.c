#include <openssl/asn1.h>
#include <openssl/asn1t.h>
int main()
{
    ASN1_STRING *a;
    BIO *bp;
    a = ASN1_STRING_new();
    ASN1_STRING_set(a, "测试", 4);
    bp = BIO_new(BIO_s_file());
    BIO_set_fp(bp, stdout, BIO_NOCLOSE);
    i2a_ASN1_STRING(bp, a, 1);
    BIO_free(bp);
    ASN1_STRING_free(a);
    printf("\n");
    return 0;
}
