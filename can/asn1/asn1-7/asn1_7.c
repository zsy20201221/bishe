#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/bio.h>
#include <string.h>

int performASN1Conversion(const char* input, int inputSize, char* output, int outputSize)
{
    ASN1_STRING *a;
    BIO *bp;
    int result = 0;

    a = ASN1_STRING_new();
    if (a == NULL)
    {
        printf("ASN1_STRING_new 失败。\n");
        return result;
    }

    if (!ASN1_STRING_set(a, input, inputSize))
    {
        printf("ASN1_STRING_set 失败。\n");
        ASN1_STRING_free(a);
        return result;
    }

    bp = BIO_new(BIO_s_mem());
    if (bp == NULL)
    {
        printf("BIO_new 失败。\n");
        ASN1_STRING_free(a);
        return result;
    }

    if (i2a_ASN1_STRING(bp, a, 1) <= 0)
    {
        printf("i2a_ASN1_STRING 失败。\n");
    }
    else
    {
        int len = BIO_read(bp, output, outputSize - 1);
        if (len > 0)
        {
            output[len] = '\0';
            result = 1;
        }
    }

    BIO_free(bp);
    ASN1_STRING_free(a);

    return result;
}

/*int main()
{
    const char* input = "测试";
    char output[100];

    if (performASN1Conversion(input, strlen(input), output, sizeof(output)))
    {
        printf("转换结果：%s\n", output);
    }
    else
    {
        printf("转换失败。\n");
    }

    return 0;
}*/

/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/bio.h>

int main()
{
    ASN1_STRING *a;
    BIO *bp;

    a = ASN1_STRING_new();
    if (a == NULL)
    {
        printf("ASN1_STRING_new 失败。\n");
        return 0;
    }

    if (!ASN1_STRING_set(a, "测试", strlen("测试")))
    {
        printf("ASN1_STRING_set 失败。\n");
        ASN1_STRING_free(a);
        return 0;
    }

    bp = BIO_new(BIO_s_file());
    if (bp == NULL)
    {
        printf("BIO_new 失败。\n");
        ASN1_STRING_free(a);
        return 0;
    }

    BIO_set_fp(bp, stdout, BIO_NOCLOSE);
    if (i2a_ASN1_STRING(bp, a, 1) <= 0)
    {
        printf("i2a_ASN1_STRING 失败。\n");
    }

    BIO_free(bp);
    ASN1_STRING_free(a);
    printf("\n");
    return 0;
}*/
