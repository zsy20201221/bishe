#include <stdio.h>
#include <string.h>
#include <openssl/asn1.h>
#include <openssl/asn1t.h>

int compareStrings(const char* input1, const char* input2, int* result)
{
    ASN1_OCTET_STRING *a, *b;
    a = ASN1_OCTET_STRING_new();
    b = ASN1_OCTET_STRING_new();

    ASN1_OCTET_STRING_set(a, input1, strlen(input1));
    ASN1_OCTET_STRING_set(b, input2, strlen(input2));

    *result = strcmp((char *)a->data, (char *)b->data);

    ASN1_OCTET_STRING_free(a);
    ASN1_OCTET_STRING_free(b);

    return 0;
}

/*int main()
{
    char input1[100];
    char input2[100];
    int comparisonResult;

    printf("Enter the first string: ");
    fgets(input1, sizeof(input1), stdin);
    input1[strcspn(input1, "\n")] = '\0';  // 移除输入字符串中的换行符

    printf("Enter the second string: ");
    fgets(input2, sizeof(input2), stdin);
    input2[strcspn(input2, "\n")] = '\0';  // 移除输入字符串中的换行符

    int result = compareStrings(input1, input2, &comparisonResult);
    if (result == 0) {
        printf("Comparison result: %d\n", comparisonResult);
    } else {
        printf("String comparison failed\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/asn1t.h>

int main()
{
    int ret;
    ASN1_OCTET_STRING *a, *b, *c;
    a = ASN1_OCTET_STRING_new();
    b = ASN1_OCTET_STRING_new();
    ASN1_OCTET_STRING_set(a, "abc", 3);
    ASN1_OCTET_STRING_set(b, "def", 3);

    ret = strcmp((char *)a->data, (char *)b->data);
    printf("%d\n", ret);

    c = ASN1_OCTET_STRING_dup(a);
    ret = strcmp((char *)a->data, (char *)c->data);
    printf("%d\n", ret);

    ASN1_OCTET_STRING_free(a);
    ASN1_OCTET_STRING_free(b);
    ASN1_OCTET_STRING_free(c);
  
    return 0;
}*/
