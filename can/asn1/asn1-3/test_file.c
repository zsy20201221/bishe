#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <string.h>
#include <openssl/asn1.h>
#include <openssl/asn1t.h>

// 声明要测试的函数
int compareStrings(const char* input1, const char* input2, int* result);

// 测试用例
void testCompareStrings(void)
{
    const char* input1 = "Hello";
    const char* input2 = "World";
    int comparisonResult;

    // 调用要测试的函数
    int result = compareStrings(input1, input2, &comparisonResult);

    // 使用断言进行验证
    CU_ASSERT_EQUAL(result, 0);
    CU_ASSERT_TRUE(comparisonResult < 0);
}

// 初始化测试套件
int init_suite(void)
{
    return 0;
}

// 清理测试套件
int clean_suite(void)
{
    return 0;
}

int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 在测试套件中添加测试用例
    if (CU_add_test(suite, "testCompareStrings", testCompareStrings) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
