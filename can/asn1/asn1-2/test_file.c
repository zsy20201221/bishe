#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <openssl/asn1.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>

// 声明要测试的函数
int generateSignature(int bits, unsigned long e, int* signatureLength);

// 测试用例
void testGenerateSignature(void)
{
    int bits = 1024;
    unsigned long e = RSA_3;
    int signatureLength;

    // 调用要测试的函数
    int result = generateSignature(bits, e, &signatureLength);

    // 使用断言进行验证
    CU_ASSERT_EQUAL(result, 0);
    CU_ASSERT_TRUE(signatureLength > 0);
}

// 初始化测试套件
int init_suite(void)
{
    return 0;
}

// 清理测试套件
int clean_suite(void)
{
    return 0;
}

int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 在测试套件中添加测试用例
    if (CU_add_test(suite, "testGenerateSignature", testGenerateSignature) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
