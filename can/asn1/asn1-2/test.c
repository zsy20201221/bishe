#include <stdio.h>
#include <stdlib.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

int generateSignature(int bits, unsigned long e, int* signatureLength)
{
    int ret;
    EVP_PKEY *pkey;
    RSA *r;
    BIGNUM *bne;
    unsigned char digest[MD5_DIGEST_LENGTH];
    unsigned int digestLen;

    /* Initialize OpenSSL */
    OpenSSL_add_all_algorithms();

    bne = BN_new();
    ret = BN_set_word(bne, e);
    if (ret != 1) {
        printf("BN_set_word err!\n");
        BN_free(bne);
        return -1;
    }

    r = RSA_new();
    ret = RSA_generate_key_ex(r, bits, bne, NULL);
    if (ret != 1) {
        printf("RSA_generate_key_ex err!\n");
        RSA_free(r);
        BN_free(bne);
        return -1;
    }

    pkey = EVP_PKEY_new();
    EVP_PKEY_assign_RSA(pkey, r);

    // Create the ASN1_INTEGER data
    ASN1_INTEGER *a = ASN1_INTEGER_new();
    ASN1_INTEGER_set(a, 100);

    // Encode the ASN1_INTEGER data and calculate the length
    int dataLen = i2d_ASN1_INTEGER(a, NULL);
    if (dataLen <= 0) {
        printf("Encoding ASN1_INTEGER failed!\n");
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        BN_free(bne);
        return -1;
    }

    // Allocate memory for the encoded data
    unsigned char *data = (unsigned char *)malloc(dataLen);
    if (data == NULL) {
        printf("Memory allocation failed!\n");
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        BN_free(bne);
        return -1;
    }

    // Encode the ASN1_INTEGER data
    unsigned char *p = data;
    ret = i2d_ASN1_INTEGER(a, &p);
    if (ret <= 0) {
        printf("Encoding ASN1_INTEGER failed!\n");
        free(data);
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        BN_free(bne);
        return -1;
    }

    // Calculate the MD5 digest of the data
    MD5(data, dataLen, digest);
    digestLen = sizeof(digest);

    // Allocate memory for the signature
    unsigned char *signature = (unsigned char *)malloc(RSA_size(r));
    if (signature == NULL) {
        printf("Memory allocation failed!\n");
        free(data);
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        BN_free(bne);
        return -1;
    }

    // Sign the digest using RSA private key
    ret = RSA_sign(NID_md5, digest, digestLen, signature, signatureLength, r);
    if (ret != 1) {
        printf("RSA_sign err!\n");
        free(signature);
        free(data);
        EVP_PKEY_free(pkey);
        ASN1_INTEGER_free(a);
        BN_free(bne);
        return -1;
    }

    EVP_PKEY_free(pkey);
    ASN1_INTEGER_free(a);
    BN_free(bne);

    /* Cleanup OpenSSL */
    EVP_cleanup();

    return 0;
}

int main()
{
    int bits = 1024;
    unsigned long e = RSA_3;
    int signatureLength;

    int result = generateSignature(bits, e, &signatureLength);
    if (result == 0) {
        printf("Signature length: %d\n", signatureLength);
    } else {
        printf("Signature generation failed\n");
    }

    return 0;
}
