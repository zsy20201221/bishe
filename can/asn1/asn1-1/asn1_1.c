#include <stdio.h>
#include <stdlib.h>
#include <openssl/bio.h>
#include <openssl/asn1.h>

#define MAX_PATH_LENGTH 256

// 解析和打印ASN.1数据的函数
int parseAndPrintASN1(const char *file_path)
{
    int ret, len, indent, dump;
    BIO *bp;
    char *pp, buf[5000];
    FILE *fp;

    // 创建一个BIO对象，用于将输出重定向到标准输出
    bp = BIO_new(BIO_s_file());
    BIO_set_fp(bp, stdout, BIO_NOCLOSE);

    // 打开文件
    fp = fopen(file_path, "rb");
    if (fp == NULL) {
        printf("无法打开文件。\n");
        return 1; // 返回适当的错误代码
    }

    // 读取文件内容并存储在缓冲区中
    len = fread(buf, 1, sizeof(buf), fp);
    if (len < 0) {
        printf("无法读取文件。\n");
        fclose(fp);
        return 1; // 返回适当的错误代码
    }
    fclose(fp);

    // 设置解析和打印的参数
    pp = buf;
    indent = 7; // 缩进级别
    dump = 11;  // 打印的字符数

    // 解析和打印ASN.1数据
    ret = ASN1_parse_dump(bp, pp, len, indent, dump);

    // 释放BIO资源
    BIO_free(bp);

    return 0;
}

/*int main()
{
    char file_path[MAX_PATH_LENGTH];

    printf("请输入文件路径：");
    // 从标准输入中读取文件路径
    if (fgets(file_path, MAX_PATH_LENGTH, stdin) == NULL) {
        printf("无法读取文件路径。\n");
        return 1; // 返回适当的错误代码
    }

    // 移除文件路径字符串末尾的换行符
    if (file_path[strlen(file_path) - 1] == '\n')
        file_path[strlen(file_path) - 1] = '\0';

    // 调用解析和打印ASN.1数据的函数，并传递文件路径
    int result = parseAndPrintASN1(file_path);

    return result;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <openssl/bio.h>
#include <openssl/asn1.h>

#define MAX_PATH_LENGTH 256

int main()
{
    int ret, len, indent, dump;
    BIO *bp;
    char *pp, buf[5000];
    FILE *fp;
    char file_path[MAX_PATH_LENGTH];

    bp = BIO_new(BIO_s_file());
    BIO_set_fp(bp, stdout, BIO_NOCLOSE);

    printf("请输入文件路径：");
    if (fgets(file_path, MAX_PATH_LENGTH, stdin) == NULL) {
        printf("无法读取文件路径。\n");
        return 1; // 返回适当的错误代码
    }

    // 移除文件路径字符串末尾的换行符
    if (file_path[strlen(file_path) - 1] == '\n')
        file_path[strlen(file_path) - 1] = '\0';

    fp = fopen(file_path, "rb");
    if (fp == NULL) {
        printf("无法打开文件。\n");
        return 1; // 返回适当的错误代码
    }

    len = fread(buf, 1, sizeof(buf), fp);
    if (len < 0) {
        printf("无法读取文件。\n");
        fclose(fp);
        return 1; // 返回适当的错误代码
    }
    fclose(fp);

    pp = buf;
    indent = 7;
    dump = 11;
    ret = ASN1_parse_dump(bp, pp, len, indent, dump);

    BIO_free(bp);

    return 0;
}
*/
/*yuan dai ma #include <openssl/bio.h>
#include <openssl/asn1.h>
int main()
{
    int ret, len, indent, dump;
    BIO *bp;
    char *pp, buf[5000];
    FILE *fp;
    bp = BIO_new(BIO_s_file());
    BIO_set_fp(bp, stdout, BIO_NOCLOSE);
    fp = fopen("/path/to/der.cer", "rb");
    len = fread(buf, 1, 5000, fp);
    fclose(fp);
    pp = buf;
    indent = 7;
    dump = 11;
    ret = ASN1_parse_dump(bp, pp, len, indent, dump);
    BIO_free(bp);
    return 0;
}*/
