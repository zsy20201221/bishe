#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include "asn1_1.c"

// 声明要测试的函数
int parseAndPrintASN1(const char *file_path);

// 测试用例
void test_parseAndPrintASN1(void)
{
    const char* test_file = "/path/to/der.cer";

    // 调用要测试的函数
    int result = parseAndPrintASN1(test_file);

    // 使用断言进行验证
    CU_ASSERT_EQUAL(result, 0);
}

// 初始化测试套件
int init_suite(void)
{
    return 0;
}

// 清理测试套件
int clean_suite(void)
{
    return 0;
}

int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 在测试套件中添加测试用例
    if (CU_add_test(suite, "test_parseAndPrintASN1", test_parseAndPrintASN1) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
