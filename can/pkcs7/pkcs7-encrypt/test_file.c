#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/pkcs7.h>

int init_suite(void) {
    return 0;
}

int clean_suite(void) {
    return 0;
}

void test_generatePKCS7(void) {
    const char *outputFileName = "p7_enc.cer";
    int result = generatePKCS7(outputFileName);
    CU_ASSERT_EQUAL(result, 0);
}

int main() {
    CU_pSuite pSuite = NULL;

    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    pSuite = CU_add_suite("Suite", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_generatePKCS7", test_generatePKCS7)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}
