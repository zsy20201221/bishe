#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>

int generatePKCS7(const char *outputFileName)
{
    PKCS7 *p7;
    int ret, len;
    char *der;
    unsigned char *p;
    FILE *fp;

    p7 = PKCS7_new();
    if (p7 == NULL) {
        printf("Failed to create PKCS7 object.\n");
        return 1;
    }

    ret = PKCS7_set_type(p7, NID_pkcs7_encrypted);
    if (ret != 1) {
        printf("Failed to set PKCS7 type.\n");
        PKCS7_free(p7);
        return 1;
    }

    ASN1_INTEGER_set(p7->d.encrypted->version, 3);

    p7->d.encrypted->enc_data->algorithm->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    p7->d.encrypted->enc_data->enc_data = ASN1_OCTET_STRING_new();
    if (p7->d.encrypted->enc_data->enc_data == NULL) {
        printf("Failed to create ASN1_OCTET_STRING object.\n");
        PKCS7_free(p7);
        return 1;
    }

    ASN1_OCTET_STRING_set(p7->d.encrypted->enc_data->enc_data, (const unsigned char *)"3434", 4);

    len = i2d_PKCS7(p7, NULL);
    der = (char *)malloc(len);
    if (der == NULL) {
        printf("Memory allocation failed.\n");
        PKCS7_free(p7);
        return 1;
    }

    p = (unsigned char *)der;
    len = i2d_PKCS7(p7, &p);
    if (len < 0) {
        printf("Failed to encode PKCS7 object.\n");
        PKCS7_free(p7);
        free(der);
        return 1;
    }

    fp = fopen(outputFileName, "wb");
    if (fp == NULL) {
        printf("Failed to open output file.\n");
        PKCS7_free(p7);
        free(der);
        return 1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);
    free(der);

    return 0;
}

/*int main()
{
    const char *outputFileName = "p7_enc.cer";

    int result = generatePKCS7(outputFileName);
    if (result == 0) {
        printf("PKCS7 generation successful.\n");
    } else {
        printf("PKCS7 generation failed.\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>

int main()
{
    PKCS7 *p7;
    int ret, len;
    char *der;
    unsigned char *p;
    FILE *fp;

    p7 = PKCS7_new();
    if (p7 == NULL) {
        printf("Failed to create PKCS7 object.\n");
        return 1;
    }

    ret = PKCS7_set_type(p7, NID_pkcs7_encrypted);
    if (ret != 1) {
        printf("Failed to set PKCS7 type.\n");
        PKCS7_free(p7);
        return 1;
    }

    ASN1_INTEGER_set(p7->d.encrypted->version, 3);

    p7->d.encrypted->enc_data->algorithm->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    p7->d.encrypted->enc_data->enc_data = ASN1_OCTET_STRING_new();
    if (p7->d.encrypted->enc_data->enc_data == NULL) {
        printf("Failed to create ASN1_OCTET_STRING object.\n");
        PKCS7_free(p7);
        return 1;
    }

    ASN1_OCTET_STRING_set(p7->d.encrypted->enc_data->enc_data, (const unsigned char *)"3434", 4);

    len = i2d_PKCS7(p7, NULL);
    der = (char *)malloc(len);
    if (der == NULL) {
        printf("Memory allocation failed.\n");
        PKCS7_free(p7);
        return 1;
    }

    p = (unsigned char *)der;
    len = i2d_PKCS7(p7, &p);
    if (len < 0) {
        printf("Failed to encode PKCS7 object.\n");
        PKCS7_free(p7);
        free(der);
        return 1;
    }

    fp = fopen("p7_enc.cer", "wb");
    if (fp == NULL) {
        printf("Failed to open output file.\n");
        PKCS7_free(p7);
        free(der);
        return 1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);
    free(der);

    return 0;
}*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
int main()
{
PKCS7 *p7;
int ret,len;
char *der,*p;
FILE *fp;
p7=PKCS7_new();
ret=PKCS7_set_type(p7,NID_pkcs7_encrypted);
ASN1_INTEGER_set(p7->d.encrypted->version,3);
p7->d.encrypted->enc_data->algorithm->algorithm=OBJ_nid2obj(NID_des_ede3_cbc);
p7->d.encrypted->enc_data->enc_data=ASN1_OCTET_STRING_new();
ASN1_OCTET_STRING_set(p7->d.encrypted->enc_data->enc_data,(const 
unsigned char *)"3434",4);
len=i2d_PKCS7(p7,NULL);
der=(char *)malloc(len);
p=der;
len=i2d_PKCS7(p7,(unsigned char **)&p);
fp=fopen("p7_enc.cer","wb");
fwrite(der,1,len,fp);
fclose(fp);
PKCS7_free(p7);
free(der);
return 0;
}*/
