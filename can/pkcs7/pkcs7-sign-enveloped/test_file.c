// test_file.c

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// 测试套件初始化函数
int init_suite(void) {
    return 0;
}

// 测试套件清理函数
int clean_suite(void) {
    return 0;
}

// 测试 generatePKCS7 函数
void test_generatePKCS7(void) {
    const char* filename = "test_output.p7";
    unsigned char* der = generatePKCS7(filename);

    // 断言生成的 DER 编码数据不为 NULL
   // CU_ASSERT_PTR_NOT_NULL(der);

    // 打开生成的文件，读取其中的内容进行验证
    FILE* fp = fopen(filename, "rb");
    CU_ASSERT_PTR_NOT_NULL(fp);

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // 读取文件内容
    unsigned char* file_content = (unsigned char*)malloc(file_size);
    fread(file_content, 1, file_size, fp);
    fclose(fp);

    // 检查文件内容是否与生成的 DER 编码数据相同
    CU_ASSERT_NSTRING_EQUAL(file_content, der, file_size);

    // 清理
    free(file_content);
    remove(filename);
    free(der);
}

// 主函数设置并运行测试
int main() {
    // 初始化 CUnit 测试注册表
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // 添加测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 添加测试用例
    if (CU_add_test(suite, "测试 generatePKCS7 函数", test_generatePKCS7) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 使用基本接口运行所有测试
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理
    CU_cleanup_registry();

    return CU_get_error();
}

