#include <stdio.h>
#include <stdlib.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

// 生成 PKCS7 对象的 DER 编码数据，并写入文件
// 返回值为 DER 编码数据的指针，调用方需要负责释放该内存
unsigned char* generatePKCS7(const char* filename)
{
    PKCS7* p7;
    int len;
    unsigned char* der;
    unsigned char* p;
    FILE* fp;

    p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_enveloped);

    len = i2d_PKCS7(p7, NULL);
    der = (unsigned char*)malloc(len);
    p = der;
    len = i2d_PKCS7(p7, &p);

    fp = fopen(filename, "wb");
    if (fp == NULL) {
        printf("Failed to open output file.\n");
        PKCS7_free(p7);
        free(der);
        return NULL;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);

    return der;
}



/*int main()
{
    const char* filename = "p7_signAndEnv.cer";
    unsigned char* der = generatePKCS7(filename);

    if (der != NULL) {
        printf("PKCS7 generated and written to file: %s\n", filename);
        free(der);
    }

    return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int main()
{
    PKCS7 *p7;
    int len;
    unsigned char *der, *p;
    FILE *fp;

    p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_enveloped);

    len = i2d_PKCS7(p7, NULL);
    der = (unsigned char *)malloc(len);
    p = der;
    len = i2d_PKCS7(p7, &p);

    fp = fopen("p7_signAndEnv.cer", "wb");
    if (fp == NULL) {
        printf("Failed to open output file.\n");
        PKCS7_free(p7);
        free(der);
        return 1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    // 释放资源
    PKCS7_free(p7);
    free(der);

    return 0;
}*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
int main()
{
PKCS7 *p7;
int len;
char *der,*p;
FILE *fp;
p7=PKCS7_new();
PKCS7_set_type(p7,NID_pkcs7_signedAndEnveloped);
len=i2d_PKCS7(p7,NULL);
der=(char *)malloc(len);
p=der;
len=i2d_PKCS7(p7,(unsigned char **)&p);
fp=fopen("p7_singAndEnv.cer","wb");
fwrite(der,1,len,fp);
fclose(fp);
PKCS7_free(p7);
free(der);
return 0;
}
*/
