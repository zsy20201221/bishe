#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/pem.h>

int readPKCS7Object(const char *filename, PKCS7 **p7) {
    BIO *b = BIO_new_file(filename, "r");
    if (b == NULL) {
        printf("Failed to open file.\n");
        return 1;
    }

    *p7 = PEM_read_bio_PKCS7(b, NULL, NULL, NULL);
    if (*p7 == NULL) {
        printf("Failed to read PKCS7 object.\n");
        BIO_free(b);
        return 1;
    }

    BIO_free(b);
    return 0;
}

void testReadPKCS7Object() {
    const char *filename = "p7Digest.pem";
    PKCS7 *p7 = NULL;

    int result = readPKCS7Object(filename, &p7);

    // Test assertions
    CU_ASSERT_EQUAL(result, 0);
    CU_ASSERT_PTR_NOT_NULL(p7);

    PKCS7_free(p7);
}

int main() {
    // Initialize CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    // Add a suite to the registry
    CU_pSuite suite = CU_add_suite("ReadPKCS7Object Suite", NULL, NULL);
    if (NULL == suite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (NULL == CU_add_test(suite, "testReadPKCS7Object", testReadPKCS7Object)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Set output verbosity and run the tests
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // Clean up the registry
    CU_cleanup_registry();

    return CU_get_error();
}
