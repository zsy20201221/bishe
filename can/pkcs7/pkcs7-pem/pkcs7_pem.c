#include <stdio.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/pem.h>

int readPKCS7Object(const char *filename, PKCS7 **p7) {
    BIO *b = BIO_new_file(filename, "r");
    if (b == NULL) {
        printf("Failed to open file.\n");
        return 1;
    }

    *p7 = PEM_read_bio_PKCS7(b, NULL, NULL, NULL);
    if (*p7 == NULL) {
        printf("Failed to read PKCS7 object.\n");
        BIO_free(b);
        return 1;
    }

    BIO_free(b);
    return 0;
}

int main() {
    PKCS7 *p7;

    if (readPKCS7Object("p7Digest.pem", &p7) != 0) {
        printf("Error reading PKCS7 object.\n");
        return 1;
    }

    PKCS7_free(p7);
    printf("PKCS7 object successfully read.\n");
    return 0;
}
/*#include <stdio.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/pem.h>

int main()
{
    BIO *b;
    PKCS7 *p7;

    b = BIO_new_file("p7Digest.pem", "r");
    if (b == NULL) {
        printf("Failed to open file.\n");
        return 1;
    }

    p7 = PEM_read_bio_PKCS7(b, NULL, NULL, NULL);
    if (p7 == NULL) {
        printf("Failed to read PKCS7 object.\n");
        BIO_free(b);
        return 1;
    }

    BIO_free(b);
    PKCS7_free(p7);
    printf("PKCS7 object successfully read.\n");
    return 0;
}*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/pem.h>
int main()
{
BIO *b;
PKCS7 *p7;
b=BIO_new_file("p7Digest.pem","r");
p7=PEM_read_bio_PKCS7(b,NULL,NULL,NULL);
BIO_free(b);
PKCS7_free(p7);
return 0;
}
*/
