生成自签名的测试证书可以使用 OpenSSL 工具。以下是一个简单的步骤来生成自签名证书：

生成私钥：
使用以下命令生成一个私钥文件（privatekey.pem）：

bash
Copy code
openssl genpkey -algorithm RSA -out privatekey.pem
你可以选择其他算法，这里使用的是 RSA 算法。

生成证书请求：
使用以下命令生成证书请求文件（certreq.csr）：

bash
Copy code
openssl req -new -key privatekey.pem -out certreq.csr
在执行此命令时，你将需要提供一些关于证书的信息，例如国家、组织、通用名称等。填写完信息后，将生成证书请求文件。

使用私钥签名证书请求：
使用以下命令使用先前生成的私钥签名证书请求，并生成自签名的证书文件（b64cert.cer）：

bash
Copy code
openssl x509 -req -in certreq.csr -signkey privatekey.pem -out b64cert.cer
这将使用私钥对证书请求进行签名，生成自签名的 X.509 证书。

现在，你已经生成了一个自签名的测试证书文件 b64cert.cer 和私钥文件 privatekey.pem。你可以将这些文件用于测试你的程序。请注意，这个测试证书仅用于开发和测试目的，不应用于生产环境。在生产环境中，你应该使用由可信的证书颁发机构（CA）签发的证书。
