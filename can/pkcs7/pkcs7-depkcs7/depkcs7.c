#include <stdio.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int parsePKCS7Data(const char* filename)
{
    PKCS7 *p7 = NULL;
    int ret, len;
    char buf[1000], *p, name[1000];
    FILE *fp;
    
    fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Failed to open PKCS7 data file.\n");
        return 1;
    }
    
    len = fread(buf, 1, 1000, fp);
    fclose(fp);
    
    p = buf;
    p7 = d2i_PKCS7(NULL, (const unsigned char **)&p, len);
    if (p7 == NULL) {
        printf("Failed to parse PKCS7 data.\n");
        return 1;
    }
    
    ret = OBJ_obj2txt(name, 1000, p7->type, 0);
    if (ret == -1) {
        printf("Failed to convert object to text.\n");
        PKCS7_free(p7);
        return 1;
    }
    
    printf("type: %s\n", name);
    
    PKCS7_free(p7);
    
    return 0;
}

/*int main()
{
    const char* filename = "out.p7";
    
    int result = parsePKCS7Data(filename);
    
    return result;
}*/
/*#include <stdio.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int main()
{
    PKCS7 *p7 = NULL;
    int ret, len;
    char buf[1000], *p, name[1000];
    FILE *fp;
    
    fp = fopen("out.p7", "rb");
    if (fp == NULL) {
        printf("Failed to open certificate file.\n");
        return 1;
    }
    
    len = fread(buf, 1, 1000, fp);
    fclose(fp);
    
    p = buf;
    p7 = d2i_PKCS7(NULL, (const unsigned char **)&p, len);
    if (p7 == NULL) {
        printf("Failed to parse PKCS7 data.\n");
        return 1;
    }
    
    ret = OBJ_obj2txt(name, 1000, p7->type, 0);
    if (ret == -1) {
        printf("Failed to convert object to text.\n");
        PKCS7_free(p7);
        return 1;
    }
    
    printf("type: %s\n", name);
    
    PKCS7_free(p7);
    
    return 0;
}
*/
/*#include <stdio.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int main()
{
    PKCS7* p7 = NULL;
    int ret, len;
    char buf[1000], *p, name[1000];
    FILE* fp;

    fp = fopen("p7_sign.cer", "rb");
    if (fp == NULL)
    {
        fprintf(stderr, "Error opening file\n");
        return -1;
    }

    len = fread(buf, 1, 1000, fp);
    fclose(fp);

    p = buf;
    p7 = d2i_PKCS7(NULL, (const unsigned char**)&p, len);
    if (p7 == NULL)
    {
        fprintf(stderr, "Error parsing PKCS7\n");
        return -1;
    }

    ret = OBJ_obj2txt(name, 1000, p7->type, 0);
    printf("type: %s\n", name);

    PKCS7_free(p7);
    return 0;
}*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
int main()
{
PKCS7 *p7=NULL;
int ret,len;
char buf[1000],*p,name[1000];
FILE *fp;
fp=fopen("p7_sign.cer","rb");
len=fread(buf,1,1000,fp);
fclose(fp);
p=buf;
d2i_PKCS7(&p7,(const unsigned char **)&p,len);
ret=OBJ_obj2txt(name,1000,p7->type,0);
printf("type : %s \n",name);
PKCS7_free(p7);
return 0;
}*/
