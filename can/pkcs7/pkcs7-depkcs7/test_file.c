#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>

void testParsePKCS7Data(void) {
    const char* filename = "out.p7";  // Replace with the actual file path

    CU_ASSERT_EQUAL(parsePKCS7Data(filename), 0);
}

int main() {
    // Initialize the CUnit test registry
    if (CU_initialize_registry() != CUE_SUCCESS) {
        fprintf(stderr, "Failed to initialize CUnit test registry.\n");
        return CU_get_error();
    }

    // Set up suites and tests
    CU_pSuite suite = CU_add_suite("PKCS7Tests", NULL, NULL);
    if (!suite) {
        CU_cleanup_registry();
        fprintf(stderr, "Failed to add suite.\n");
        return CU_get_error();
    }

    if (!CU_add_test(suite, "testParsePKCS7Data", testParsePKCS7Data)) {
        CU_cleanup_registry();
        fprintf(stderr, "Failed to add test.\n");
        return CU_get_error();
    }

    // Run all tests using the basic interface
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // Clean up the CUnit test registry
    CU_cleanup_registry();

    return CU_get_error();
}

