#include <string.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int createAndWritePKCS7(const char *data, const char *filename) {
    PKCS7 *p7;
    int len;
    char *der, *p;
    FILE *fp;

    p7 = PKCS7_new();
    if (p7 == NULL) {
        fprintf(stderr, "Error creating PKCS7 object\n");
        return 1;
    }

    p7->d.data = ASN1_OCTET_STRING_new();
    if (p7->d.data == NULL) {
        fprintf(stderr, "Error creating ASN1_OCTET_STRING object\n");
        PKCS7_free(p7);
        return 1;
    }

    PKCS7_set_type(p7, NID_pkcs7_data);

    len = strlen(data);
    ASN1_OCTET_STRING_set(p7->d.data, (const unsigned char *)data, len);

    len = i2d_PKCS7(p7, NULL);
    der = (char *)malloc(len);
    if (der == NULL) {
        fprintf(stderr, "Error allocating memory\n");
        PKCS7_free(p7);
        return 1;
    }

    p = der;
    len = i2d_PKCS7(p7, (unsigned char **)&p);

    fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Error opening file for writing\n");
        free(der);
        PKCS7_free(p7);
        return 1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);
    free(der);

    return 0;
}

/*int main() {
    const char *data = "pkcs7 data !\n";
    const char *filename = "p7_data.cer";

    int result = createAndWritePKCS7(data, filename);

    if (result == 0) {
        printf("PKCS7 object created and written to file successfully.\n");
    } else {
        fprintf(stderr, "Error creating or writing PKCS7 object.\n");
    }

    return result;
}*/


/*int main() {
    const char* inputData = "pkcs7 data !\n";
    PKCS7* p7 = createPKCS7(inputData);
    if (p7 == NULL) {
        return 1;
    }

    const char* outputFile = "p7_data.cer";
    int result = writePKCS7ToFile(p7, outputFile);

    return result;
}*/
/*#include <string.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>

int main() {
    PKCS7 *p7;
    int len;
    char buf[1000], *der, *p;
    FILE *fp;

    p7 = PKCS7_new();
    if (p7 == NULL) {
        fprintf(stderr, "Error creating PKCS7 object\n");
        return 1;
    }

    p7->d.data = ASN1_OCTET_STRING_new();
    if (p7->d.data == NULL) {
        fprintf(stderr, "Error creating ASN1_OCTET_STRING object\n");
        PKCS7_free(p7);
        return 1;
    }

    PKCS7_set_type(p7, NID_pkcs7_data);

    strcpy(buf, "pkcs7 data !\n");
    len = strlen(buf);
    ASN1_OCTET_STRING_set(p7->d.data, (const unsigned char *)buf, len);

    len = i2d_PKCS7(p7, NULL);
    der = (char *)malloc(len);
    if (der == NULL) {
        fprintf(stderr, "Error allocating memory\n");
        PKCS7_free(p7);
        return 1;
    }

    p = der;
    len = i2d_PKCS7(p7, (unsigned char **)&p);

    fp = fopen("p7_data.cer", "wb");
    if (fp == NULL) {
        fprintf(stderr, "Error opening file for writing\n");
        free(der);
        PKCS7_free(p7);
        return 1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);
    free(der);

    return 0;
}*/
/*
#include <string.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
int main()
{
PKCS7 *p7;
int len;
char buf[1000],*der,*p;
FILE *fp;
p7=PKCS7_new();
PKCS7_set_type(p7,NID_pkcs7_data);
strcpy(buf,"pkcs7 data !\n");
len=strlen(buf);
ASN1_OCTET_STRING_set(p7->d.data,(const unsigned char *)buf,len);
len=i2d_PKCS7(p7,NULL);
der=(char *)malloc(len);
p=der;
len=i2d_PKCS7(p7,(unsigned char **)&p);
fp=fopen("p7_data.cer","wb");
fwrite(der,1,len,fp);
fclose(fp);
PKCS7_free(p7);
free(der);
return 0;
}*/
