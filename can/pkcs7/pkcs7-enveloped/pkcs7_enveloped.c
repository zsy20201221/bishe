#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>

PKCS7 *createAndEncodeEnvelopedData()
{
    PKCS7 *p7 = PKCS7_new();
    if (!p7) {
        fprintf(stderr, "Error creating PKCS7 object\n");
        return NULL;
    }

    PKCS7_set_type(p7, NID_pkcs7_enveloped);
    ASN1_INTEGER_set(p7->d.enveloped->version, 3);

    PKCS7_RECIP_INFO *inf = PKCS7_RECIP_INFO_new();
    if (!inf) {
        fprintf(stderr, "Error creating PKCS7_RECIP_INFO object\n");
        PKCS7_free(p7);
        return NULL;
    }

    ASN1_INTEGER_set(inf->version, 4);
    ASN1_INTEGER_set(inf->issuer_and_serial->serial, 888888);

    inf->key_enc_algor = X509_ALGOR_new();
    if (!inf->key_enc_algor) {
        fprintf(stderr, "Error creating X509_ALGOR object\n");
        PKCS7_RECIP_INFO_free(inf);
        PKCS7_free(p7);
        return NULL;
    }

    inf->key_enc_algor->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    inf->enc_key = ASN1_OCTET_STRING_new();
    if (!inf->enc_key) {
        fprintf(stderr, "Error creating ASN1_OCTET_STRING object\n");
        X509_ALGOR_free(inf->key_enc_algor);
        PKCS7_RECIP_INFO_free(inf);
        PKCS7_free(p7);
        return NULL;
    }

    ASN1_OCTET_STRING_set(inf->enc_key, (const unsigned char *)"key info....", 12);

    sk_PKCS7_RECIP_INFO_push(p7->d.enveloped->recipientinfo, inf);

    p7->d.enveloped->enc_data->algorithm = X509_ALGOR_new();
    if (!p7->d.enveloped->enc_data->algorithm) {
        fprintf(stderr, "Error creating X509_ALGOR object\n");
        PKCS7_free(p7);
        return NULL;
    }

    p7->d.enveloped->enc_data->algorithm->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    p7->d.enveloped->enc_data->enc_data = ASN1_OCTET_STRING_new();
    if (!p7->d.enveloped->enc_data->enc_data) {
        fprintf(stderr, "Error creating ASN1_OCTET_STRING object\n");
        X509_ALGOR_free(p7->d.enveloped->enc_data->algorithm);
        PKCS7_free(p7);
        return NULL;
    }

    ASN1_OCTET_STRING_set(p7->d.enveloped->enc_data->enc_data, (const unsigned char *)"info....", 8);

    return p7;
}

void writePKCS7ToFile(const PKCS7 *p7, const char *filename)
{
    if (!p7 || !filename) {
        fprintf(stderr, "Invalid input\n");
        return;
    }

    int len = i2d_PKCS7(p7, NULL);
    char *der = (char *)malloc(len);
    if (!der) {
        fprintf(stderr, "Error allocating memory\n");
        return;
    }

    char *p = der;
    len = i2d_PKCS7(p7, (unsigned char **)&p);

    FILE *fp = fopen(filename, "wb");
    if (!fp) {
        fprintf(stderr, "Error opening file for writing\n");
        free(der);
        return;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    free(der);
}
/*int main()
{
    PKCS7 *p7 = createAndEncodeEnvelopedData();
    if (p7) {
        writePKCS7ToFile(p7, "p7_enveloped.cer");
        PKCS7_free(p7);
    }

    return 0;
}
*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>

int main()
{
    PKCS7 *p7;
    int len;
    char *der, *p;
    FILE *fp;
    PKCS7_RECIP_INFO *inf;

    p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_enveloped);
    ASN1_INTEGER_set(p7->d.enveloped->version, 3);

    inf = PKCS7_RECIP_INFO_new();
    ASN1_INTEGER_set(inf->version, 4);
    ASN1_INTEGER_set(inf->issuer_and_serial->serial, 888888);

    // 分配内存并设置 key_enc_algor->algorithm
    inf->key_enc_algor = X509_ALGOR_new();
    inf->key_enc_algor->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    // 分配内存并设置 enc_key
    inf->enc_key = ASN1_OCTET_STRING_new();
    ASN1_OCTET_STRING_set(inf->enc_key, (const unsigned char *)"key info....", 12);

    sk_PKCS7_RECIP_INFO_push(p7->d.enveloped->recipientinfo, inf);

    // 分配内存并设置 algorithm->algorithm
    p7->d.enveloped->enc_data->algorithm = X509_ALGOR_new();
    p7->d.enveloped->enc_data->algorithm->algorithm = OBJ_nid2obj(NID_des_ede3_cbc);

    // 分配内存并设置 enc_data
    p7->d.enveloped->enc_data->enc_data = ASN1_OCTET_STRING_new();
    ASN1_OCTET_STRING_set(p7->d.enveloped->enc_data->enc_data, (const unsigned char *)"info....", 8);

    len = i2d_PKCS7(p7, NULL);
    der = (char *)malloc(len);
    p = der;
    len = i2d_PKCS7(p7, (unsigned char **)&p);

    fp = fopen("p7_enveloped.cer", "wb");
    fwrite(der, 1, len, fp);
    fclose(fp);

    PKCS7_free(p7);
    free(der);

    return 0;
}
*/
/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
int main()
{
PKCS7 *p7;
int len;
char *der,*p;
FILE *fp;
PKCS7_RECIP_INFO *inf;
p7=PKCS7_new();
PKCS7_set_type(p7,NID_pkcs7_enveloped);
ASN1_INTEGER_set(p7->d.enveloped->version,3);
inf=PKCS7_RECIP_INFO_new();
ASN1_INTEGER_set(inf->version,4);
ASN1_INTEGER_set(inf->issuer_and_serial->serial,888888);
inf->key_enc_algor->algorithm=OBJ_nid2obj(NID_des_ede3_cbc);
ASN1_OCTET_STRING_set(inf->enc_key,(const unsigned char *)"key info....",12);
sk_PKCS7_RECIP_INFO_push(p7->d.enveloped->recipientinfo,inf);
p7->d.enveloped->enc_data->algorithm->algorithm=OBJ_nid2obj(NID_des_ede3_cbc);
p7->d.enveloped->enc_data->enc_data=ASN1_OCTET_STRING_new();
ASN1_OCTET_STRING_set(p7->d.enveloped->enc_data->enc_data,(const unsigned char *)"info....",8); 
len=i2d_PKCS7(p7,NULL);
der=(char *)malloc(len);
p=der;
len=i2d_PKCS7(p7,(unsigned char **)&p);
fp=fopen("p7_evveloped.cer","wb");
fwrite(der,1,len,fp);
fclose(fp);
PKCS7_free(p7);
free(der);
return 0;
}*/
