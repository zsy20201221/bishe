#include <openssl/objects.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>

int createAndSerializePKCS7(const char* content, const char* outputFileName) {
    PKCS7* p7;
    int ret;
    BIO *b, *content_bio;

    p7 = PKCS7_new();
    if (!p7) {
        fprintf(stderr, "Error creating PKCS7 structure\n");
        return 1;
    }

    ret = PKCS7_set_type(p7, NID_pkcs7_signed);
    if (ret != 1) {
        fprintf(stderr, "Error setting PKCS7 type\n");
        PKCS7_free(p7);
        return 1;
    }

    // 添加实际内容
    content_bio = BIO_new_mem_buf(content, -1);
    if (!content_bio) {
        fprintf(stderr, "Error creating content BIO\n");
        PKCS7_free(p7);
        return 1;
    }

    // 设置内容
    PKCS7_content_new(p7, NID_pkcs7_data);
    ret = PEM_write_bio_PKCS7_stream(content_bio, p7, NULL, 0);
    if (ret != 1) {
        fprintf(stderr, "Error setting PKCS7 content\n");
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    printf("Content set successfully\n");

    b = BIO_new_file(outputFileName, "wb");
    if (!b) {
        fprintf(stderr, "Error creating file BIO\n");
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    // 将 PKCS7 结构体序列化为PEM格式到文件 BIO 中
    ret = PEM_write_bio_PKCS7(b, p7);
    if (ret != 1) {
        fprintf(stderr, "Error writing to file BIO\n");
        ERR_print_errors_fp(stderr);
        BIO_free(b);
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    printf("PKCS7 serialized to file BIO successfully\n");

    // 处理文件 BIO 中的数据，例如将其写入文件或发送到网络等

    BIO_free(b);
    BIO_free(content_bio);
    PKCS7_free(p7);

    return 0;
}

int main() {
    const char* content_str = "Hello, PKCS7!";
    const char* outputFileName = "p7Digest.pem";

    int result = createAndSerializePKCS7(content_str, outputFileName);

    return result;
}


/*#include <openssl/objects.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>

int main() {
    PKCS7* p7;
    int ret;
    BIO *b, *content_bio;

    p7 = PKCS7_new();
    if (!p7) {
        fprintf(stderr, "Error creating PKCS7 structure\n");
        return 1;
    }

    ret = PKCS7_set_type(p7, NID_pkcs7_signed);
    if (ret != 1) {
        fprintf(stderr, "Error setting PKCS7 type\n");
        PKCS7_free(p7);
        return 1;
    }

    // 添加一些实际的内容
    const char* content_str = "Hello, PKCS7!";
    content_bio = BIO_new_mem_buf(content_str, -1);
    if (!content_bio) {
        fprintf(stderr, "Error creating content BIO\n");
        PKCS7_free(p7);
        return 1;
    }

    // 设置内容
    PKCS7_content_new(p7, NID_pkcs7_data);
    ret = PEM_write_bio_PKCS7_stream(content_bio, p7, NULL, 0);
    if (ret != 1) {
        fprintf(stderr, "Error setting PKCS7 content\n");
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    printf("Content set successfully\n");

    b = BIO_new_file("p7Digest.pem", "wb");  // 使用"wb"以二进制方式打开文件
    if (!b) {
        fprintf(stderr, "Error creating file BIO\n");
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    // 将 PKCS7 结构体序列化为PEM格式到文件 BIO 中
    ret = PEM_write_bio_PKCS7(b, p7);
    if (ret != 1) {
        fprintf(stderr, "Error writing to file BIO\n");
        ERR_print_errors_fp(stderr);
        BIO_free(b);
        BIO_free(content_bio);
        PKCS7_free(p7);
        return 1;
    }

    printf("PKCS7 serialized to file BIO successfully\n");

    // 处理文件 BIO 中的数据，例如将其写入文件或发送到网络等

    BIO_free(b);
    BIO_free(content_bio);
    PKCS7_free(p7);

    return 0;
}
*/



/*#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/pem.h>
int main()
{
PKCS7 *p7;
int ret;
BIO *b;
p7=PKCS7_new();
ret=PKCS7_set_type(p7,NID_pkcs7_digest);
b=BIO_new_file("p7Digest.pem","w");
PEM_write_bio_PKCS7(b,p7);
BIO_free(b);
PKCS7_free(p7);
return 0;
}*/

