#include <CUnit/Basic.h>


void test_createPKCS7() {
    const char* content_str = "Hello, PKCS7!";
    const char* output_file = "test_output.pem";

    int result = createPKCS7(content_str, output_file);

    CU_ASSERT_EQUAL(result, 0);
    // 可以添加更多的测试条件，确保函数的正确性
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 添加测试套件
    CU_pSuite suite = CU_add_suite("createPKCS7_suite", NULL, NULL);
    CU_add_test(suite, "test_createPKCS7", test_createPKCS7);

    // 运行测试
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return CU_get_error();
}

