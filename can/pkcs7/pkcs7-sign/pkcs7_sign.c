#include <stdio.h>
#include <stdlib.h>
#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
#include <openssl/err.h>

int generatePKCS7(const char* certFilePath, const char* outputFilePath) {
    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();

    PKCS7* p7;
    int len;
    unsigned char* der;
    FILE* fp;
    X509* x;
    BIO* in;
    X509_ALGOR* md;
    PKCS7_SIGNER_INFO* si;

    p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);

    // 创建 PKCS7_SIGNED 结构并设置
    p7->d.sign = PKCS7_SIGNED_new();

    // 读取证书
    in = BIO_new_file(certFilePath, "r");
    x = PEM_read_bio_X509(in, NULL, NULL, NULL);
    BIO_free(in);

    // 设置签名者的证书
    PKCS7_add_certificate(p7, x);

    // 设置签名算法
    md = X509_ALGOR_new();
    md->algorithm = OBJ_nid2obj(NID_md5);
    sk_X509_ALGOR_push(p7->d.sign->md_algs, md);

    // 设置签名者信息
    si = PKCS7_SIGNER_INFO_new();
    ASN1_INTEGER_set(si->version, 2);
    ASN1_INTEGER_set(si->issuer_and_serial->serial, 333);
    sk_PKCS7_SIGNER_INFO_push(p7->d.sign->signer_info, si);

    // 对 PKCS7 对象进行编码
    len = i2d_PKCS7(p7, NULL);
    der = (unsigned char*)malloc(len);
    unsigned char* p = der;
    i2d_PKCS7(p7, &p);

    // 将 DER 数据写入文件
    fp = fopen(outputFilePath, "wb");
    fwrite(der, 1, len, fp);
    fclose(fp);

    // 释放资源
    free(der);
    PKCS7_free(p7);
    X509_free(x);

    ERR_free_strings();
    EVP_cleanup();

    return 0;
}

int main()
{
    const char* certFilePath = "certificate.crt";
    const char* outputFilePath = "p7_sign.p7";

    if (generatePKCS7(certFilePath, outputFilePath) == 0) {
        printf("PKCS7 generated successfully.\n");
    }
    else {
        printf("Failed to generate PKCS7.\n");
    }

    return 0;
}
/*#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
#include <openssl/err.h>

int main()
{
    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();

    PKCS7 *p7;
    int len;
    unsigned char *der;
    FILE *fp;
    X509 *x;
    BIO *in;
    X509_ALGOR *md;
    PKCS7_SIGNER_INFO *si;

    p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);

    // 创建 PKCS7_SIGNED 结构并设置
    p7->d.sign = PKCS7_SIGNED_new();

    // 读取证书
    in = BIO_new_file("b64cert.cer", "r");
    x = PEM_read_bio_X509(in, NULL, NULL, NULL);
    BIO_free(in);

    // 设置签名者的证书
    PKCS7_add_certificate(p7, x);

    // 设置签名算法
    md = X509_ALGOR_new();
    md->algorithm = OBJ_nid2obj(NID_md5);
    sk_X509_ALGOR_push(p7->d.sign->md_algs, md);

    // 设置签名者信息
    si = PKCS7_SIGNER_INFO_new();
    ASN1_INTEGER_set(si->version, 2);
    ASN1_INTEGER_set(si->issuer_and_serial->serial, 333);
    sk_PKCS7_SIGNER_INFO_push(p7->d.sign->signer_info, si);

    // 对 PKCS7 对象进行编码
    len = i2d_PKCS7(p7, NULL);
    der = (unsigned char *)malloc(len);
    unsigned char *p = der;
    i2d_PKCS7(p7, &p);

    // 将 DER 数据写入文件
    fp = fopen("p7_sign.cer", "wb");
    fwrite(der, 1, len, fp);
    fclose(fp);

    // 释放资源
    free(der);
    PKCS7_free(p7);
    X509_free(x);

    ERR_free_strings();
    EVP_cleanup();

    return 0;
}
*/
/*#include <openssl/pem.h>
#include <openssl/pkcs7.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
int main()
{
PKCS7 *p7;
int len;
unsigned char *der,*p;
FILE *fp;
X509 *x;
BIO *in;
X509_ALGOR *md;
PKCS7_SIGNER_INFO*si;
p7=PKCS7_new();
PKCS7_set_type(p7,NID_pkcs7_signed);
p7->d.sign->cert=sk_X509_new_null();
in=BIO_new_file("b64cert.cer","r");
x=PEM_read_bio_X509(in,NULL,NULL,NULL);
sk_X509_push(p7->d.sign->cert,x);
md=X509_ALGOR_new();
md->algorithm=OBJ_nid2obj(NID_md5);
sk_X509_ALGOR_push(p7->d.sign->md_algs,md);
si=PKCS7_SIGNER_INFO_new();
ASN1_INTEGER_set(si->version,2);
ASN1_INTEGER_set(si->issuer_and_serial->serial,333);
sk_PKCS7_SIGNER_INFO_push(p7->d.sign->signer_info,si);
len=i2d_PKCS7(p7,NULL);
der=(unsigned char *)malloc(len);
p=der;
len=i2d_PKCS7(p7,&p);
fp=fopen("p7_sign.cer","wb");
fwrite(der,1,len,fp);
fclose(fp);
free(der);
PKCS7_free(p7);
return 0;
}*/

