#include <stdio.h>
#include <string.h>
#include <openssl/md4.h>
#include <openssl/md5.h>
#include <openssl/ripemd.h>
#include <openssl/sha.h>

void calculateHash(const unsigned char *input, size_t inputLength, unsigned char *output, size_t outputLength, const char *algorithm)
{
    if (strcmp(algorithm, "RIPEMD160") == 0)
    {
        RIPEMD160(input, inputLength, output);
    }
    else if (strcmp(algorithm, "MD4") == 0)
    {
        MD4(input, inputLength, output);
    }
    else if (strcmp(algorithm, "MD5") == 0)
    {
        MD5(input, inputLength, output);
    }
    else if (strcmp(algorithm, "SHA1") == 0)
    {
        SHA1(input, inputLength, output);
    }
    else if (strcmp(algorithm, "SHA256") == 0)
    {
        SHA256(input, inputLength, output);
    }
    else if (strcmp(algorithm, "SHA512") == 0)
    {
        SHA512(input, inputLength, output);
    }
    else
    {
        printf("Invalid algorithm specified.\n");
        return;
    }

    printf("%s digest result:\n", algorithm);
    for (int i = 0; i < outputLength; i++)
    {
        printf("%02x ", output[i]);
    }
    printf("\n");
}

/*int main()
{
    unsigned char input[] = "3dsferyewyrtetegvbzVEgarhaggavxcv";
    size_t inputLength = strlen((const char *)input);

    unsigned char output[64];

    calculateHash(input, inputLength, output, 20, "RIPEMD160");
    calculateHash(input, inputLength, output, 16, "MD4");
    calculateHash(input, inputLength, output, 16, "MD5");
    calculateHash(input, inputLength, output, 20, "SHA1");
    calculateHash(input, inputLength, output, 32, "SHA256");
    calculateHash(input, inputLength, output, 64, "SHA512");

    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <openssl/md4.h>
#include <openssl/md5.h>
#include <openssl/ripemd.h>
#include <openssl/sha.h>

int main()
{
    unsigned char in[] = "3dsferyewyrtetegvbzVEgarhaggavxcv";
    unsigned char out[64];
    size_t n = strlen((const char*)in);
    int i;
    printf("默认 openssl 安装配置无 MDC2\n");


    unsigned char out_ripemd160[20];
    RIPEMD160(in, n, out_ripemd160);
    printf("RIPEMD160 digest result :\n");
    for (i = 0; i < 20; i++)
        printf("%02x ", out_ripemd160[i]);
    printf("\n");

    // MD2(in,n,out);

    unsigned char out_md4[16];
    MD4(in, n, out_md4);
    printf("MD4 digest result :\n");
    for (i = 0; i < 16; i++)
        printf("%02x ", out_md4[i]);
    printf("\n");

    unsigned char out_md5[16];
    MD5(in, n, out_md5);
    printf("MD5 digest result :\n");
    for (i = 0; i < 16; i++)
        printf("%02x ", out_md5[i]);
    printf("\n");

    // SHA(in,n,out);

    unsigned char out_sha1[20];
    SHA1(in, n, out_sha1);
    printf("SHA1 digest result :\n");
    for (i = 0; i < 20; i++)
        printf("%02x ", out_sha1[i]);
    printf("\n");

    unsigned char out_sha256[32];
    SHA256(in, n, out_sha256);
    printf("SHA256 digest result :\n");
    for (i = 0; i < 32; i++)
        printf("%02x ", out_sha256[i]);
    printf("\n");

    unsigned char out_sha512[64];
    SHA512(in, n, out_sha512);
    printf("SHA512 digest result :\n");
    for (i = 0; i < 64; i++)
        printf("%02x ", out_sha512[i]);
    printf("\n");

    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <openssl/md2.h>
#include <openssl/md4.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

int main()
{
unsigned char in[]="3dsferyewyrtetegvbzVEgarhaggavxcv";
unsigned char out[20];
size_t n;
int i;
n=strlen((const char*)in);
#ifdef OPENSSL_NO_MDC2
printf("默认 openssl 安装配置无 MDC2\n");
#else
//MDC2(in,n,out);
printf("MDC2 digest result :\n");
for(i=0;i<16;i++)
printf("%x ",out[i]);
#endif
RIPEMD160(in,n,out);
printf("RIPEMD160 digest result :\n");
for(i=0;i<20;i++)
printf("%x ",out[i]);
//MD2(in,n,out);
printf("MD2 digest result :\n");
for(i=0;i<16;i++)
printf("%x ",out[i]);
MD4(in,n,out);
printf("\n\nMD4 digest result :\n");
for(i=0;i<16;i++)
printf("%x ",out[i]);
MD5(in,n,out);
printf("\n\nMD5 digest result :\n");
for(i=0;i<16;i++)
printf("%x ",out[i]);
//SHA(in,n,out);
printf("\n\nSHA digest result :\n");
for(i=0;i<20;i++)
printf("%x ",out[i]);
SHA1(in,n,out);
printf("\n\nSHA1 digest result :\n");
for(i=0;i<20;i++)
printf("%x ",out[i]);
SHA256(in,n,out);
printf("\n\nSHA256 digest result :\n");
for(i=0;i<32;i++)
printf("%x ",out[i]);
SHA512(in,n,out);
printf("\n\nSHA512 digest result :\n");
for(i=0;i<64;i++)
printf("%x ",out[i]);
printf("\n");
return 0;
}
*/

