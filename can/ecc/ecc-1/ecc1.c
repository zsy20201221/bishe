#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#include <openssl/err.h>
#include <openssl/evp.h>

int performECDH(EC_KEY *key1, EC_KEY *key2) {
    const EC_POINT *pubkey1, *pubkey2;
    const EC_GROUP *group1, *group2;
    unsigned char *shareKey1, *shareKey2;
    size_t len1, len2, i;

    /* 获取对方公钥，不能直接引用 */
    pubkey1 = EC_KEY_get0_public_key(key1);
    pubkey2 = EC_KEY_get0_public_key(key2);

    /* 获取密钥参数 group */
    group1 = EC_KEY_get0_group(key1);
    group2 = EC_KEY_get0_group(key2);

    /* 创建存储共享密钥的缓冲区 */
    shareKey1 = malloc((EC_GROUP_get_degree(group1) + 7) / 8);
    shareKey2 = malloc((EC_GROUP_get_degree(group2) + 7) / 8);

    if (shareKey1 == NULL || shareKey2 == NULL) {
        fprintf(stderr, "Memory allocation failed!\n");
        return -1;
    }

    /* 生成一方的共享密钥 */
    len1 = ECDH_compute_key(shareKey1, (EC_GROUP_get_degree(group1) + 7) / 8, pubkey2, key1, NULL);

    /* 生成另一方共享密钥*/
    len2 = ECDH_compute_key(shareKey2, (EC_GROUP_get_degree(group2) + 7) / 8, pubkey1, key2, NULL);

    /* 检查共享密钥是否相同 */
    if (len1 != len2 || memcmp(shareKey1, shareKey2, len1) != 0) {
        fprintf(stderr, "Shared keys do not match!\n");
        free(shareKey1);
        free(shareKey2);
        return -1;
    }

    /* 打印共享密钥 */
    printf("Shared Key: ");
    for (i = 0; i < len1; i++) {
        printf("%02x", shareKey1[i]);
    }
    printf("\n");

    printf("ECDH key exchange successful!\n");

    /* 释放资源 */
    free(shareKey1);
    free(shareKey2);

    return 0;
}

/*int main() {
    EC_KEY *key1, *key2;
    int ret;

    //创建 EC_KEY 结构体 
    key1 = EC_KEY_new();
    key2 = EC_KEY_new();

    if (key1 == NULL || key2 == NULL) {
        fprintf(stderr, "EC_KEY_new failed!\n");
        return -1;
    }

    // 选择椭圆曲线 
    int nid = NID_X9_62_prime256v1;

    //根据选择的椭圆曲线生成密钥参数 group 
    const EC_GROUP *group1 = EC_GROUP_new_by_curve_name(nid);
    const EC_GROUP *group2 = EC_GROUP_new_by_curve_name(nid);

    if (group1 == NULL || group2 == NULL) {
        fprintf(stderr, "EC_GROUP_new_by_curve_name failed!\n");
        return -1;
    }

    //设置密钥参数 
    if (EC_KEY_set_group(key1, group1) != 1 || EC_KEY_set_group(key2, group2) != 1) {
        fprintf(stderr, "EC_KEY_set_group failed!\n");
        return -1;
    }

    // 生成密钥对 
    if (EC_KEY_generate_key(key1) != 1 || EC_KEY_generate_key(key2) != 1) {
        fprintf(stderr, "EC_KEY_generate_key failed!\n");
        return -1;
    }

    // 执行 ECDH 密钥交换 
    ret = performECDH(key1, key2);

    //释放资源 
    EC_KEY_free(key1);
    EC_KEY_free(key2);

    return ret;
}*/


/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#include <openssl/err.h>
#include <openssl/evp.h>

int main() {
    EC_KEY *key1, *key2;
    const EC_POINT *pubkey1, *pubkey2;
    const EC_GROUP *group1, *group2;
    int ret, nid, size, i, sig_len;
    unsigned char *signature, digest[20];
    BIO *berr;
    EC_builtin_curve *curves;
    int crv_len;
    unsigned char shareKey1[128], shareKey2[128];
    size_t len1, len2;

    // 构造 EC_KEY 数据结构 
    key1 = EC_KEY_new();
    if (key1 == NULL) {
        printf("EC_KEY_new err!\n");
        return -1;
    }
    key2 = EC_KEY_new();
    if (key2 == NULL) {
        printf("EC_KEY_new err!\n");
        return -1;
    }

    // 获取实现的椭圆曲线个数 
    crv_len = EC_get_builtin_curves(NULL, 0);
    curves = (EC_builtin_curve *)malloc(sizeof(EC_builtin_curve) * crv_len);
    // 获取椭圆曲线列表 
    EC_get_builtin_curves(curves, crv_len);

    // 选取一种椭圆曲线 
    nid = curves[25].nid;

    // 根据选择的椭圆曲线生成密钥参数 group 
    group1 = EC_GROUP_new_by_curve_name(nid);
    if (group1 == NULL) {
        printf("EC_GROUP_new_by_curve_name err!\n");
        return -1;
    }
    group2 = EC_GROUP_new_by_curve_name(nid);
    if (group2 == NULL) {
        printf("EC_GROUP_new_by_curve_name err!\n");
        return -1;
    }

    // 设置密钥参数 
    ret = EC_KEY_set_group(key1, group1);
    if (ret != 1) {
        printf("EC_KEY_set_group err.\n");
        return -1;
    }
    ret = EC_KEY_set_group(key2, group2);
    if (ret != 1) {
        printf("EC_KEY_set_group err.\n");
        return -1;
    }

    // 生成密钥 
    ret = EC_KEY_generate_key(key1);
    if (ret != 1) {
        printf("EC_KEY_generate_key err.\n");
        return -1;
    }
    ret = EC_KEY_generate_key(key2);
    if (ret != 1) {
        printf("EC_KEY_generate_key err.\n");
        return -1;
    }

    // 检查密钥 
    ret = EC_KEY_check_key(key1);
    if (ret != 1) {
        printf("check key err.\n");
        return -1;
    }

    // 获取密钥大小 
    size = ECDSA_size(key1);
    printf("size %d \n", size);

    for (i = 0; i < 20; i++)
        memset(&digest[i], i + 1, 1);

    signature = malloc(size);
    ERR_load_crypto_strings();
    berr = BIO_new(BIO_s_file());
    BIO_set_fp(berr, stdout, BIO_NOCLOSE);

    // 签名数据，本例未做摘要，可将 digest 中的数据看作是 sha1 摘要结果 
    ret = ECDSA_sign(EVP_sha256(), digest, sizeof(digest), signature, &sig_len, key1);
    if (ret != 1) {
        ERR_print_errors(berr);
        printf("sign err!\n");
        return -1;
    }

    // 验证签名 
    ret = ECDSA_verify(EVP_sha256(), digest, sizeof(digest), signature, sig_len, key1);
    if (ret != 1) {
        ERR_print_errors(berr);
        printf("ECDSA_verify err!\n");
        return -1;
    }

    // 获取对方公钥，不能直接引用 
    pubkey1 = EC_KEY_get0_public_key(key1);
    pubkey2 = EC_KEY_get0_public_key(key2);

    // 生成一方的共享密钥 
    len1 = ECDH_compute_key(shareKey1, sizeof(shareKey1), pubkey2, key1, NULL);

    // 生成另一方共享密钥
    len2 = ECDH_compute_key(shareKey2, sizeof(shareKey2), pubkey1, key2, NULL);

    // 检查共享密钥是否相同 
    if (len1 != len2 || memcmp(shareKey1, shareKey2, len1) != 0) {
        printf("Shared keys do not match!\n");
        return -1;
    }

    // 打印共享密钥 
    printf("Shared Key: ");
    for (i = 0; i < len1; i++) {
        printf("%02x", shareKey1[i]);
    }
    printf("\n");

    // 释放资源 
    free(signature);
    EC_KEY_free(key1);
    EC_KEY_free(key2);
    EC_GROUP_free(group1);
    EC_GROUP_free(group2);
    free(curves);
    BIO_free(berr);

    return 0;
}*/
