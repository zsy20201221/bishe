#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#include <openssl/err.h>
#include <openssl/evp.h>

// 测试套件初始化函数
int init_suite(void) {
    return 0;
}

// 测试套件清理函数
int clean_suite(void) {
    return 0;
}

// 测试 performECDH 函数
void test_performECDH(void) {
    EC_KEY *key1, *key2;
    int ret;

    key1 = EC_KEY_new();
    key2 = EC_KEY_new();

    CU_ASSERT_PTR_NOT_NULL(key1);
    CU_ASSERT_PTR_NOT_NULL(key2);

    int nid = NID_X9_62_prime256v1;
    const EC_GROUP *group1 = EC_GROUP_new_by_curve_name(nid);
    const EC_GROUP *group2 = EC_GROUP_new_by_curve_name(nid);

    CU_ASSERT_PTR_NOT_NULL(group1);
    CU_ASSERT_PTR_NOT_NULL(group2);

    CU_ASSERT_EQUAL(EC_KEY_set_group(key1, group1), 1);
    CU_ASSERT_EQUAL(EC_KEY_set_group(key2, group2), 1);

    CU_ASSERT_EQUAL(EC_KEY_generate_key(key1), 1);
    CU_ASSERT_EQUAL(EC_KEY_generate_key(key2), 1);

    ret = performECDH(key1, key2);

    CU_ASSERT_EQUAL(ret, 0);

    // 释放资源
    EC_KEY_free(key1);
    EC_KEY_free(key2);
}

// 主函数设置并运行测试
int main() {
    // 初始化 CUnit 测试注册表
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // 添加测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 添加测试用例
    if (CU_add_test(suite, "测试 performECDH 函数", test_performECDH) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 使用基本接口运行所有测试
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理
    CU_cleanup_registry();

    return CU_get_error();
}

