#include <openssl/evp.h>
#include <openssl/rand.h>

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int main()
{
    // 初始化 OpenSSL
    OPENSSL_init_crypto(OPENSSL_INIT_LOAD_CRYPTO_STRINGS | OPENSSL_INIT_ADD_ALL_CIPHERS | OPENSSL_INIT_ADD_ALL_DIGESTS, NULL);

    // 加载 SM2 算法
    EVP_add_cipher(EVP_sm2());

    // 设置密钥
    const char *priv_key_hex = "7C9A7A8F41B391BD0E7436E5512FE4616F85E003D7A10A1EC07A9EFE673A5A34";
    const char *pub_key_hex = "0412EB4F34042EE82C1D2270F4E7B21BC03EDB97156B9E80FB7C978C2FCD255ABE49577D02A877084A99660A90916EBB6A4AE8F8750203D1EB69AB3E64F9D51B7B";

    EC_KEY *ec_key = EC_KEY_new_by_curve_name(NID_sm2);
    if (!ec_key)
        handleErrors();

    BIGNUM *priv_key = BN_new();
    if (!BN_hex2bn(&priv_key, priv_key_hex))
        handleErrors();

    BIGNUM *pub_key_x = BN_new();
    BIGNUM *pub_key_y = BN_new();

    if (!BN_hex2bn(&pub_key_x, pub_key_hex + 2) || !BN_hex2bn(&pub_key_y, pub_key_hex + 66))
        handleErrors();

    EC_KEY_set_private_key(ec_key, priv_key);
    EC_KEY_set_public_key_affine_coordinates(ec_key, pub_key_x, pub_key_y);

    // 加密
    const char *plaintext = "Hello, SM2!";
    size_t plaintext_len = strlen(plaintext);

    // 使用随机数生成一个对称密钥
    unsigned char symmetric_key[32];
    if (RAND_bytes(symmetric_key, sizeof(symmetric_key)) != 1)
        handleErrors();

    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    if (!ctx)
        handleErrors();

    if (EVP_EncryptInit_ex(ctx, EVP_sm2_ecb(), NULL, symmetric_key, NULL) != 1)
        handleErrors();

    // 获取加密后的密文长度
    size_t ciphertext_len = 0;
    if (EVP_EncryptUpdate(ctx, NULL, (int *)&ciphertext_len, (const unsigned char *)plaintext, plaintext_len) != 1)
        handleErrors();

    unsigned char *ciphertext = OPENSSL_malloc(ciphertext_len);
    if (!ciphertext)
        handleErrors();

    // 执行加密
    if (EVP_EncryptUpdate(ctx, ciphertext, (int *)&ciphertext_len, (const unsigned char *)plaintext, plaintext_len) != 1)
        handleErrors();

    // 输出加密后的密文
    printf("Ciphertext: ");
    for (size_t i = 0; i < ciphertext_len; i++)
        printf("%02X", ciphertext[i]);
    printf("\n");

    // 解密
    unsigned char *decryptedtext = OPENSSL_malloc(plaintext_len);
    if (!decryptedtext)
        handleErrors();

    if (EVP_DecryptInit_ex(ctx, EVP_sm2_ecb(), NULL, symmetric_key, NULL) != 1)
        handleErrors();

    // 执行解密
    if (EVP_DecryptUpdate(ctx, decryptedtext, (int *)&plaintext_len, ciphertext, ciphertext_len) != 1)
        handleErrors();

    // 输出解密后的明文
    printf("Decrypted text: %s\n", decryptedtext);

    // 释放资源
    OPENSSL_free(ciphertext);
    OPENSSL_free(decryptedtext);
    EVP_CIPHER_CTX_free(ctx);
    EC_KEY_free(ec_key);
    BN_free(priv_key);
    BN_free(pub_key_x);
    BN_free(pub_key_y);

    return 0;
}

