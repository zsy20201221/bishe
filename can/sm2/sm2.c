#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ec.h>
#include <openssl/objects.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

void handleErrors()
{
    fprintf(stderr, "Error occurred.\n");
    exit(1);
}

void generate_keypair()
{
    EVP_PKEY_CTX *pctx = NULL;
    EVP_PKEY *pkey = NULL;
    EC_KEY *eckey = NULL;
    const EC_POINT *pub_key = NULL;
    const BIGNUM *priv_key = NULL;
    unsigned char *pub_key_bytes = NULL;
    size_t pub_key_len;

    pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL);
    if (!pctx)
        handleErrors();

    if (EVP_PKEY_keygen_init(pctx) <= 0)
        handleErrors();

    if (EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_sm2) <= 0)
        handleErrors();

    if (EVP_PKEY_keygen(pctx, &pkey) <= 0)
        handleErrors();

    eckey = EVP_PKEY_get1_EC_KEY(pkey);
    if (!eckey)
        handleErrors();

    pub_key = EC_KEY_get0_public_key(eckey);
    priv_key = EC_KEY_get0_private_key(eckey);

    // Export public key to bytes
    pub_key_len = EC_POINT_point2buf(EC_KEY_get0_group(eckey), pub_key, POINT_CONVERSION_UNCOMPRESSED, &pub_key_bytes, NULL);
    if (!pub_key_bytes)
        handleErrors();

    printf("Public Key: ");
    for (size_t i = 0; i < pub_key_len; i++)
        printf("%02x", pub_key_bytes[i]);
    printf("\n");

    // Export private key to bytes
    size_t priv_key_len = BN_num_bytes(priv_key);
    unsigned char *priv_key_bytes = malloc(priv_key_len);
    if (!priv_key_bytes)
        handleErrors();

    BN_bn2bin(priv_key, priv_key_bytes);

    printf("Private Key: ");
    for (size_t i = 0; i < priv_key_len; i++)
        printf("%02x", priv_key_bytes[i]);
    printf("\n");

    free(pub_key_bytes);
    free(priv_key_bytes);
    EVP_PKEY_CTX_free(pctx);
    EVP_PKEY_free(pkey);
    EC_KEY_free(eckey);
}

/*int main()
{
    OpenSSL_add_all_algorithms();
    generate_keypair();
    EVP_cleanup();
    return 0;
}*/
