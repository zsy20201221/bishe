#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/ec.h>
#include <openssl/evp.h>
#include <openssl/rand.h>


void testGenerateKeypair(void) {
    EVP_PKEY_CTX *pctx = NULL;
    EVP_PKEY *pkey = NULL;
    EC_KEY *eckey = NULL;
    const EC_POINT *pub_key = NULL;
    const BIGNUM *priv_key = NULL;
    unsigned char *pub_key_bytes = NULL;
    size_t pub_key_len;

    pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL);
    CU_ASSERT_PTR_NOT_NULL(pctx);

    CU_ASSERT_TRUE(EVP_PKEY_keygen_init(pctx) > 0);
    CU_ASSERT_TRUE(EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_sm2) > 0);
    CU_ASSERT_TRUE(EVP_PKEY_keygen(pctx, &pkey) > 0);

    eckey = EVP_PKEY_get1_EC_KEY(pkey);
    CU_ASSERT_PTR_NOT_NULL(eckey);

    pub_key = EC_KEY_get0_public_key(eckey);
    priv_key = EC_KEY_get0_private_key(eckey);

    // Export public key to bytes
    pub_key_len = EC_POINT_point2buf(EC_KEY_get0_group(eckey), pub_key, POINT_CONVERSION_UNCOMPRESSED, &pub_key_bytes, NULL);
    CU_ASSERT_PTR_NOT_NULL(pub_key_bytes);

    // 验证公钥长度是否符合预期
    CU_ASSERT_EQUAL(pub_key_len, 65);

    // Export private key to bytes
    size_t priv_key_len = BN_num_bytes(priv_key);
    unsigned char *priv_key_bytes = malloc(priv_key_len);
    CU_ASSERT_PTR_NOT_NULL(priv_key_bytes);

    BN_bn2bin(priv_key, priv_key_bytes);

    // 验证私钥长度是否符合预期
    CU_ASSERT_EQUAL(priv_key_len, 32);

    free(pub_key_bytes);
    free(priv_key_bytes);
    EVP_PKEY_CTX_free(pctx);
    EVP_PKEY_free(pkey);
    EC_KEY_free(eckey);
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("GenerateKeypairSuite", NULL, NULL);

    // 将测试用例添加到测试套件中
    CU_add_test(suite, "testGenerateKeypair", testGenerateKeypair);

    // 设置 CUnit 基本模式并运行测试套件
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return 0;
}
