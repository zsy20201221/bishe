#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

void generateKeyAndIV(const EVP_CIPHER *type, const EVP_MD *md, unsigned char *salt, unsigned char *data, int datal, int count, unsigned char **key, unsigned char **iv) {
    int keyl, ivl, ret, i;

    keyl = EVP_CIPHER_key_length(type);
    ivl = EVP_CIPHER_iv_length(type);

    *key = (unsigned char *)malloc(keyl);
    *iv = (unsigned char *)malloc(ivl);

    ret = EVP_BytesToKey(type, md, salt, data, datal, count, *key, *iv);

    if (ret == 0) {
        printf("Failed to generate key and iv.\n");
        free(*key);
        free(*iv);
        *key = NULL;
        *iv = NULL;
        return;
    }

    printf("Generated key value:\n");
    for (i = 0; i < keyl; i++)
        printf("%02x ", *(*key + i));
    printf("\n");

    printf("Generated IV value:\n");
    for (i = 0; i < ivl; i++)
        printf("%02x ", *(*iv + i));
    printf("\n");
}

/*int main() {
    int cnid, i, cbsize, mtype, mnid, mbsize, msize;
    const EVP_CIPHER *type;
    const EVP_MD *md;
    unsigned char salt[20], data[100], *key, *iv;
    const char *cname, *mname;

    type = EVP_des_ecb();
    cnid = EVP_CIPHER_nid(type);
    cname = EVP_CIPHER_name(type);
    cbsize = EVP_CIPHER_block_size(type);
    printf("encrypto nid: %d\n", cnid);
    printf("encrypto name: %s\n", cname);
    printf("encrypto block size: %d\n", cbsize);

    md = EVP_md5();
    mtype = EVP_MD_type(md);
    mnid = EVP_MD_nid(md);
    mname = EVP_MD_name(md);
    msize = EVP_MD_size(md);
    mbsize = EVP_MD_block_size(md);
    printf("md info:\n");
    printf("md type: %d\n", mtype);
    printf("md nid: %d\n", mnid);
    printf("md name: %s\n", mname);
    printf("md size: %d\n", msize);
    printf("md block size: %d\n", mbsize);

    for (i = 0; i < 100; i++)
        memset(&data[i], i, 1);

    for (i = 0; i < 20; i++)
        memset(&salt[i], i, 1);

    generateKeyAndIV(type, md, salt, data, 100, 2, &key, &iv);

    free(key);
    free(iv);
    return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

int main()
{
    int cnid, ret, i, msize, mtype;
    int mpktype, cbsize, mnid, mbsize;
    const EVP_CIPHER *type;
    const EVP_MD *md;
    int datal, count, keyl, ivl;
    unsigned char salt[20], data[100], *key, *iv;
    const char *cname, *mname;

    type = EVP_des_ecb();
    cnid = EVP_CIPHER_nid(type);
    cname = EVP_CIPHER_name(type);
    cbsize = EVP_CIPHER_block_size(type);
    printf("encrypto nid : %d\n", cnid);
    printf("encrypto name: %s\n", cname);
    printf("encrypto block size : %d\n", cbsize);

    md = EVP_md5();
    mtype = EVP_MD_type(md);
    mnid = EVP_MD_nid(md);
    mname = EVP_MD_name(md);
    mpktype = EVP_MD_pkey_type(md);
    msize = EVP_MD_size(md);
    mbsize = EVP_MD_block_size(md);
    printf("md info : \n");
    printf("md type : %d\n", mtype);
    printf("md nid : %d\n", mnid);
    printf("md name : %s\n", mname);
    printf("md pkey type : %d\n", mpktype);
    printf("md size : %d\n", msize);
    printf("md block size : %d\n", mbsize);

    keyl = EVP_CIPHER_key_length(type);
    key = (unsigned char *)malloc(keyl);
    ivl = EVP_CIPHER_iv_length(type);
    iv = (unsigned char *)malloc(ivl);

    for (i = 0; i < 100; i++)
        memset(&data[i], i, 1);

    for (i = 0; i < 20; i++)
        memset(&salt[i], i, 1);

    datal = 100;
    count = 2;
    ret = EVP_BytesToKey(type, md, salt, data, datal, count, key, iv);

    if (ret == 0) {
        printf("Failed to generate key and iv.\n");
        free(key);
        free(iv);
        return 1;
    }

    printf("Generated key value: \n");
   for (i = 0; i < keyl; i++)
        printf("%02x ", *(key + i));
    printf("\n");

    printf("Generated IV value: \n");
    for (i = 0; i < ivl; i++)
        printf("%02x ", *(iv + i));
    printf("\n");

    free(key);
    free(iv);
    return 0;
}*/
/*#include <string.h>
#include <openssl/evp.h>
int main()
{
int cnid,ret,i,msize,mtype;
int mpktype,cbsize,mnid,mbsize;
const EVP_CIPHER *type;
const EVP_MD *md;
int datal,count,keyl,ivl;
unsigned char salt[20],data[100],*key,*iv;
const char *cname,*mname;
type=EVP_des_ecb();
cnid=EVP_CIPHER_nid(type);
cname=EVP_CIPHER_name(type);
cbsize=EVP_CIPHER_block_size(type);
printf("encrypto nid : %d\n",cnid);
printf("encrypto name: %s\n",cname);
printf("encrypto bock size : %d\n",cbsize);
md=EVP_md5();
mtype=EVP_MD_type(md);
mnid=EVP_MD_nid(md);
mname=EVP_MD_name(md);
mpktype=EVP_MD_pkey_type(md);
msize=EVP_MD_size(md);
mbsize=EVP_MD_block_size(md);
printf("md info : \n");
printf("md type : %d\n",mtype);
printf("md nid : %d\n",mnid);
printf("md name : %s\n",mname);
printf("md pkey type : %d\n",mpktype);
printf("md size : %d\n",msize);
printf("md block size : %d\n",mbsize);
keyl=EVP_CIPHER_key_length(type);
key=(unsigned char *)malloc(keyl);
ivl=EVP_CIPHER_iv_length(type);
iv=(unsigned char *)malloc(ivl);
for(i=0;i<100;i++)
memset(&data[i],i,1);
for(i=0;i<20;i++)
memset(&salt[i],i,1);
datal=100;
count=2;
ret=EVP_BytesToKey(type,md,salt,data,datal,count,key,iv);
printf("generate key value: \n");
for(i=0;i<keyl;i++)
printf("%x ",*(key+i));
printf("\n");
printf("generate iv value: \n");
for(i=0;i<ivl;i++)
printf("%x ",*(iv+i));
printf("\n");
return 0;
}
*/
