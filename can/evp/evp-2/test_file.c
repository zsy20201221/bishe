#include <CUnit/Basic.h>
#include <openssl/evp.h>

// 测试用例: 检查 generateKeyAndIV 函数是否能够成功执行
void test_generateKeyAndIV()
{
    const EVP_CIPHER *type = EVP_des_ecb();
    const EVP_MD *md = EVP_md5();
    unsigned char salt[20];
    unsigned char data[100];
    int datal = 100;
    int count = 2;
    unsigned char *key = NULL;
    unsigned char *iv = NULL;

    generateKeyAndIV(type, md, salt, data, datal, count, &key, &iv);

    // 断言生成的密钥和IV不为NULL
    CU_ASSERT_PTR_NOT_NULL(key);
    CU_ASSERT_PTR_NOT_NULL(iv);

    free(key);
    free(iv);
}

// 注册测试用例
void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("KeyAndIV", NULL, NULL);
    CU_add_test(suite, "test_generateKeyAndIV", test_generateKeyAndIV);
}

int main()
{
    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 注册测试套件
    add_test_suite();

    // 运行所有测试套件
    CU_basic_run_tests();

    // 清理 CUnit 资源
    CU_cleanup_registry();

    return CU_get_error();
}
