#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include "random.h"
#include <time.h>

typedef int (*callback_random)(char *random, int len);

static callback_random cb_rand = NULL;

void set_callback(callback_random cb)
{
    cb_rand = cb;
}

int generate_random(char *random, int len)
{
    if (cb_rand == NULL)
    {
        // 使用默认的随机数生成逻辑
        int i;
        for (i = 0; i < len; i++)
        {
            random[i] = rand() % 256;
        }
    }
    else
    {
        return (*cb_rand)(random, len);
    }

    return 0;
}

 int my_rand(char *random, int len)
{
    int i;
    for (i = 0; i < len; i++)
    {
        random[i] = 0x02;
    }
    return 0;
}

int main()
{
    char random[10];
    int ret;
    //set_callback(my_rand);

    // 初始化随机数种子
    srand(time(NULL));

    ret = generate_random(random, 10);

    // 打印生成的随机数
    int i;
    printf("Random numbers: ");
    for (i = 0; i < 10; i++)
    {
        printf("%02X ", (unsigned char)random[i]);
    }
    printf("\n");

    return 0;
}
/*#include "random.h"
#include <stdio.h>

callback_random *cb_rand = NULL;

static void default_random(char *random, int len)
{
    memset(random, 0x01, len);
}

void set_callback(callback_random *cb)
{
    cb_rand = cb;
}

int generate_random(char *random, int len)
{
    if (cb_rand == NULL)
        default_random(random, len);
    else
    return (*cb_rand)(random, len);

    return 0;
}*/
