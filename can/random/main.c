#include "random.h"
#include <stdio.h>

static int my_rand(char *rand, int len)
{
    memset(rand, 0x02, len);
    return 0;
}

int main()
{
    char random[10];
    int ret;
    set_callback(my_rand);
    ret = generate_random(random, 10);

    // 打印生成的随机数
    int i;
    printf("Random numbers: ");
    for (i = 0; i < 10; i++)
    {
        printf("%02X ", (unsigned char)random[i]);
    }
    printf("\n");

    return 0;
}
/*#include "random.h"
static int my_rand(char *rand,int len)
{

memset(rand,0x02,len);
return 0;
}
int main()
{
char random[10];
int ret;
set_callback(my_rand);
ret=genrate_random(random,10);
//以下为gpt添加在终端上查看生成的随机数
printf("Generated random numbers: ");
for (int i = 0; i < 10; i++) {
    printf("%02X ", random[i]);
}
printf("\n");
//end
return 0;
}*/

