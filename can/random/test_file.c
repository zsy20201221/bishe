#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <string.h>

// 原始的 generate_random 函数
int generate_random(char *random, int len);
int my_rand(char *random, int len);

// 用于设置回调的函数指针类型
typedef int (*callback_random)(char *random, int len);

// 设置回调函数
void set_callback(callback_random cb);

// 辅助函数：比较两个数组是否相等
int compareArrays(char *arr1, char *arr2, int len)
{
    for (int i = 0; i < len; i++)
    {
        if (arr1[i] != arr2[i])
        {
            return 0; // 不相等
        }
    }
    return 1; // 相等
}

// 测试回调函数 my_rand
void test_my_rand()
{
    char expected[10];
    char actual[10];

    // 期望 my_rand 函数生成的结果
    for (int i = 0; i < 10; i++)
    {
        expected[i] = 0x02;
    }

    // 设置回调函数
    set_callback(my_rand);

    // 调用 generate_random
    generate_random(actual, 10);

    // 检查生成的随机数是否与期望相等
    CU_ASSERT(compareArrays(actual, expected, 10));
}

// 测试默认的 generate_random 函数
void test_generate_random_default(void)
{
    char actual[10];
    int ret = generate_random(actual, 10);

    CU_ASSERT_EQUAL(ret, 0);

    for (int i = 0; i < 10; i++)
    {
        // 修改测试条件
        CU_ASSERT_TRUE(actual[i] >= 0 && actual[i] <= 255);
    }
}
// 初始化测试套件
int init_suite(void)
{
    return 0; // 成功初始化
}

// 清理测试套件
int clean_suite(void)
{
    return 0; // 成功清理
}

// 主函数
int main()
{
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("Suite", init_suite, clean_suite);

    // 向套件中添加测试用例
    CU_add_test(suite, "test_my_rand", test_my_rand);
    CU_add_test(suite, "test_generate_random_default", test_generate_random_default);

    // 运行所有测试
    CU_basic_run_tests();

    // 清理 CUnit 测试框架
    CU_cleanup_registry();

    return 0;
}

