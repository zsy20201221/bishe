#ifndef RANDOM_H
#define RANDOM_H

typedef int (*callback_random)(char *random, int len);

void set_callback(callback_random *cb);
int generate_random(char *random, int len);

#endif
