#include <CUnit/Basic.h>
#include <openssl/err.h>

// 测试用例: 检查 handleErrors 函数是否能够成功执行
void test_handleErrors()
{
    // 执行 handleErrors 函数
    handleErrors();

    // 断言没有发生错误
    CU_ASSERT_EQUAL(ERR_get_error(), 0);
}

// 注册测试用例
void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("Errors", NULL, NULL);
    CU_add_test(suite, "test_handleErrors", test_handleErrors);
}

int main()
{
    // 初始化 CUnit 测试框架
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    // 注册测试套件
    add_test_suite();

    // 运行所有测试套件
    CU_basic_run_tests();

    // 清理 CUnit 资源
    CU_cleanup_registry();

    return CU_get_error();
}
