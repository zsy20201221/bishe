#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/asn1.h>

int performBioDump(const char *filename, int indent)
{
    int ret, len;
    BIO *bp;
    char *pp, buf[5000];
    FILE *fp;

    bp = BIO_new(BIO_s_file());
    if (bp == NULL) {
        fprintf(stderr, "Failed to create BIO object\n");
        return 1;
    }

    BIO_set_fp(bp, stdout, BIO_NOCLOSE);

    fp = fopen(filename, "rb");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file\n");
        BIO_free(bp);
        return 1;
    }

    len = fread(buf, 1, 5000, fp);
    if (len <= 0) {
        fprintf(stderr, "Failed to read file\n");
        fclose(fp);
        BIO_free(bp);
        return 1;
    }

    fclose(fp);

    pp = buf;
    ret = BIO_dump_indent(bp, pp, len, indent);

    if (ret <= 0) {
        fprintf(stderr, "Failed to perform BIO dump\n");
        BIO_free(bp);
        return 1;
    }

    BIO_free(bp);

    return 0;
}

/*int main()
{
    const char *filename = "der.cer";
    int indent = 5;

    int result = performBioDump(filename, indent);

    return result;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/asn1.h>

int main()
{
    int ret, len, indent;
    BIO *bp;
    char *pp, buf[5000];
    FILE *fp;

    bp = BIO_new(BIO_s_file());
    if (bp == NULL) {
        fprintf(stderr, "Failed to create BIO object\n");
        return 1;
    }

    BIO_set_fp(bp, stdout, BIO_NOCLOSE);

    fp = fopen("/path/to/der.cer", "rb");//填写具体路径
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file\n");
        BIO_free(bp);
        return 1;
    }

    len = fread(buf, 1, 5000, fp);
    if (len <= 0) {
        fprintf(stderr, "Failed to read file\n");
        fclose(fp);
        BIO_free(bp);
        return 1;
    }

    fclose(fp);

    pp = buf;
    indent = 5;
    ret = BIO_dump_indent(bp, pp, len, indent);

    if (ret <= 0) {
        fprintf(stderr, "Failed to perform BIO dump\n");
        BIO_free(bp);
        return 1;
    }

    BIO_free(bp);

    return 0;
}*/
/*#include <openssl/bio.h>
#include <openssl/asn1.h>
int main()
{
int ret,len,indent;
BIO *bp;
char *pp,buf[5000];
FILE *fp;
bp=BIO_new(BIO_s_file());
BIO_set_fp(bp,stdout,BIO_NOCLOSE);
fp=fopen("der.cer","rb");
len=fread(buf,1,5000,fp);
fclose(fp);
pp=buf;
indent=5;
ret=BIO_dump_indent(bp,pp,len,indent);
BIO_free(bp);
return 0;
}*/
