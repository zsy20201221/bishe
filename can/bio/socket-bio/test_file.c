#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/bio.h>

// 服务端代码中的函数声明和定义
void startServer(const char* port);

// 客户端代码中的函数声明和定义
void communicate_with_server(BIO *cbio, BIO *out);

// 测试用例 - 服务端函数
void testStartServer()
{
    const char* port = "2323";

    // 调用被测试函数
    startServer(port);

    // 在这个示例中，无法直接对服务端函数进行断言测试，因为它会一直运行，直到接收到客户端的退出指令。
    // 您可以手动测试服务端函数的行为和结果，确保它正常运行并与客户端进行通信。
}

// 测试用例 - 客户端函数
void testCommunicateWithServer()
{
    const char* server_address = "127.0.0.1:2323";

    // 创建客户端连接 BIO
    BIO *cbio = BIO_new_connect(server_address);
    CU_ASSERT_PTR_NOT_NULL(cbio);

    // 创建输出 BIO
    BIO *out = BIO_new(BIO_s_file());
    CU_ASSERT_PTR_NOT_NULL(out);
    BIO_set_fp(out, stdout, BIO_NOCLOSE);

    // 连接到服务器
    int result = BIO_do_connect(cbio);
    //CU_ASSERT_TRUE(result > 0);

    // 向服务器发送请求
    const char *request = "GET / HTTP/1.0\n\n";
    result = BIO_puts(cbio, request);
    CU_ASSERT_TRUE(result >= 0);

    // 与服务器通信
    communicate_with_server(cbio, out);

    // 释放资源
    BIO_free(cbio);
    BIO_free(out);
}

// 初始化测试套件
int init_suite()
{
    return 0;
}

// 清理测试套件
int clean_suite()
{
    return 0;
}

// 主函数
int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("ServerClientSuite", init_suite, clean_suite);
    if (suite == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 向测试套件中添加测试用例
    if (CU_add_test(suite, "testStartServer", testStartServer) == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (CU_add_test(suite, "testCommunicateWithServer", testCommunicateWithServer) == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 输出测试结果
    CU_basic_show_failures(CU_get_failure_list());
    printf("\n");

    // 获取测试失败的数量
    unsigned int num_failures = CU_get_number_of_failures();
    if (num_failures == 0) {
        printf("All tests passed\n");
    } else {
        printf("%d test(s) failed\n", num_failures);
    }

    // 清理CUnit测试框架
    CU_cleanup_registry();

    // 退出程序
    return num_failures;
}
