#include <stdio.h>
#include <openssl/bio.h>
#include <string.h>

void communicate_with_client(BIO *b_io) {
    int len;
    char out[80];

    // 从客户端读取数据
    while (1) {
        memset(out, 0, 80);
        len = BIO_read(b_io, out, 80);

        if (len <= 0) {
            if (BIO_should_retry(b_io)) {
                // 需要重试
                continue;
            } else {
                perror("从客户端读取数据出错");
                break;
            }
        }

        if (out[0] == 'q') {
            break;
        }

        printf("%s\n", out);

        // 回复客户端
        const char* response = "Message received\n";
        BIO_write(b_io, response, strlen(response));
    }
}

void startServer(const char* port) {
    BIO *b_accept = NULL, *b_io = NULL;

    // 创建用于监听的 BIO
    b_accept = BIO_new_accept(port);
    if (b_accept == NULL) {
        perror("创建监听 BIO 出错");
        return;
    }

    // 设置监听 BIO
    if (BIO_do_accept(b_accept) <= 0) {
        perror("监听 BIO 设置出错");
        BIO_free(b_accept);
        return;
    }

    printf("等待连接...\n");

    // 等待并接受连接
    while (1) {
        if (BIO_do_accept(b_accept) <= 0) {
            perror("等待连接出错");
            continue;
        }

        printf("连接已接受。\n");

        // 获取与客户端通信的 BIO
        b_io = BIO_pop(b_accept);

        // 与客户端通信
        communicate_with_client(b_io);

        // 释放资源
        BIO_free(b_io);
        
        // 重新接受连接
        if (BIO_do_accept(b_accept) <= 0) {
            perror("等待连接出错");
            continue;
        }
    }

    // 释放资源
    BIO_free(b_accept);
}

int main() {
    const char* port = "2323";
    startServer(port);
    return 0;
}

