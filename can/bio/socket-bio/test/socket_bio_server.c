#include <stdio.h>
#include <openssl/bio.h>
#include <string.h>

void startServer(const char* port) {
    BIO *b_accept = NULL, *b_io = NULL;
    int len;
    char out[80];

    // 创建用于监听的 BIO
    b_accept = BIO_new_accept(port);
    if (b_accept == NULL) {
        perror("创建监听 BIO 出错");
        return;
    }

    // 设置监听 BIO
    if (BIO_do_accept(b_accept) <= 0) {
        perror("监听 BIO 设置出错");
        BIO_free(b_accept);
        return;
    }

    // 等待并接受连接
    if (BIO_do_accept(b_accept) <= 0) {
        perror("等待连接出错");
        BIO_free(b_accept);
        return;
    }

    printf("连接已接受。\n");

    // 获取与客户端通信的 BIO
    b_io = BIO_pop(b_accept);

    // 从客户端读取数据
    while (1) {
        memset(out, 0, 80);
        len = BIO_read(b_io, out, 80);

        if (len <= 0) {
            if (BIO_should_retry(b_io)) {
                // 需要重试
                continue;
            } else {
                perror("从客户端读取数据出错");
                break;
            }
        }

        if (out[0] == 'q') {
            break;
        }

        printf("%s", out);
    }

    // 释放资源
    BIO_free(b_accept);
    BIO_free(b_io);
}

/*int main() {
    const char* port = "2323";
    startServer(port);
    return 0;
}*/

/*#include <stdio.h>
#include <openssl/bio.h>
#include <string.h>

int main() {
    BIO *b_accept = NULL, *b_io = NULL;
    int len;
    char out[80];

    // 创建用于监听的 BIO
    b_accept = BIO_new_accept("2323");
    if (b_accept == NULL) {
        perror("创建监听 BIO 出错");
        return 1;
    }

    // 设置监听 BIO
    if (BIO_do_accept(b_accept) <= 0) {
        perror("监听 BIO 设置出错");
        BIO_free(b_accept);
        return 1;
    }

    // 等待并接受连接
    if (BIO_do_accept(b_accept) <= 0) {
        perror("等待连接出错");
        BIO_free(b_accept);
        return 1;
    }

    printf("连接已接受。\n");

    // 获取与客户端通信的 BIO
    b_io = BIO_pop(b_accept);

    // 从客户端读取数据
    while (1) {
        memset(out, 0, 80);
        len = BIO_read(b_io, out, 80);

        if (len <= 0) {
            if (BIO_should_retry(b_io)) {
                // 需要重试
                continue;
            } else {
                perror("从客户端读取数据出错");
                break;
            }
        }

        if (out[0] == 'q') {
            break;
        }

        printf("%s", out);
    }

    // 释放资源
    BIO_free(b_accept);
    BIO_free(b_io);

    return 0;
}*/

/*#include <stdio.h>
#include <openssl/bio.h>
#include <string.h>
int main()
{
BIO *b=NULL,*c=NULL;
int sock,ret,len;
char *addr=NULL;
char out[80];
sock=BIO_get_accept_socket("2323",0);
b=BIO_new_socket(sock, BIO_NOCLOSE);
ret=BIO_accept(sock,&addr);
BIO_set_fd(b,ret,BIO_NOCLOSE);
while(1)
{
memset(out,0,80);
len=BIO_read(b,out,80);
if(out[0]=='q')
break;
printf("%s",out);
}
BIO_free(b);
return 0;
}*/
