#include <openssl/bio.h>
#include <stdbool.h>

void communicate_with_server(BIO *cbio, BIO *out) {
    int len;
    char tmpbuf[1024];

    // 读取服务器响应，并在读取到的数据的第一个字节是 'q' 时退出循环
    while ((len = BIO_read(cbio, tmpbuf, 1024)) > 0) {
        BIO_write(out, tmpbuf, len);

        // 检查读取到的数据的第一个字节是否是 'q'
        if (tmpbuf[0] == 'q') {
            break; // 如果是 'q'，退出循环
        }
    }
}

/*int main() {
    BIO *cbio, *out;

    cbio = BIO_new_connect("127.0.0.1:2323"); // 指定正确的端口
    out = BIO_new_fp(stdout, BIO_NOCLOSE);

    if (BIO_do_connect(cbio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        BIO_free(cbio);
        BIO_free(out);
        return 1; // 返回非零值表示连接失败
    }

    BIO_puts(cbio, "GET / HTTP/1.0\n\n");

    // 与服务器通信
    communicate_with_server(cbio, out);

    // 释放资源
    BIO_free(cbio);
    BIO_free(out);

    return 0; // 返回零值表示连接和读取成功
}*/

/*#include <openssl/bio.h>
#include <stdbool.h>

int main() {
    BIO *cbio, *out;
    int len;
    char tmpbuf[1024];

    cbio = BIO_new_connect("127.0.0.1:2323"); // 指定正确的端口
    out = BIO_new_fp(stdout, BIO_NOCLOSE);

    if (BIO_do_connect(cbio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        BIO_free(cbio);
        BIO_free(out);
        return 1; // 返回非零值表示连接失败
    }

    BIO_puts(cbio, "GET / HTTP/1.0\n\n");

    // 读取服务器响应，并在读取到的数据的第一个字节是 'q' 时退出循环
    while ((len = BIO_read(cbio, tmpbuf, 1024)) > 0) {
        BIO_write(out, tmpbuf, len);

        // 检查读取到的数据的第一个字节是否是 'q'
        if (tmpbuf[0] == 'q') {
            break; // 如果是 'q'，退出循环
        }
    }

    BIO_free(cbio);
    BIO_free(out);

    return 0; // 返回零值表示连接和读取成功
}
*/
/*#include <openssl/bio.h>
int main()
{
BIO *cbio, *out;
 int len;
 char tmpbuf[1024];
 
 cbio = BIO_new_connect("127.0.0.1:23");//ifconfig 
 out = BIO_new_fp(stdout, BIO_NOCLOSE);
 if(BIO_do_connect(cbio) <= 0) 
 {
 fprintf(stderr, "Error connecting to server\n");
 }
 BIO_puts(cbio, "GET / HTTP/1.0\n\n");
 for(;;) 
 {
 len = BIO_read(cbio, tmpbuf, 1024);
 if(len <= 0) break;
 BIO_write(out, tmpbuf, len);
 }
 BIO_free(cbio);
 BIO_free(out);
 return 0;
 }*/
