#include <openssl/bio.h>
#include <stdio.h>
#include <string.h>

void communicate_with_server(BIO *cbio, BIO *out) {
    int len;
    char tmpbuf[1024];

    while (1) {
        // 从标准输入读取用户输入的消息
        char input[1024];
        fgets(input, sizeof(input), stdin);

        // 移除换行符
        input[strcspn(input, "\n")] = '\0';

        // 发送消息给服务器
        BIO_write(cbio, input, strlen(input));

        // 读取服务器响应
        len = BIO_read(cbio, tmpbuf, sizeof(tmpbuf) - 1);
        if (len <= 0) {
            break; // 发生错误或连接关闭，退出循环
        }

        tmpbuf[len] = '\0';

        // 打印服务器响应
        printf("Server response: %s\n", tmpbuf);

        // 检查是否退出循环
        if (strcmp(tmpbuf, "q") == 0) {
            break;
        }
    }
}

int main() {
    BIO *cbio, *out;

    cbio = BIO_new_connect("127.0.0.1:2323");
    out = BIO_new_fp(stdout, BIO_NOCLOSE);

    if (BIO_do_connect(cbio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        BIO_free(cbio);
        BIO_free(out);
        return 1;
    }

    printf("Connected to server. Enter 'q' to quit.\n");

    // 与服务器通信
    communicate_with_server(cbio, out);

    // 释放资源
    BIO_free(cbio);
    BIO_free(out);

    return 0;
}
