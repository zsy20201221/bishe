#include <CUnit/Basic.h>
#include <openssl/evp.h>

void test_encryptAndDecryptData() {
    unsigned char key[16], iv[16];
    const EVP_CIPHER *cipher = EVP_aes_128_ecb();

    for (int i = 0; i < 16; i++) {
        memset(&key[i], i + 1, 1);
        memset(&iv[i], i + 1, 1);
    }

    char input[] = "openssl";
    int inputLength = strlen(input);

    // 保存标准输出
    FILE *original_stdout = stdout;
    FILE *output_file = fopen("output.txt", "w");

    // 重定向输出到文件
    stdout = output_file;

    // 调用被测试的函数
    encryptAndDecryptData(key, iv, cipher, input, inputLength);

    // 恢复标准输出
    fflush(stdout);
    fclose(output_file);
    stdout = original_stdout;

    // 读取输出文件内容
    char encryptedData[1024];
    char decryptedData[1024];

    FILE *fp = fopen("output.txt", "r");
    fscanf(fp, "加密后的数据：%[^\n]\n", encryptedData);
    fscanf(fp, "解密后的数据：%[^\n]\n", decryptedData);
    fclose(fp);

    // 进行断言验证
    CU_ASSERT_STRING_EQUAL(encryptedData, "output.txt");
    CU_ASSERT_STRING_EQUAL(decryptedData, "openssl");

    // 删除临时文件
    //remove("output.txt");
}

int main() {
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("EncryptionSuite", NULL, NULL);
    CU_add_test(suite, "test_encryptAndDecryptData", test_encryptAndDecryptData);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}
