#include <stdio.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

void encryptAndDecryptData(unsigned char *key, unsigned char *iv, const EVP_CIPHER *cipher, char *input, int inputLength)
{
    /* 加密 */
    BIO *bc = NULL, *b = NULL;
    int len;
    char tmp[1024];
    
    bc = BIO_new(BIO_f_cipher());
    BIO_set_cipher(bc, cipher, key, iv, 1);
    b = BIO_new(BIO_s_null());
    b = BIO_push(bc, b);
    len = BIO_write(b, input, inputLength);
    len = BIO_read(b, tmp, sizeof(tmp) - 1);
    tmp[len] = '\0';
    printf("加密后的数据：%s\n", tmp);
    BIO_free(b);

    /* 解密 */
    BIO *bdec = NULL, *bd = NULL;
    bdec = BIO_new(BIO_f_cipher());
    BIO_set_cipher(bdec, cipher, key, iv, 0);
    bd = BIO_new(BIO_s_null());
    bd = BIO_push(bdec, bd);
    len = BIO_write(bd, tmp, len);
    len = BIO_read(bd, tmp, sizeof(tmp) - 1);
    tmp[len] = '\0';
    printf("解密后的数据：%s\n", tmp);
    BIO_free(bdec);
}

/*int main()
{
    unsigned char key[16], iv[16];
    const EVP_CIPHER *cipher = EVP_aes_128_ecb();

    for (int i = 0; i < 16; i++)
    {
        memset(&key[i], i + 1, 1);
        memset(&iv[i], i + 1, 1);
    }

    char input[] = "openssl";
    int inputLength = strlen(input);

    encryptAndDecryptData(key, iv, cipher, input, inputLength);

    return 0;
}*/
/*#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

int main()
{
    // 加密
    BIO *bc = NULL, *b = NULL;
    const EVP_CIPHER *c = EVP_aes_128_ecb();  // 替换为其他加密算法
    int len, i;
    char tmp[1024];
    unsigned char key[16], iv[16];  // 根据选择的加密算法调整密钥和向量长度
    for (i = 0; i < 16; i++)
    {
        memset(&key[i], i + 1, 1);
        memset(&iv[i], i + 1, 1);
    }
    bc = BIO_new(BIO_f_cipher());
    BIO_set_cipher(bc, c, key, iv, 1);
    b = BIO_new(BIO_s_null());
    b = BIO_push(bc, b);
    len = BIO_write(b, "openssl", 7);
    len = BIO_read(b, tmp, 1024);
    BIO_free(b);

    // 解密
    BIO *bdec = NULL, *bd = NULL;
    const EVP_CIPHER *cd = EVP_aes_128_ecb();  // 替换为相同的加密算法
    bdec = BIO_new(BIO_f_cipher());
    BIO_set_cipher(bdec, cd, key, iv, 0);
    bd = BIO_new(BIO_s_null());
    bd = BIO_push(bdec, bd);
    len = BIO_write(bd, tmp, len);
    len = BIO_read(bd, tmp, 1024);
    BIO_free(bdec);

    return 0;
}*/

/*#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
int main()
{
// 加密 
BIO *bc=NULL,*b=NULL;
const EVP_CIPHER*c=EVP_des_ecb();
int len,i;
char tmp[1024];
unsigned char key[8],iv[8];
for(i=0;i<8;i++)
{
memset(&key[i],i+1,1);
memset(&iv[i],i+1,1);
}
bc=BIO_new(BIO_f_cipher());
BIO_set_cipher(bc,c,key,iv,1);
b= BIO_new(BIO_s_null());
b=BIO_push(bc,b);
len=BIO_write(b,"openssl",7);
len=BIO_read(b,tmp,1024);
BIO_free(b);
// 解密 
BIO *bdec=NULL,*bd=NULL;
const EVP_CIPHER*cd=EVP_des_ecb();
bdec=BIO_new(BIO_f_cipher());
BIO_set_cipher(bdec,cd,key,iv,0);
bd= BIO_new(BIO_s_null());
bd=BIO_push(bdec,bd);
len=BIO_write(bdec,tmp,len);
len=BIO_read(bdec,tmp,1024);
BIO_free(bdec);
return 0;
}*/

