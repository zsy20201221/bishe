#include <stdio.h>
#include <openssl/bio.h>

int processFile()
{
    BIO *b = NULL;
    int len = 0, outlen = 0;
    char *out = NULL;

    b = BIO_new_file("bf.txt", "w");
    if (b == NULL)
    {
        printf("Failed to open file for writing.\n");
        return 1;
    }

    len = BIO_write(b, "openssl", 4);
    if (len <= 0)
    {
        printf("Failed to write to file.\n");
        BIO_free(b);
        return 1;
    }

    len = BIO_printf(b, "%s", "zcp");
    if (len <= 0)
    {
        printf("Failed to write to file.\n");
        BIO_free(b);
        return 1;
    }

    BIO_free(b);

    b = BIO_new_file("bf.txt", "r");
    if (b == NULL)
    {
        printf("Failed to open file for reading.\n");
        return 1;
    }

    len = BIO_pending(b);

    out = (char *)OPENSSL_malloc(10);
    if (out == NULL)
    {
        printf("Failed to allocate memory.\n");
        BIO_free(b);
        return 1;
    }

    while ((len = BIO_read(b, out + outlen, 1)) > 0)
    {
        outlen += len;
    }

    BIO_free(b);
    OPENSSL_free(out);

    return 0;
}

/*int main()
{
    return processFile();
}*/
/*#include <stdio.h>
#include <openssl/bio.h>

int main()
{
    BIO *b = NULL;
    int len = 0, outlen = 0;
    char *out = NULL;

    b = BIO_new_file("bf.txt", "w");
    if (b == NULL)
    {
        printf("Failed to open file for writing.\n");
        return 1;
    }

    len = BIO_write(b, "openssl", 4);
    if (len <= 0)
    {
        printf("Failed to write to file.\n");
        BIO_free(b);
        return 1;
    }

    len = BIO_printf(b, "%s", "zcp");
    if (len <= 0)
    {
        printf("Failed to write to file.\n");
        BIO_free(b);
        return 1;
    }

    BIO_free(b);

    b = BIO_new_file("bf.txt", "r");
    if (b == NULL)
    {
        printf("Failed to open file for reading.\n");
        return 1;
    }

    len = BIO_pending(b);

    out = (char *)OPENSSL_malloc(10);
    if (out == NULL)
    {
        printf("Failed to allocate memory.\n");
        BIO_free(b);
        return 1;
    }

    while ((len = BIO_read(b, out + outlen, 1)) > 0)
    {
        outlen += len;
    }

    BIO_free(b);
    OPENSSL_free(out);

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
int main()
{
BIO *b=NULL;
int len=0,outlen=0;
 char *out=NULL;
 b=BIO_new_file("bf.txt","w");
 len=BIO_write(b,"openssl",4);
len=BIO_printf(b,"%s","zcp");
BIO_free(b);
b=BIO_new_file("bf.txt","r");
len=BIO_pending(b);
len=50;
out=(char *)OPENSSL_malloc(len);
len=1;
while(len>0)
{
len=BIO_read(b,out+outlen,1);
outlen+=len;
}
BIO_free(b);
free(out);
 return 0;
}*/

