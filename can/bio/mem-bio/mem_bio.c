#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/crypto.h>

void processBioOperations(const char *input, char **output)
{
    BIO *b = NULL;
    int len = 0;

    b = BIO_new(BIO_s_mem());
    len = BIO_write(b, input, strlen(input));
    len = BIO_printf(b, "%s", "zcp");
    len = BIO_ctrl_pending(b);

    char *out = (char *)OPENSSL_malloc(len + 1); // 加1是为了包含字符串结束符
    len = BIO_read(b, out, len);
    out[len] = '\0'; // 添加字符串结束符

    *output = out;

    BIO_free(b);
}

/*int main()
{
    const char *input = "openssl";
    char *output = NULL;

    processBioOperations(input, &output);

    if (output != NULL)
    {
        printf("Output: %s\n", output);
        OPENSSL_free(output);
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/crypto.h>

int main()
{
    BIO *b = NULL;
    int len = 0;
    char *out = NULL;

    b = BIO_new(BIO_s_mem());
    len = BIO_write(b, "openssl", strlen("openssl"));
    len = BIO_printf(b, "%s", "zcp");
    len = BIO_ctrl_pending(b);
    out = (char *)OPENSSL_malloc(len + 1); // 加1是为了包含字符串结束符
    len = BIO_read(b, out, len);
    out[len] = '\0'; // 添加字符串结束符

    printf("Output: %s\n", out);

    OPENSSL_free(out);
    BIO_free(b);

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
int main()
{
 BIO *b=NULL;
 int len=0;
 char *out=NULL;
 b=BIO_new(BIO_s_mem());
len=BIO_write(b,"openssl",4);
len=BIO_printf(b,"%s","zcp");
len=BIO_ctrl_pending(b);
out=(char *)OPENSSL_malloc(len);
len=BIO_read(b,out,len);
OPENSSL_free(out);
BIO_free(b);
return 0;
}
*/
