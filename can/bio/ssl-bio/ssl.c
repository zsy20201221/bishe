#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string.h>

int connectAndFetchData(const char *hostname, const char *data, char *response, int responseSize) {
    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    ERR_load_BIO_strings();

    // 创建 SSL 上下文
    SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());
    if (!ctx) {
        fprintf(stderr, "Error creating SSL context\n");
        return -1;
    }

    // 创建 BIO
    BIO *bio = BIO_new_ssl_connect(ctx);
    if (!bio) {
        fprintf(stderr, "Error creating SSL connection BIO\n");
        SSL_CTX_free(ctx);
        return -1;
    }

    // 获取 SSL 对象
    SSL *ssl;
    BIO_get_ssl(bio, &ssl);
    if (!ssl) {
        fprintf(stderr, "Can not locate SSL pointer\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    BIO_set_conn_hostname(bio, hostname);

    // 建立连接
    if (BIO_do_connect(bio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    // 发送数据
    int len = BIO_write(bio, data, strlen(data));
    if (len < 0) {
        fprintf(stderr, "Error sending data\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    // 读取响应数据
    int totalBytesRead = 0;
    int bytesRead;
    while ((bytesRead = BIO_read(bio, response + totalBytesRead, responseSize - totalBytesRead - 1)) > 0) {
        totalBytesRead += bytesRead;
    }
    response[totalBytesRead] = '\0';

    // 释放资源
    BIO_free_all(bio);
    SSL_CTX_free(ctx);

    return totalBytesRead;
}

/*int main() {
    const char *hostname = "www.baidu.com:https";
    const char *data = "GET / HTTP/1.1\r\nHost: www.baidu.com\r\n\r\n";
    char response[4096];

    int bytesRead = connectAndFetchData(hostname, data, response, sizeof(response));
    if (bytesRead < 0) {
        fprintf(stderr, "Error connecting and fetching data\n");
        return -1;
    }

    printf("Response:\n%s\n", response);

    return 0;
}*/
/*#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string.h>

int main() {
    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    ERR_load_BIO_strings();

    // 创建 SSL 上下文
    SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());
    if (!ctx) {
        fprintf(stderr, "Error creating SSL context\n");
        return -1;
    }

    // 创建 BIO
    BIO *bio = BIO_new_ssl_connect(ctx);
    if (!bio) {
        fprintf(stderr, "Error creating SSL connection BIO\n");
        SSL_CTX_free(ctx);
        return -1;
    }

    // 获取 SSL 对象
    SSL *ssl;
    BIO_get_ssl(bio, &ssl);
    if (!ssl) {
        fprintf(stderr, "Can not locate SSL pointer\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    BIO_set_conn_hostname(bio, "www.example.com:https");

    // 建立连接
    if (BIO_do_connect(bio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    // 发送数据
    const char *data = "GET / HTTP/1.1\r\nHost: www.baidu.com\r\n\r\n";
    int len = BIO_write(bio, data, strlen(data));

    if (len < 0) {
        fprintf(stderr, "Error sending data\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    // 读取响应数据
    char buf[1024];
    int bytes;
    while ((bytes = BIO_read(bio, buf, sizeof(buf) - 1)) > 0) {
        buf[bytes] = '\0';
        printf("%s", buf);
    }

    // 释放资源
    BIO_free_all(bio);
    SSL_CTX_free(ctx);

    return 0;
}*/
