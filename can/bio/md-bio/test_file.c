#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <string.h>

// 函数声明
int calculateDigest(const char *input, unsigned char *digest, size_t digestLen);

// 测试用例
void testCalculateDigest()
{
    const char *input = "Hello, World!";
    unsigned char digest[16];
    size_t digestLen = sizeof(digest);

    // 调用被测试函数
    int result = calculateDigest(input, digest, digestLen);

    // 断言结果
    CU_ASSERT_EQUAL(result, 0);
    // 添加更多的断言来验证digest的正确性，例如：
    // CU_ASSERT_EQUAL(memcmp(digest, expectedDigest, digestLen), 0);
}

// 初始化测试套件
int init_suite()
{
    return 0;
}

// 清理测试套件
int clean_suite()
{
    return 0;
}

// 主函数
int main()
{
    // 初始化CUnit测试框架
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("calculateDigest Suite", init_suite, clean_suite);
    if (suite == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 向测试套件中添加测试用例
    if (CU_add_test(suite, "testCalculateDigest", testCalculateDigest) == NULL)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架
    CU_cleanup_registry();

    return CU_get_error();
}
