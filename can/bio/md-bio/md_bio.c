#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

int calculateDigest(const char *input, unsigned char *digest, size_t digestLen)
{
    BIO *bmd = NULL, *b = NULL;
    const EVP_MD *md = EVP_md5();
    int len;

    bmd = BIO_new(BIO_f_md());
    if (bmd == NULL)
    {
        printf("Failed to create BIO_f_md.\n");
        return 1;
    }

    BIO_set_md(bmd, md);
    
    b = BIO_new(BIO_s_null());
    if (b == NULL)
    {
        printf("Failed to create BIO_s_null.\n");
        BIO_free(bmd);
        return 1;
    }

    b = BIO_push(bmd, b);
    
    len = BIO_write(b, input, strlen(input));
    if (len <= 0)
    {
        printf("Failed to write to BIO.\n");
        BIO_free_all(b);
        return 1;
    }

    len = BIO_gets(b, (char *)digest, digestLen);
    if (len <= 0)
    {
        printf("Failed to get digest from BIO.\n");
        BIO_free_all(b);
        return 1;
    }

    BIO_free_all(b);

    return 0;
}

/*int main()
{
    char input[] = "openssl";
    unsigned char digest[16]; // MD5摘要值为16字节
    size_t digestLen = sizeof(digest);

    int result = calculateDigest(input, digest, digestLen);
    if (result == 0)
    {
        printf("Digest: ");
        for (size_t i = 0; i < digestLen; i++)
        {
            printf("%02x", digest[i]);
        }
        printf("\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

int main()
{
    BIO *bmd = NULL, *b = NULL;
    const EVP_MD *md = EVP_md5();
    int len;
    char tmp[1024];

    bmd = BIO_new(BIO_f_md());
    if (bmd == NULL)
    {
        printf("Failed to create BIO_f_md.\n");
        return 1;
    }

    BIO_set_md(bmd, md);
    
    b = BIO_new(BIO_s_null());
    if (b == NULL)
    {
        printf("Failed to create BIO_s_null.\n");
        BIO_free(bmd);
        return 1;
    }

    b = BIO_push(bmd, b);
    
    len = BIO_write(b, "openssl", 7);
    if (len <= 0)
    {
        printf("Failed to write to BIO.\n");
        BIO_free_all(b);
        return 1;
    }

    len = BIO_gets(b, tmp, sizeof(tmp));
    if (len <= 0)
    {
        printf("Failed to get digest from BIO.\n");
        BIO_free_all(b);
        return 1;
    }

    printf("Digest: %s\n", tmp);

    BIO_free_all(b);

    return 0;
}*/
/*#include <openssl/bio.h>
#include <openssl/evp.h>
int main()
{
BIO *bmd=NULL,*b=NULL;
const EVP_MD *md=EVP_md5();
int len;
char tmp[1024];
bmd=BIO_new(BIO_f_md());
BIO_set_md(bmd,md);
b= BIO_new(BIO_s_null());
b=BIO_push(bmd,b);
len=BIO_write(b,"openssl",7);
len=BIO_gets(b,tmp,1024);
BIO_free(b);
return 0;
}*/
