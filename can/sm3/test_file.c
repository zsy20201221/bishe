#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/evp.h>
#include <openssl/sm3.h>


void testSM3Hash(void) {
    const unsigned char* message = (unsigned char*)"hello world";
    size_t messageLen = strlen((const char*)message);
    unsigned char expectedDigest[EVP_MAX_MD_SIZE];

    // 计算预期的 SM3 摘要
    EVP_MD_CTX* ctx = EVP_MD_CTX_new();
    if (ctx == NULL) {
        printf("Error creating EVP_MD_CTX.\n");
        CU_FAIL("Error creating EVP_MD_CTX.");
        return;
    }

    if (EVP_DigestInit_ex(ctx, EVP_sm3(), NULL) != 1) {
        printf("Error initializing SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        CU_FAIL("Error initializing SM3 digest.");
        return;
    }

    if (EVP_DigestUpdate(ctx, message, messageLen) != 1) {
        printf("Error updating SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        CU_FAIL("Error updating SM3 digest.");
        return;
    }

    if (EVP_DigestFinal_ex(ctx, expectedDigest, NULL) != 1) {
        printf("Error getting SM3 digest result.\n");
        EVP_MD_CTX_free(ctx);
        CU_FAIL("Error getting SM3 digest result.");
        return;
    }

    EVP_MD_CTX_free(ctx);

    // 调用被测试的函数
    unsigned char digest[EVP_MAX_MD_SIZE];
    sm3_hash(message, messageLen, digest);

    // 验证结果是否符合预期
    CU_ASSERT_NSTRING_EQUAL(digest, expectedDigest, EVP_MD_size(EVP_sm3()));
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("SM3HashSuite", NULL, NULL);

    // 将测试用例添加到测试套件中
    CU_add_test(suite, "testSM3Hash", testSM3Hash);

    // 设置 CUnit 基本模式并运行测试套件
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return 0;
}
