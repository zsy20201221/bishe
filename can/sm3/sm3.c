#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

void sm3_hash(const unsigned char* message, size_t messageLen, unsigned char* digest) {
    EVP_MD_CTX* ctx;
    const EVP_MD* md;
    unsigned int digestLen;

    ctx = EVP_MD_CTX_new();
    if (ctx == NULL) {
        printf("Error creating EVP_MD_CTX.\n");
        return;
    }

    md = EVP_sm3();
    if (md == NULL) {
        printf("Error getting SM3 algorithm.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    if (EVP_DigestInit_ex(ctx, md, NULL) != 1) {
        printf("Error initializing SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    if (EVP_DigestUpdate(ctx, message, messageLen) != 1) {
        printf("Error updating SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    if (EVP_DigestFinal_ex(ctx, digest, &digestLen) != 1) {
        printf("Error getting SM3 digest result.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    EVP_MD_CTX_free(ctx);
}

/*int main() {
    const int maxMessageSize = 1024;
    unsigned char message[maxMessageSize];
    unsigned char digest[EVP_MAX_MD_SIZE];
    unsigned int digestLen;

    printf("Enter the message to be hashed: ");
    fgets(message, maxMessageSize, stdin);

    // Remove the trailing newline character
    message[strcspn(message, "\n")] = '\0';

    printf("Message: %s\n", message);

    sm3_hash(message, strlen(message), digest);

    printf("SM3 Digest: ");
    for (int i = 0; i < EVP_MD_size(EVP_sm3()); i++) {
        printf("%02x", digest[i]);
    }
    printf("\n");

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/evp.h>

void sm3_hash(const unsigned char* message, size_t messageLen, unsigned char* digest) {
    EVP_MD_CTX* ctx;
    const EVP_MD* md;
    unsigned int digestLen;

    // 创建并初始化EVP_MD_CTX
    ctx = EVP_MD_CTX_new();
    if (ctx == NULL) {
        printf("Error creating EVP_MD_CTX.\n");
        return;
    }

    // 获取SM3摘要算法
    md = EVP_sm3();
    if (md == NULL) {
        printf("Error getting SM3 algorithm.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    // 初始化SM3摘要计算
    if (EVP_DigestInit_ex(ctx, md, NULL) != 1) {
        printf("Error initializing SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    // 更新摘要计算
    if (EVP_DigestUpdate(ctx, message, messageLen) != 1) {
        printf("Error updating SM3 digest.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    // 获取计算结果
    if (EVP_DigestFinal_ex(ctx, digest, &digestLen) != 1) {
        printf("Error getting SM3 digest result.\n");
        EVP_MD_CTX_free(ctx);
        return;
    }

    // 释放资源
    EVP_MD_CTX_free(ctx);
}

int main() {
    const unsigned char message[] = "Hello, SM3!";
    unsigned char digest[EVP_MAX_MD_SIZE];
    unsigned int digestLen;

    sm3_hash(message, sizeof(message) - 1, digest);

    printf("SM3 Digest: ");
    for (int i = 0; i < EVP_MD_size(EVP_sm3()); i++) {
        printf("%02x", digest[i]);
    }
    printf("\n");

    return 0;
}*/
