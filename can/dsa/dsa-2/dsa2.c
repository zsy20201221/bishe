#include <stdio.h>
#include <string.h>
#include <openssl/objects.h>
#include <openssl/dsa.h>

// 辅助函数：生成 DSA 参数和密钥
DSA* generateDSA() {
    DSA *d = DSA_new();
    if (!d) {
        fprintf(stderr, "Error creating DSA object\n");
        return NULL;
    }

    int ret = DSA_generate_parameters_ex(d, 1024, NULL, 0, NULL, NULL, NULL);
    if (ret != 1) {
        fprintf(stderr, "Error generating DSA parameters\n");
        DSA_free(d);
        return NULL;
    }

    ret = DSA_generate_key(d);
    if (ret != 1) {
        fprintf(stderr, "Error generating DSA key\n");
        DSA_free(d);
        return NULL;
    }

    return d;
}

// 辅助函数：选择摘要算法并设置对应的参数
int chooseDigestAlgorithm(int* datalen, int* nid) {
    int alg;
    printf("请选择摘要算法：\n");
    printf("1. NID_md5\n");
    printf("2. NID_sha\n");
    printf("3. NID_sha1\n");
    printf("4. NID_md5_sha1\n");

    int ret = scanf("%d", &alg);
    if (ret != 1 || alg < 1 || alg > 4) {
        printf("输入错误！\n");
        return -1;
    }

    if (alg == 1) {
        *datalen = 16;
        *nid = NID_md5;
    } else if (alg == 2) {
        *datalen = 20;
        *nid = NID_sha;
    } else if (alg == 3) {
        *datalen = 20;
        *nid = NID_sha1;
    } else if (alg == 4) {
        *datalen = 36; // 根据需要调整数据长度
        *nid = NID_md5_sha1;
    }

    return 0;
}

// 辅助函数：执行DSA测试
int performDSATest(DSA *d) {
    int datalen, nid;
    unsigned char data[100], signret[200];
    int signlen;

    for (int i = 0; i < 100; i++)
        data[i] = i + 1;

    if (chooseDigestAlgorithm(&datalen, &nid) != 0) {
        DSA_free(d);
        return -1;
    }

    int ret = DSA_sign(nid, data, datalen, signret, &signlen, d);
    if (ret != 1) {
        fprintf(stderr, "DSA_sign 错误！\n");
        DSA_free(d);
        return -1;
    }

    ret = DSA_verify(nid, data, datalen, signret, signlen, d);
    if (ret != 1) {
        fprintf(stderr, "DSA_verify 错误！\n");
        DSA_free(d);
        return -1;
    }

    DSA_free(d);
    printf("测试通过！\n");
    return 0;
}

/*int main() {
    DSA *d = generateDSA();
    if (!d) {
        return -1;
    }

    performDSATest(d);

    return 0;
}*/

/*#include <stdio.h>
#include <string.h>
#include <openssl/objects.h>
#include <openssl/dsa.h>

int main() {
    int ret;
    DSA *d;
    int i, bits = 1024, signlen, datalen, alg, nid;
    unsigned char data[100], signret[200];

    d = DSA_new();
    ret = DSA_generate_parameters_ex(d, 1024, NULL, 0, NULL, NULL, NULL);
    if (ret != 1) {
        DSA_free(d);
        return -1;
    }

    ret = DSA_generate_key(d);
    if (ret != 1) {
        DSA_free(d);
        return -1;
    }

    for (i = 0; i < 100; i++)
        data[i] = i + 1; // 直接赋值初始化输入数据

    printf("请选择摘要算法：\n");
    printf("1. NID_md5\n");
    printf("2. NID_sha\n");
    printf("3. NID_sha1\n");
    printf("4. NID_md5_sha1\n");

    ret = scanf("%d", &alg);
    if (ret != 1 || alg < 1 || alg > 4) {
        printf("输入错误！\n");
        DSA_free(d);
        return -1;
    }

    if (alg == 1) {
        datalen = 16;
        nid = NID_md5;
    } else if (alg == 2) {
        datalen = 20;
        nid = NID_sha;
    } else if (alg == 3) {
        datalen = 20;
        nid = NID_sha1;
    } else if (alg == 4) {
        datalen = 36; // 根据需要调整数据长度
        nid = NID_md5_sha1;
    }

    ret = DSA_sign(nid, data, datalen, signret, &signlen, d);
    if (ret != 1) {
        printf("DSA_sign 错误！\n");
        DSA_free(d);
        return -1;
    }

    ret = DSA_verify(nid, data, datalen, signret, signlen, d);
    if (ret != 1) {
        printf("DSA_verify 错误！\n");
        DSA_free(d);
        return -1;
    }

    DSA_free(d);
    printf("测试通过！\n");
    return 0;
}
*/
/*#include <string.h>
#include <openssl/objects.h>
#include <openssl/dsa.h>
int main()
{
int ret;
DSA *d;
int i,bits=1024,signlen,datalen,alg,nid;
unsigned char data[100],signret[200];
d=DSA_new();
ret=DSA_generate_parameters_ex(d,512,NULL,0,NULL,NULL,NULL);
if(ret!=1)
{
DSA_free(d);
return -1;
}
ret=DSA_generate_key(d);
if(ret!=1)
{
DSA_free(d);
return -1;
}
for(i=0;i<100;i++)
memset(&data[i],i+1,1);
printf("please select digest alg: \n");
printf("1.NID_md5\n");
printf("2.NID_sha\n");
printf("3.NID_sha1\n");
printf("4.NID_md5_sha1\n");
scanf("%d",&alg);
if(alg==1)
{
datalen=20;
nid=NID_md5;
}
else if(alg==2)
{
datalen=20;
nid=NID_sha;
}
else if(alg==3)
{
datalen=20;
nid=NID_sha1;
}
else if(alg==4)
{
datalen=20;
nid=NID_md5_sha1;
}
ret=DSA_sign(nid,data,datalen,signret,&signlen,d);
if(ret!=1)
{
printf("DSA_sign err!\n");
DSA_free(d);
return -1;
}
ret=DSA_verify(nid,data,datalen,signret,signlen,d);
if(ret!=1)
{
printf("DSA_verify err!\n");
DSA_free(d);
return -1;
}
DSA_free(d);
printf("test ok!\n");
return 0;
}
*/
