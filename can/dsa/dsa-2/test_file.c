#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <string.h>
#include <openssl/dsa.h>
#include <openssl/evp.h>


// 辅助函数：生成 DSA 对象
DSA* generateDSA() {
    DSA *d = DSA_new();
    if (!d) {
        fprintf(stderr, "Error creating DSA object\n");
        return NULL;
    }

    int ret = DSA_generate_parameters_ex(d, 1024, NULL, 0, NULL, NULL, NULL);
    if (ret != 1) {
        fprintf(stderr, "Error generating DSA parameters\n");
        DSA_free(d);
        return NULL;
    }

    ret = DSA_generate_key(d);
    if (ret != 1) {
        fprintf(stderr, "Error generating DSA key\n");
        DSA_free(d);
        return NULL;
    }

    return d;
}

// 辅助函数：选择摘要算法
int chooseDigestAlgorithm(int* datalen, int* nid) {
    int alg;
    printf("请选择摘要算法：\n");
    printf("1. NID_md5\n");
    printf("2. NID_sha\n");
    printf("3. NID_sha1\n");
    printf("4. NID_md5_sha1\n");

    int ret = scanf("%d", &alg);
    if (ret != 1 || alg < 1 || alg > 4) {
        printf("输入错误！\n");
        return -1;
    }

    if (alg == 1) {
        *datalen = 16;
        *nid = NID_md5;
    } else if (alg == 2) {
        *datalen = 20;
        *nid = NID_sha;
    } else if (alg == 3) {
        *datalen = 20;
        *nid = NID_sha1;
    } else if (alg == 4) {
        *datalen = 36; // 根据需要调整数据长度
        *nid = NID_md5_sha1;
    }

    return 0;
}

// 辅助函数：执行 DSA 测试
int performDSATest(DSA *d) {
    int datalen, nid;
    unsigned char data[100], signret[200];
    int signlen;

    for (int i = 0; i < 100; i++)
        data[i] = i + 1;

    if (chooseDigestAlgorithm(&datalen, &nid) != 0) {
        return -1;
    }

    int ret = DSA_sign(nid, data, datalen, signret, &signlen, d);
    if (ret != 1) {
        fprintf(stderr, "DSA_sign 错误！\n");
        return -1;
    }

    ret = DSA_verify(nid, data, datalen, signret, signlen, d);
    if (ret != 1) {
        fprintf(stderr, "DSA_verify 错误！\n");
        return -1;
    }

    printf("测试通过！\n");
    return 0;
}

// 测试生成 DSA 对象的函数
void testGenerateDSA() {
    DSA *d = generateDSA();
    CU_ASSERT_PTR_NOT_NULL(d);
    DSA_free(d);
}

// 测试选择摘要算法的函数
void testChooseDigestAlgorithm() {
    int datalen, nid;
    CU_ASSERT_EQUAL(chooseDigestAlgorithm(&datalen, &nid), 0);
    CU_ASSERT_TRUE(datalen > 0);
    CU_ASSERT_TRUE(nid > 0);
}

// 测试执行 DSA 测试的函数
void testPerformDSATest() {
    DSA *d = generateDSA();
    CU_ASSERT_PTR_NOT_NULL(d);

    // 在此处可以模拟输入数据和签名以测试 DSA 测试的不同方面
    CU_ASSERT_EQUAL(performDSATest(d), 0);

    DSA_free(d);
}

int main() {
    // 初始化 CUnit 测试注册表
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // 添加测试套件
    CU_pSuite suite = CU_add_suite("DSA_Test_Suite", NULL, NULL);
    if (!suite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 将测试用例添加到套件
    if (!CU_add_test(suite, "testGenerateDSA", testGenerateDSA) ||
        !CU_add_test(suite, "testChooseDigestAlgorithm", testChooseDigestAlgorithm) ||
        !CU_add_test(suite, "testPerformDSATest", testPerformDSATest)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 使用基本接口运行所有测试
    CU_basic_run_tests();

    // 清理并返回任何错误
    CU_cleanup_registry();
    return CU_get_error();
}

