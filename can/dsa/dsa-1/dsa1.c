#include <stdio.h>
#include <string.h>
#include <openssl/dsa.h>

void generateAndPrintDSA(int keyLength, unsigned long* h)
{
    DSA *d;
    int ret, i;
    unsigned char seed[20];

    d = DSA_new();

    for (i = 0; i < 20; i++)
        memset(seed + i, i, 1);

    ret = DSA_generate_parameters_ex(d, keyLength, NULL, 0, NULL, h, NULL);
    if (ret != 1)
    {
        fprintf(stderr, "Error generating parameters\n");
        DSA_free(d);
        return;
    }

    ret = DSA_generate_key(d);
    if (ret != 1)
    {
        fprintf(stderr, "Error generating key\n");
        DSA_free(d);
        return;
    }

    DSA_print_fp(stdout, d, 0);
    DSA_free(d);
}

/*int main()
{
    int keyLength = 512;
    unsigned long h = 0;
    generateAndPrintDSA(keyLength, &h);
    return 0;
}*/
/*#include <stdio.h>
#include <string.h>
#include <openssl/dsa.h>

int main()
{
    DSA *d;
    int ret, i;
    unsigned char seed[20];
    unsigned long h = 0;

    d = DSA_new();

    for (i = 0; i < 20; i++)
        memset(seed + i, i, 1);

    ret = DSA_generate_parameters_ex(d, 512, NULL, 0, NULL, &h, NULL);
    if (ret != 1)
    {
        fprintf(stderr, "Error generating parameters\n");
        DSA_free(d);
        return -1;
    }

    ret = DSA_generate_key(d);
    if (ret != 1)
    {
        fprintf(stderr, "Error generating key\n");
        DSA_free(d);
        return -1;
    }

    DSA_print_fp(stdout, d, 0);
    DSA_free(d);
    return 0;
}*/
/*#include <string.h>
#include <openssl/dsa.h>
int main()
{
DSA *d;
int ret,i;
unsigned char seed[20];
int counter=2;
unsigned long h;
d=DSA_new();
for(i=0;i<20;i++)
memset(seed+i,i,1);
//ret=DSA_generate_parameters_ex(d, 512,seed, 20, &counter,&h,NULL);
// 生成密钥参数 
ret=DSA_generate_parameters_ex(d, 512,NULL,0,NULL,NULL,NULL);
if(ret!=1)
{
DSA_free(d);
return -1;
}
// 生成密钥 
ret=DSA_generate_key(d);
if(ret!=1)
{
DSA_free(d);
return -1;
}
DSA_print_fp(stdout,d,0);
DSA_free(d);
return 0;
}
*/
