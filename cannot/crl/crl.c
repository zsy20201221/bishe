#include <stdio.h>
#include <stdlib.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <time.h>

int generateCRL(const char* crlFilePath) {
  int ret;
  unsigned long e = RSA_3;
  time_t t;
  X509_NAME *issuer;
  ASN1_TIME *lastUpdate, *nextUpdate, *rvTime;
  X509_CRL *crl = NULL;
  X509_REVOKED *revoked;
  EVP_PKEY *pkey;
  ASN1_INTEGER *serial;
  RSA *r;
  BIGNUM *bne;
  BIO *bp;

  /* 生成密钥 */
  bne = BN_new();
  ret = BN_set_word(bne, e);
  r = RSA_new();
  ret = RSA_generate_key_ex(r, 1024, bne, NULL);
  if (ret != 1) {
    printf("RSA_generate_key_ex err!\n");
    return -1;
  }

  pkey = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(pkey, r);

  crl = X509_CRL_new();
  /* 设置版本 */
  ret = X509_CRL_set_version(crl, 2); // 修正版本为2

  /* 设置颁发者 */
  issuer = X509_NAME_new();
  ret = X509_NAME_add_entry_by_NID(issuer, NID_commonName, MBSTRING_ASC, (unsigned char*)"CRL issuer", -1, -1, 0);
  ret = X509_CRL_set_issuer_name(crl, issuer);

  /* 设置上次发布时间 */
  lastUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(lastUpdate, t);
  ret = X509_CRL_set_lastUpdate(crl, lastUpdate);

  /* 设置下次发布时间 */
  nextUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(nextUpdate, t + 1000);
  ret = X509_CRL_set_nextUpdate(crl, nextUpdate);

  /* 添加被撤销证书序列号 */
  revoked = X509_REVOKED_new();
  serial = ASN1_INTEGER_new();
  ret = ASN1_INTEGER_set(serial, 1000);
  ret = X509_REVOKED_set_serialNumber(revoked, serial);
  rvTime = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(rvTime, t + 2000);
  ret = X509_REVOKED_set_revocationDate(revoked, rvTime);
  ret = X509_CRL_add0_revoked(crl, revoked);

  /* 排序 */
  ret = X509_CRL_sort(crl);

  /* 签名 */
  ret = X509_CRL_sign(crl, pkey, EVP_sha256()); // 使用更安全的哈希算法，如 SHA-256

  /* 写入文件 */
  bp = BIO_new_file(crlFilePath, "wb");
  ret = i2d_X509_CRL_bio(bp, crl);
  BIO_free(bp);

  X509_CRL_free(crl);
  EVP_PKEY_free(pkey);
  BN_free(bne);

  printf("CRL 文件生成成功！\n");

  return 0;
}

/*int main() {
  const char* crlFilePath = "crl.crl";
  int result = generateCRL(crlFilePath);

  if (result != 0) {
    printf("生成 CRL 文件失败！\n");
  }

  return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <time.h>

int main() {
  int ret;
  unsigned long e = RSA_3;
  time_t t;
  X509_NAME *issuer;
  ASN1_TIME *lastUpdate, *nextUpdate, *rvTime;
  X509_CRL *crl = NULL;
  X509_REVOKED *revoked;
  EVP_PKEY *pkey;
  ASN1_INTEGER *serial;
  RSA *r;
  BIGNUM *bne;
  BIO *bp;

  // 生成密钥 
  bne = BN_new();
  ret = BN_set_word(bne, e);
  r = RSA_new();
  ret = RSA_generate_key_ex(r, 1024, bne, NULL);
  if (ret != 1) {
    printf("RSA_generate_key_ex err!\n");
    return -1;
  }

  pkey = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(pkey, r);

  crl = X509_CRL_new();
  // 设置版本 
  ret = X509_CRL_set_version(crl, 2); // 修正版本为2

  // 设置颁发者 
  issuer = X509_NAME_new();
  ret = X509_NAME_add_entry_by_NID(issuer, NID_commonName, MBSTRING_ASC, (unsigned char*)"CRL issuer", -1, -1, 0);
  ret = X509_CRL_set_issuer_name(crl, issuer);

  // 设置上次发布时间 
  lastUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(lastUpdate, t);
  ret = X509_CRL_set_lastUpdate(crl, lastUpdate);

  // 设置下次发布时间 
  nextUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(nextUpdate, t + 1000);
  ret = X509_CRL_set_nextUpdate(crl, nextUpdate);

  // 添加被撤销证书序列号 
  revoked = X509_REVOKED_new();
  serial = ASN1_INTEGER_new();
  ret = ASN1_INTEGER_set(serial, 1000);
  ret = X509_REVOKED_set_serialNumber(revoked, serial);
  rvTime = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(rvTime, t + 2000);
  ret = X509_REVOKED_set_revocationDate(revoked, rvTime);
  ret = X509_CRL_add0_revoked(crl, revoked);

  //排序 
  ret = X509_CRL_sort(crl);

  // 签名 
  ret = X509_CRL_sign(crl, pkey, EVP_sha256()); // 使用更安全的哈希算法，如 SHA-256

  // 写入文件 
  bp = BIO_new_file("crl.crl", "wb");
  ret = i2d_X509_CRL_bio(bp, crl);
  BIO_free(bp);

  X509_CRL_free(crl);
  EVP_PKEY_free(pkey);
  BN_free(bne);

  printf("CRL 文件生成成功！\n");

  return 0;
}*/
/*#pragma warning(disable : 4996)
#define _CRT_SECURE_NO_WARNINGS
#include <openssl/x509.h>
int main() {
  int ret, len;
  unsigned char *buf, *p;
  unsigned long e = RSA_3;
  FILE *fp;
  time_t t;
  X509_NAME *issuer;
  ASN1_TIME *lastUpdate, *nextUpdate, *rvTime;
  X509_CRL *crl = NULL;
  X509_REVOKED *revoked;
  EVP_PKEY *pkey;
  ASN1_INTEGER *serial;
  RSA *r;
  BIGNUM *bne;
  BIO *bp;
  // 生成密钥
  bne = BN_new();
  ret = BN_set_word(bne, e);
  r = RSA_new();
  ret = RSA_generate_key_ex(r, 1024, bne, NULL);
  if (ret != 1) {
    printf("RSA_generate_key_ex err!\n");
    return -1;
  }
  pkey = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(pkey, r);
  crl = X509_CRL_new();
  // 设置版本
  ret = X509_CRL_set_version(crl, 3);
  // 设置颁发者
  issuer = X509_NAME_new();
  //ret = X509_NAME_add_entry_by_NID(issuer, NID_commonName, V_ASN1_PRINTAB LESTRING, "CRL issuer", 10, -1, 0);原错误代码
  ret = X509_NAME_add_entry_by_NID(issuer, NID_commonName, MBSTRING_ASC, "CRL issuer", 10, -1, 0);//更新后代码
  ret = X509_CRL_set_issuer_name(crl, issuer);
  // 设置上次发布时间
  lastUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(lastUpdate, t);
  ret = X509_CRL_set_lastUpdate(crl, lastUpdate);
  // 设置下次发布时间
  nextUpdate = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(nextUpdate, t + 1000);
  ret = X509_CRL_set_nextUpdate(crl, nextUpdate);
  // 添加被撤销证书序列号
  revoked = X509_REVOKED_new();
  serial = ASN1_INTEGER_new();
  ret = ASN1_INTEGER_set(serial, 1000);
  ret = X509_REVOKED_set_serialNumber(revoked, serial);
  rvTime = ASN1_TIME_new();
  t = time(NULL);
  ASN1_TIME_set(rvTime, t + 2000);
  ret = X509_CRL_set_nextUpdate(crl, rvTime);
  ret = X509_REVOKED_set_revocationDate(revoked, rvTime);
  ret = X509_CRL_add0_revoked(crl, revoked);
  // 排序
  ret = X509_CRL_sort(crl);
  // 签名
  ret = X509_CRL_sign(crl, pkey, EVP_md5());
  // 写入文件
  bp = BIO_new(BIO_s_file());
  BIO_set_fp(bp, stdout, BIO_NOCLOSE);
  X509_CRL_print(bp, crl);
  len = i2d_X509_CRL(crl, NULL);
  buf = malloc(len + 10);
  p = buf;
  len = i2d_X509_CRL(crl, &p);
  fp = fopen("crl.crl", "wb");
  fwrite(buf, 1, len, fp);
  fclose(fp);
  BIO_free(bp);
  X509_CRL_free(crl);
  free(buf);
  getchar();
  return 0;
}
*/
