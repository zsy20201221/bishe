#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

// 测试套件初始化函数
int init_suite(void) {
    // 在这里可以进行一些初始化操作
    return 0;
}

// 测试套件清理函数
int clean_suite(void) {
    // 在这里可以进行一些清理操作
    return 0;
}

// 测试用例1：检查生成的私钥是否有效
void testGenerateKeysAndTest_privateKeyValidity(void) {
    RSA *privateKey;
    BIO *in = BIO_new_file("pri.pem", "rb");
    privateKey = PEM_read_bio_RSAPrivateKey(in, NULL, NULL, NULL);
    CU_ASSERT_PTR_NOT_NULL_FATAL(privateKey);
    CU_ASSERT_EQUAL(RSA_check_key(privateKey), 1);
    RSA_free(privateKey);
    BIO_free(in);
}

// 测试用例2：检查生成的公钥是否有效
void testGenerateKeysAndTest_publicKeyValidity(void) {
    RSA *publicKey;
    BIO *in = BIO_new_file("pub.pem", "rb");
    publicKey = PEM_read_bio_RSAPublicKey(in, NULL, NULL, NULL);
    CU_ASSERT_PTR_NOT_NULL_FATAL(publicKey);
    RSA_free(publicKey);
    BIO_free(in);
}

// 将测试用例添加到测试套件
void addTests() {
    CU_pSuite suite = CU_add_suite("generateKeysAndTest Suite", init_suite, clean_suite);
    CU_add_test(suite, "Test Private Key Validity", testGenerateKeysAndTest_privateKeyValidity);
    CU_add_test(suite, "Test Public Key Validity", testGenerateKeysAndTest_publicKeyValidity);
}

int main() {
    // 初始化CUnit测试框架
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // 添加测试用例到测试套件
    addTests();

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理CUnit测试框架资源
    CU_cleanup_registry();

    return CU_get_error();
}
