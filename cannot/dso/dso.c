#include <stdio.h>
#include <dlfcn.h>

// Function to demonstrate obtaining and calling function pointers
void demonstrateDynamicLoading() {
    // 打开动态库
    void* libHandle = dlopen("/usr/local/lib/libcrypto.so", RTLD_NOW);
    if (!libHandle) {
        fprintf(stderr, "无法打开动态库: %s\n", dlerror());
        return;
    }

    // 获取函数地址
    void (*BIO_free_ptr)(void*) = dlsym(libHandle, "BIO_free");
    if (!BIO_free_ptr) {
        fprintf(stderr, "无法获取函数地址: %s\n", dlerror());
        dlclose(libHandle);
        return;
    }

    void* (*BIO_new_ptr)(void*) = dlsym(libHandle, "BIO_new");
    if (!BIO_new_ptr) {
        fprintf(stderr, "无法获取函数地址: %s\n", dlerror());
        dlclose(libHandle);
        return;
    }

    // 这里只是演示获取函数指针，不实际调用 BIO_new
    // 如果需要使用 BIO，还需要其他的初始化和配置步骤

    // 调用 BIO_free
    BIO_free_ptr(NULL);

    // 输出结果
    printf("函数指针获取和调用演示完成\n");

    // 关闭动态库
    dlclose(libHandle);
}

/*int main() {
    // 调用新的函数
    demonstrateDynamicLoading();

    return 0;
}
*/
/*#include <stdio.h>
#include <dlfcn.h>

int main() {
    // 打开动态库
    void* libHandle = dlopen("/usr/local/lib/libcrypto.so", RTLD_NOW);
    if (!libHandle) {
        fprintf(stderr, "无法打开动态库: %s\n", dlerror());
        return -1;
    }

    // 获取函数地址
    void (*BIO_free_ptr)(void*) = dlsym(libHandle, "BIO_free");
    if (!BIO_free_ptr) {
        fprintf(stderr, "无法获取函数地址: %s\n", dlerror());
        dlclose(libHandle);
        return -1;
    }

    void* (*BIO_new_ptr)(void*) = dlsym(libHandle, "BIO_new");
    if (!BIO_new_ptr) {
        fprintf(stderr, "无法获取函数地址: %s\n", dlerror());
        dlclose(libHandle);
        return -1;
    }

    // 这里只是演示获取函数指针，不实际调用 BIO_new
    // 如果需要使用 BIO，还需要其他的初始化和配置步骤

    // 调用 BIO_free
    BIO_free_ptr(NULL);

    // 输出结果
    printf("函数指针获取和调用演示完成\n");

    // 关闭动态库
    dlclose(libHandle);

    return 0;
}
*/

/*
#include <openssl/bio.h>
#include <openssl/dso.h>
int main() {
  DSO *d;
  void (*f1)();
  void (*f2)();
  BIO *(*BIO_newx)(BIO_METHOD *a);
  BIO *(*BIO_freex)(BIO_METHOD *a);
  BIO *test;
  d = DSO_new();
  d = DSO_load(d, "libeay32", NULL, 0);
  f1 = DSO_bind_func(d, "BIO_new");
  f2 = DSO_bind_var(d, "BIO_free");
  BIO_newx = (BIO * (*)(BIO_METHOD *)) f1;
  BIO_freex = (BIO * (*)(BIO_METHOD *)) f2;
  test = BIO_newx(BIO_s_file());
  BIO_set_fp(test, stdout, BIO_NOCLOSE);
  BIO_puts(test, "abd\n\n");
  BIO_freex(test);
  DSO_free(d);
  return 0;
}*/
