#include <CUnit/Basic.h>
#include <dlfcn.h>

void test_demonstrateDynamicLoading()
{
    // 打开动态库
    void* libHandle = dlopen("/usr/local/lib/libcrypto.so", RTLD_NOW);
    CU_ASSERT_PTR_NOT_NULL_FATAL(libHandle);

    // 获取函数地址
    void (*BIO_free_ptr)(void*) = dlsym(libHandle, "BIO_free");
    CU_ASSERT_PTR_NOT_NULL_FATAL(BIO_free_ptr);

    void* (*BIO_new_ptr)(void*) = dlsym(libHandle, "BIO_new");
    CU_ASSERT_PTR_NOT_NULL_FATAL(BIO_new_ptr);

    // 这里只是演示获取函数指针，不实际调用 BIO_new
    // 如果需要使用 BIO，还需要其他的初始化和配置步骤

    // 调用 BIO_free
    BIO_free_ptr(NULL);

    // 关闭动态库
    dlclose(libHandle);

    // 断言测试通过
    CU_PASS("demonstrateDynamicLoading test passed");
}

void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("DynamicLoadingTests", NULL, NULL);
    CU_add_test(suite, "test_demonstrateDynamicLoading", test_demonstrateDynamicLoading);
}

int main()
{
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    add_test_suite();

    CU_basic_run_tests();

    CU_cleanup_registry();

    return CU_get_error();
}
