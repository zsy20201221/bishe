#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>



void testGenerateX509Request(void) {
    // 定义测试用例中的输入和期望输出
    const char* commonName = "openssl";
    const char* organizationName = "zsy";
    const char* organizationalUnitName = "besti";
    const char* outputPemFile = "certreq.pem";
    const char* outputDerFile = "certreq.der";

    // 调用被测试的函数
    int result = generateX509Request(commonName, organizationName, organizationalUnitName, outputPemFile, outputDerFile);

    // 验证结果是否符合预期
    CU_ASSERT_EQUAL(result, 0);
}

int main() {
    // 初始化 CUnit 测试框架
    CU_initialize_registry();

    // 创建测试套件
    CU_pSuite suite = CU_add_suite("GenerateX509RequestSuite", NULL, NULL);

    // 将测试用例添加到测试套件中
    CU_add_test(suite, "testGenerateX509Request", testGenerateX509Request);

    // 设置 CUnit 基本模式并运行测试套件
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // 清理 CUnit 测试框架资源
    CU_cleanup_registry();

    return 0;
}
