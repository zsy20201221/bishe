#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/sha.h>
#include <string.h>

void handleErrors(void) {
    ERR_print_errors_fp(stderr);
    abort();
}

int generateX509Request(const char* commonName, const char* organizationName, const char* organizationalUnitName,
                        const char* outputPemFile, const char* outputDerFile) {
    X509_REQ *req;
    int ret;
    long version;
    X509_NAME *name;
    EVP_PKEY *pkey;
    RSA *rsa;
    unsigned char *der, *p;
    FILE *fp;
    BIO *b;

    req = X509_REQ_new();
    if (req == NULL) {
        printf("Error creating X509 request.\n");
        return -1;
    }

    version = 1;
    ret = X509_REQ_set_version(req, version);
    if (ret != 1) {
        printf("Error setting X509 request version.\n");
        X509_REQ_free(req);
        return -1;
    }

    name = X509_NAME_new();
    if (name == NULL) {
        printf("Error creating X509 name.\n");
        X509_REQ_free(req);
        return -1;
    }

    ret = X509_NAME_add_entry_by_txt(name, "commonName", MBSTRING_ASC, (const unsigned char *)commonName, -1, -1, 0);
    if (ret != 1) {
        printf("Error adding commonName entry.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_set_subject_name(req, name);
    if (ret != 1) {
        printf("Error setting subject name.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    pkey = EVP_PKEY_new();
    if (pkey == NULL) {
        printf("Error creating EVP_PKEY.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    rsa = RSA_generate_key(2048, RSA_F4, NULL, NULL);
    if (rsa == NULL) {
        printf("Error generating RSA key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    EVP_PKEY_assign_RSA(pkey, rsa);
    ret = X509_REQ_set_pubkey(req, pkey);
    if (ret != 1) {
        printf("Error setting public key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_add1_attr_by_txt(req, "organizationName", MBSTRING_UTF8, (const unsigned char *)organizationName, -1);

    if (ret != 1) {
        printf("Error adding organizationName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_add1_attr_by_txt(req, "organizationalUnitName", MBSTRING_UTF8, (const unsigned char *)organizationalUnitName, -1);

    if (ret != 1) {
        printf("Error adding organizationalUnitName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_sign(req, pkey, EVP_sha256());
    if (ret <= 0) {
        printf("Error signing X509 request.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    b = BIO_new_file(outputPemFile, "w");
    if (b == NULL) {
        printf("Error creating output file.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    PEM_write_bio_X509_REQ(b, req);
    BIO_free(b);

    int len = i2d_X509_REQ(req, NULL);
    der = malloc(len);
    p = der;
    len = i2d_X509_REQ(req, &p);

    OpenSSL_add_all_algorithms();
    ret = X509_REQ_verify(req, pkey);
    if (ret < 0) {
        printf("Error verifying X509 request.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        free(der);
        return -1;
    }

    fp = fopen(outputDerFile, "wb");
    if (fp == NULL) {
        printf("Error creating DER file.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        free(der);
        return -1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    free(der);
    EVP_PKEY_free(pkey);
    X509_REQ_free(req);
    X509_NAME_free(name);

    return 0;
}

/*int main() {
    const char* commonName = "openssl";
    const char* organizationName = "zsy";
    const char* organizationalUnitName = "besti";
    const char* outputPemFile = "certreq.pem";
    const char* outputDerFile = "certreq.der";

    int result = generateX509Request(commonName, organizationName, organizationalUnitName, outputPemFile, outputDerFile);

    if (result == 0) {
        printf("X509 request generation completed successfully.\n");
    } else {
        printf("Error generating X509 request.\n");
    }

    return 0;
}*/
/*#include <stdio.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/sha.h>
#include <string.h>

void handleErrors(void) {
    ERR_print_errors_fp(stderr);
    abort();
}

int main() {
    X509_REQ *req;
    int ret;
    long version;
    X509_NAME *name;
    EVP_PKEY *pkey;
    RSA *rsa;
    X509_NAME_ENTRY *entry = NULL;
    char mdout[EVP_MAX_MD_SIZE];
    unsigned int mdlen;
    int bits = 2048;
    unsigned long e = RSA_F4;
    unsigned char *der, *p;
    FILE *fp;
    BIO *b;

    req = X509_REQ_new();
    if (req == NULL) {
        printf("Error creating X509 request.\n");
        return -1;
    }

    version = 1;
    ret = X509_REQ_set_version(req, version);
    if (ret != 1) {
        printf("Error setting X509 request version.\n");
        X509_REQ_free(req);
        return -1;
    }

    name = X509_NAME_new();
    if (name == NULL) {
        printf("Error creating X509 name.\n");
        X509_REQ_free(req);
        return -1;
    }

    ret = X509_NAME_add_entry_by_txt(name, "commonName", MBSTRING_ASC, (const unsigned char *)"openssl", -1, -1, 0);
    if (ret != 1) {
        printf("Error adding commonName entry.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_set_subject_name(req, name);
    if (ret != 1) {
        printf("Error setting subject name.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    pkey = EVP_PKEY_new();
    if (pkey == NULL) {
        printf("Error creating EVP_PKEY.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    rsa = RSA_generate_key(bits, e, NULL, NULL);
    if (rsa == NULL) {
        printf("Error generating RSA key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    EVP_PKEY_assign_RSA(pkey, rsa);
    ret = X509_REQ_set_pubkey(req, pkey);
    if (ret != 1) {
        printf("Error setting public key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_add1_attr_by_txt(req, "organizationName", MBSTRING_UTF8, (const unsigned char *)"zsy", -1);

    if (ret != 1) {
        printf("Error adding organizationName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_add1_attr_by_txt(req, "organizationalUnitName", MBSTRING_UTF8, (const unsigned char *)"besti", -1);

    if (ret != 1) {
        printf("Error adding organizationalUnitName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    ret = X509_REQ_sign(req, pkey, EVP_sha256());
    if (ret <= 0) {
        printf("Error signing X509 request.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    b = BIO_new_file("certreq.pem", "w");
    if (b == NULL) {
        printf("Error creating output file.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }

    PEM_write_bio_X509_REQ(b, req);
    BIO_free(b);

    int len = i2d_X509_REQ(req, NULL);
    der = malloc(len);
    p = der;
    len = i2d_X509_REQ(req, &p);

    OpenSSL_add_all_algorithms();
    ret = X509_REQ_verify(req, pkey);
    if (ret < 0) {
        printf("Error verifying X509 request.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        free(der);
        return -1;
    }

    fp = fopen("certreq.der", "wb");
    if (fp == NULL) {
        printf("Error creating DER file.\n");
        handleErrors();
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        free(der);
        return -1;
    }

    fwrite(der, 1, len, fp);
    fclose(fp);

    free(der);
    EVP_PKEY_free(pkey);
    X509_REQ_free(req);
    X509_NAME_free(name);

    return 0;
}
*/


/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/err.h>

int main() {
    X509_REQ *req;
    int ret;
    long version;
    X509_NAME *name;
    EVP_PKEY *pkey;
    RSA *rsa;
    X509_NAME_ENTRY *entry = NULL;
    char mdout[EVP_MAX_MD_SIZE];
    unsigned int mdlen;
    int bits = 2048;
    unsigned long e = RSA_F4;
    unsigned char *der, *p;
    FILE *fp;
    const EVP_MD *md;
    BIO *b;
    
    req = X509_REQ_new();
    if (req == NULL) {
        printf("Error creating X509 request.\n");
        return -1;
    }
    
    version = 1;
    ret = X509_REQ_set_version(req, version);
    if (ret != 1) {
        printf("Error setting X509 request version.\n");
        X509_REQ_free(req);
        return -1;
    }
    
    name = X509_NAME_new();
    if (name == NULL) {
        printf("Error creating X509 name.\n");
        X509_REQ_free(req);
        return -1;
    }
    
    ret = X509_NAME_add_entry_by_txt(name, "commonName", V_ASN1_PRINTABLESTRING, (const unsigned char *)"openssl", -1, -1, 0);
    if (ret != 1) {
        printf("Error adding commonName entry.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    ret = X509_REQ_set_subject_name(req, name);
    if (ret != 1) {
        printf("Error setting subject name.\n");
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    pkey = EVP_PKEY_new();
    if (pkey == NULL) {
        printf("Error creating EVP_PKEY.\n");
       X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    rsa = RSA_generate_key(bits, e, NULL, NULL);
    if (rsa == NULL) {
        printf("Error generating RSA key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    EVP_PKEY_assign_RSA(pkey, rsa);
    ret = X509_REQ_set_pubkey(req, pkey);
    if (ret != 1) {
        printf("Error setting public key.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    ret = X509_REQ_add1_attr_by_txt(req, "organizationName", V_ASN1_UTF8STRING, (const unsigned char *)"test", 4);
    if (ret != 1) {
        printf("Error adding organizationName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    ret = X509_REQ_add1_attr_by_txt(req, "organizationalUnitName", V_ASN1_UTF8STRING, (const unsigned char *)"ttt", 3);
    if (ret != 1) {
        printf("Error adding organizationalUnitName attribute.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    md = EVP_sha1();
ret = X509_REQ_digest(req, md, mdout, &mdlen);
if (ret != 1) {
    printf("Error digesting X509 request.\n");
    EVP_PKEY_free(pkey);
    X509_REQ_free(req);
    X509_NAME_free(name);
    return -1;
}

ret = X509_REQ_sign(req, pkey, md);

    if (ret != 1) {
        printf("Error signing X509 request.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    b = BIO_new_file("certreq.pem", "w");
    if (b == NULL) {
        printf("Error creating output file.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        return -1;
    }
    
    PEM_write_bio_X509_REQ(b, req);
    BIO_free(b);
    
    int len = i2d_X509_REQ(req, NULL);
    der = malloc(len);
    p = der;
    len = i2d_X509_REQ(req, &p);
    
    OpenSSL_add_all_algorithms();
    ret = X509_REQ_verify(req, pkey);
if (ret < 0) {
    printf("Error verifying X509 request.\n");
    EVP_PKEY_free(pkey);
    X509_REQ_free(req);
    X509_NAME_free(name);
    free(der);
    return -1;
}
    
    fp = fopen("certreq.der", "wb");
    if (fp == NULL) {
        printf("Error creating DER file.\n");
        EVP_PKEY_free(pkey);
        X509_REQ_free(req);
        X509_NAME_free(name);
        free(der);
        return -1;
    }
    
    fwrite(der, 1, len, fp);
    fclose(fp);
    
    free(der);
    EVP_PKEY_free(pkey);
    X509_REQ_free(req);
    X509_NAME_free(name);
    
    return 0;
}*/
/*#include <string.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
int main()
{
X509_REQ *req;
int ret;
long version;
X509_NAME *name;
EVP_PKEY *pkey;
RSA *rsa;
X509_NAME_ENTRY *entry=NULL;
char bytes[100],mdout[20];
int len,mdlen;
int bits=512;
unsigned long e=RSA_3;
unsigned char *der,*p;
FILE *fp;
const EVP_MD *md;
X509 *x509;
BIO *b;
STACK_OF(X509_EXTENSION) *exts;
req=X509_REQ_new();
version=1;
ret=X509_REQ_set_version(req,version);
name=X509_NAME_new();
strcpy(bytes,"openssl");
len=strlen(bytes);
entry=X509_NAME_ENTRY_create_by_txt(&entry,"commonName",V_ASN1_UTF8STRI
NG,(unsigned char *)bytes,len);
X509_NAME_add_entry(name,entry,0,-1);
strcpy(bytes,"bj");
len=strlen(bytes);
entry=X509_NAME_ENTRY_create_by_txt(&entry,"countryName",V_ASN1_UTF8STRIN
G,bytes,len);
X509_NAME_add_entry(name,entry,1,-1);
// subject name 
ret=X509_REQ_set_subject_name(req,name);
// pub key 
pkey=EVP_PKEY_new();
rsa=RSA_generate_key(bits,e,NULL,NULL);
EVP_PKEY_assign_RSA(pkey,rsa);
ret=X509_REQ_set_pubkey(req,pkey);
// attribute 
strcpy(bytes,"test");
len=strlen(bytes);
ret=X509_REQ_add1_attr_by_txt(req,"organizationName",V_ASN1_UTF8STRING,bytes,le
n);
strcpy(bytes,"ttt");
len=strlen(bytes);
ret=X509_REQ_add1_attr_by_txt(req,"organizationalUnitName",V_ASN1_UTF8STRING,b
ytes,len);
md=EVP_sha1();
ret=X509_REQ_digest(req,md,mdout,&mdlen);
ret=X509_REQ_sign(req,pkey,md);
if(!ret)
{
printf("sign err!\n");
X509_REQ_free(req);
return -1;
}
// 写入文件 PEM 格式 
b=BIO_new_file("certreq.txt","w");
PEM_write_bio_X509_REQ(b,req,NULL,NULL);
BIO_free(b);
// DER 编码 
len=i2d_X509_REQ(req,NULL);
der=malloc(len);
p=der;
len=i2d_X509_REQ(req,&p);
OpenSSL_add_all_algorithms();
ret=X509_REQ_verify(req,pkey);
if(ret<0)
{
printf("verify err.\n");
}
fp=fopen("certreq2.txt","wb");
fwrite(der,1,len,fp);
fclose(fp);
free(der);
X509_REQ_free(req);
return 0;
}*/
