#include <openssl/crypto.h>
#include <openssl/bio.h>

void performMemoryOperations() {
    char *p;
    BIO *b;

    // 启用内存调试
   int CRYPTO_malloc_init();
   int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);

    p = OPENSSL_malloc(4);

    // 禁用内存调试
   int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_OFF);

    // Your code here

    b = BIO_new_file("leak.log", "w");

    // 启用内存调试
   int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);

    // 输出内存泄漏信息
   int CRYPTO_mem_leaks(b);

    // Your code here

    // 释放分配的内存
    OPENSSL_free(p);
    BIO_free(b);
}

/*int main() {
    performMemoryOperations();
    return 0;
}
*/
/*#include <openssl/crypto.h>
#include <openssl/bio.h>

int main() {
    char *p;
    BIO *b;

    // 启用内存调试
 int   CRYPTO_malloc_init();
 int  CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);

    p = OPENSSL_malloc(4);

    // 禁用内存调试
 int   CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_OFF);

    // Your code here

    b = BIO_new_file("leak.log", "w");

    // 启用内存调试
 int    CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);

    // 输出内存泄漏信息
 int   CRYPTO_mem_leaks(b);

    // Your code here

    // 释放分配的内存
    OPENSSL_free(p);
    BIO_free(b);

    return 0;
}

*/

/*#include <openssl/crypto.h>
#include <openssl/bio.h>
int main()
{
char *p;
BIO *b;
int flag=0;
int CRYPTO_malloc_debug_init();
int CRYPTO_set_mem_debug(flag);
int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);
p=OPENSSL_malloc(4);
int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_OFF);
b=BIO_new_file("leak.log","w");
int CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);
int CRYPTO_mem_leaks(b);
OPENSSL_free(p);
BIO_free(b);
return 0;
}
*/
