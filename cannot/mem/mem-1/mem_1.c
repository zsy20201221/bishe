#include <openssl/crypto.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 新的函数，用于分配内存、操作数据并打印结果
void allocateAndPrintData() {
    // 使用 OPENSSL_malloc 分配内存
    char *data = OPENSSL_malloc(50);
    
    if (data == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        return;
    }

    // 在分配的内存中进行一些操作
    strcpy(data, "Hello, OpenSSL!");

    // 打印结果
    printf("Data: %s\n", data);

    // 使用 OPENSSL_free 释放分配的内存
    OPENSSL_free(data);
}

// 主函数
/*int main() {
    // 调用新的函数进行内存分配、操作和打印
    allocateAndPrintData();

    return 0;
}
*/
/*#include <openssl/crypto.h> // 包含 OpenSSL 头文件

int main() {
    // 使用 OPENSSL_malloc 分配内存
    char *data = OPENSSL_malloc(50);
    
    if (data == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        return 1;
    }

    // 在分配的内存中进行一些操作
    strcpy(data, "Hello, OpenSSL!");

    // 打印结果
    printf("Data: %s\n", data);

    // 使用 OPENSSL_free 释放分配的内存
    OPENSSL_free(data);

    return 0;
}
*/
/*#include <string.h>
#include <openssl/crypto.h>
int    main()
{    
	char *p;
	int  i;
	p=OPENSSL_malloc(4);
	//p=OPENSSL_remalloc(p,40);
	p=OPENSSL_realloc(p,32);
	for(i=0;i<32;i++)
		memset(&p[i],i,1);
	// realloc时将以前的内存区清除(置乱) 
	//p=OPENSSL_realloc_clean(p,32,77);
	//p=OPENSSL_remalloc(p,40);
	//OPENSSL_malloc_locked(3);
	OPENSSL_free(p);
	return 0;
}
*/
/*#include <string.h>
#include <openssl/crypto.h>
int main()
{
char *p;
int i;
p=OPENSSL_malloc(4);
p=OPENSSL_remalloc(p,40);
p=OPENSSL_realloc(p,32);
for(i=0;i<32;i++)
memset(&p[i],i,1);
// realloc 时将以前的内存区清除(置乱) 
p=OPENSSL_realloc_clean(p,32,77);
p=OPENSSL_remalloc(p,40);
OPENSSL_malloc_locked(3);
OPENSSL_free(p);
return 0;
}
*/
