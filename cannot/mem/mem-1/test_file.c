#include <CUnit/Basic.h>
#include <openssl/crypto.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 声明待测试的函数
void allocateAndPrintData();

// 初始化测试套件
int init_suite(void) {
    return 0;
}

// 清理测试套件
int clean_suite(void) {
    return 0;
}

// 测试函数
void test_allocateAndPrintData(void) {
    // 保存原 stdout
    FILE *original_stdout = stdout;
    
    // 为测试目的创建临时文件
    FILE *temp_stdout = fopen("temp_stdout.txt", "w");
    if (temp_stdout == NULL) {
        CU_FAIL("Failed to create temp_stdout.txt");
        return;
    }

    // 重定向 stdout 到临时文件
    stdout = temp_stdout;

    // 调用待测试的函数
    allocateAndPrintData();

    // 恢复原 stdout
    fclose(temp_stdout);
    stdout = original_stdout;

    // 读取临时文件内容
    FILE *temp_stdout_read = fopen("temp_stdout.txt", "r");
    if (temp_stdout_read == NULL) {
        CU_FAIL("Failed to read temp_stdout.txt");
        return;
    }

    char buffer[100];
    fgets(buffer, sizeof(buffer), temp_stdout_read);

    // 关闭临时文件
    fclose(temp_stdout_read);

    // 断言测试结果是否符合预期
    CU_ASSERT_STRING_EQUAL(buffer, "Data: Hello, OpenSSL!\n");
}

int main() {
    // 初始化 CUnit 测试框架
    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    // 添加测试套件到注册表
    CU_pSuite suite = CU_add_suite("SuiteName", init_suite, clean_suite);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 添加测试函数到测试套件
    if (CU_add_test(suite, "test_allocateAndPrintData", test_allocateAndPrintData) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // 运行所有测试
    CU_basic_run_tests();

    // 完成后清理测试框架资源
    CU_cleanup_registry();

    return CU_get_error();
}

