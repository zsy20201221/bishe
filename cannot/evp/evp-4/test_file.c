#include <CUnit/Basic.h>
#include <openssl/rand.h>


void testEncryptAndDecrypt() {
    char input[] = "openssl 编程";
    char output[500];
    size_t out_len;

    // Replace with your actual key generation mechanism if needed
    unsigned char key[EVP_MAX_KEY_LENGTH] = "01234567890123456789012345678901";

    encryptAndDecrypt(input, output, &out_len);

    // Add your assertion here based on the expected output
    //CU_ASSERT_STRING_EQUAL(output, "openssl 编程");
    printf("Actual Decrypted Result: %s\n", output);

    // You can add more assertions as needed
}

int main() {
    // Initialize CUnit test registry
    if (CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    // Add a suite to the registry
    CU_pSuite suite = CU_add_suite("SuiteName", NULL, NULL);
    if (!suite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test function to the suite
    if (!CU_add_test(suite, "testEncryptAndDecrypt", testEncryptAndDecrypt)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // Clean up
    CU_cleanup_registry();

    return CU_get_error();
}

