#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void encryptAndDecrypt(const char *input, unsigned char *output, size_t *output_len) {
    int ret, inl, outl, total = 0, total2 = 0;
    char iv[16], de[500];

    EVP_CIPHER_CTX *ctx_encrypt = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX *ctx_decrypt = EVP_CIPHER_CTX_new();

    const EVP_CIPHER *type = EVP_aes_256_cbc();

    unsigned char key[EVP_MAX_KEY_LENGTH] = "01234567890123456789012345678901";  // 32 bytes for AES-256
    unsigned char *ek = malloc(EVP_CIPHER_key_length(type));
    
    if (RAND_bytes(ek, EVP_CIPHER_key_length(type)) <= 0) {
        fprintf(stderr, "Error generating random key\n");
        goto err;
    }

    if (RAND_bytes((unsigned char *)iv, EVP_CIPHER_iv_length(type)) <= 0) {
        fprintf(stderr, "Error generating random IV\n");
        goto err;
    }

    EVP_CIPHER_CTX_init(ctx_encrypt);
    ret = EVP_EncryptInit_ex(ctx_encrypt, type, NULL, ek, iv);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptInit_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    inl = strlen(input);

    ret = EVP_EncryptUpdate(ctx_encrypt, output, &outl, (unsigned char *)input, inl);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptUpdate failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    total += outl;

    ret = EVP_EncryptFinal_ex(ctx_encrypt, output + total, &outl);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptFinal_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    total += outl;

    memset(de, 0, 500);

    EVP_CIPHER_CTX_init(ctx_decrypt);
    ret = EVP_DecryptInit_ex(ctx_decrypt, type, NULL, ek, iv);
    if (ret != 1) {
        fprintf(stderr, "EVP_DecryptInit_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    ret = EVP_DecryptUpdate(ctx_decrypt, de, &outl, output, total);
    total2 += outl;

    ret = EVP_DecryptFinal_ex(ctx_decrypt, de + total2, &outl);
    total2 += outl;

    de[total2] = 0;

    printf("Decrypted: %s\n", de);

err:
    EVP_CIPHER_CTX_free(ctx_encrypt);
    EVP_CIPHER_CTX_free(ctx_decrypt);
    free(ek);
}
/*
int main() {
    char out[500];
    size_t out_len;

    encryptAndDecrypt("openssl 编程", out, &out_len);
    return 0;
}
*/
/*#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    int ret, inl, outl, total = 0, total2 = 0;
    char iv[16], in[100], out[500], de[500];

    EVP_CIPHER_CTX *ctx_encrypt = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX *ctx_decrypt = EVP_CIPHER_CTX_new();

    const EVP_CIPHER *type = EVP_aes_256_cbc();

    unsigned char key[EVP_MAX_KEY_LENGTH] = "01234567890123456789012345678901";  // 32 bytes for AES-256
    unsigned char *ek = malloc(EVP_CIPHER_key_length(type));
    
    if (RAND_bytes(ek, EVP_CIPHER_key_length(type)) <= 0) {
        fprintf(stderr, "Error generating random key\n");
        goto err;
    }

    if (RAND_bytes((unsigned char *)iv, EVP_CIPHER_iv_length(type)) <= 0) {
        fprintf(stderr, "Error generating random IV\n");
        goto err;
    }

    EVP_CIPHER_CTX_init(ctx_encrypt);
    ret = EVP_EncryptInit_ex(ctx_encrypt, type, NULL, ek, iv);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptInit_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    strcpy(in, "openssl 编程");
    inl = strlen(in);

    ret = EVP_EncryptUpdate(ctx_encrypt, out, &outl, (unsigned char *)in, inl);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptUpdate failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    total += outl;

    ret = EVP_EncryptFinal_ex(ctx_encrypt, out + total, &outl);
    if (ret != 1) {
        fprintf(stderr, "EVP_EncryptFinal_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    total += outl;

    memset(de, 0, 500);

    EVP_CIPHER_CTX_init(ctx_decrypt);
    ret = EVP_DecryptInit_ex(ctx_decrypt, type, NULL, ek, iv);
    if (ret != 1) {
        fprintf(stderr, "EVP_DecryptInit_ex failed\n");
        ERR_print_errors_fp(stderr);
        goto err;
    }

    ret = EVP_DecryptUpdate(ctx_decrypt, de, &outl, out, total);
    total2 += outl;

    ret = EVP_DecryptFinal_ex(ctx_decrypt, de + total2, &outl);
    total2 += outl;

    de[total2] = 0;

    printf("%s\n", de);

err:
    

    EVP_CIPHER_CTX_free(ctx_encrypt);
    EVP_CIPHER_CTX_free(ctx_decrypt);

    free(ek);

    return 0;
}
*/

/*#include <openssl/evp.h>
#include <openssl/rsa.h>
int main()
{
int ret,ekl[2],npubk,inl,outl,total=0,total2=0;
unsigned long e=RSA_3;
char *ek[2],iv[8],in[100],out[500],de[500];
EVP_CIPHER_CTX ctx,ctx2;
EVP_CIPHER *type;
EVP_PKEY *pubkey[2];
RSA *rkey;
BIGNUM *bne;
// 生成 RSA 密钥
bne=BN_new();
ret=BN_set_word(bne,e);
rkey=RSA_new();
ret=RSA_generate_key_ex(rkey,1024,bne,NULL);
pubkey[0]=EVP_PKEY_new();
EVP_PKEY_assign_RSA(pubkey[0],rkey);
type=EVP_des_cbc();
npubk=1;
EVP_CIPHER_CTX_init(&ctx);
ek[0]=malloc(500);
ek[1]=malloc(500);
ret=EVP_SealInit(&ctx,type,ek,ekl,iv,pubkey,1); // 只有一个公钥
if(ret!=1) goto err;
strcpy(in,"openssl 编程");
inl=strlen(in);
ret=EVP_SealUpdate(&ctx,out,&outl,in,inl);
if(ret!=1) goto err;
total+=outl;
ret=EVP_SealFinal(&ctx,out+outl,&outl);
if(ret!=1) goto err;
total+=outl;
memset(de,0,500);
EVP_CIPHER_CTX_init(&ctx2);
ret=EVP_OpenInit(&ctx2,EVP_des_cbc(),ek[0],ekl[0],iv,pubkey[0]);
if(ret!=1) goto err;
ret=EVP_OpenUpdate(&ctx2,de,&outl,out,total);
total2+=outl;
ret=EVP_OpenFinal(&ctx2,de+outl,&outl);
total2+=outl;
de[total2]=0;
printf("%s\n",de);
err:
free(ek[0]);
free(ek[1]);
EVP_PKEY_free(pubkey[0]);
BN_free(bne);
getchar();
return 0;
}
*/
