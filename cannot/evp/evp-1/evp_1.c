#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

int performEncryptionDecryption(const EVP_CIPHER *cipher, const unsigned char *key, const unsigned char *iv, const unsigned char *in, int inl, unsigned char *out, unsigned char *de)
{
    int ret;
    int len, outl, total = 0;
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();

    EVP_CIPHER_CTX_init(ctx);

    ret = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);
    if (ret != 1)
    {
        printf("EVP_EncryptInit_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    len = 0;
    EVP_EncryptUpdate(ctx, out + len, &outl, in, inl);
    len += outl;
    EVP_EncryptUpdate(ctx, out + len, &outl, in + 50, inl);
    len += outl;
    EVP_EncryptFinal_ex(ctx, out + len, &outl);
    len += outl;

    printf("加密结果长度：%d\n", len);

    EVP_CIPHER_CTX_reset(ctx);

    ret = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv);
    if (ret != 1)
    {
        printf("EVP_DecryptInit_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    total = 0;
    EVP_DecryptUpdate(ctx, de + total, &outl, out, 44);
    total += outl;
    EVP_DecryptUpdate(ctx, de + total, &outl, out + 44, len - 44);
    total += outl;
    ret = EVP_DecryptFinal_ex(ctx, de + total, &outl);
    total += outl;

    if (ret != 1)
    {
        EVP_CIPHER_CTX_reset(ctx);
        printf("EVP_DecryptFinal_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if ((total != 100) || (memcmp(de, in, 100) != 0))
    {
        printf("Error: Decryption failed!\n");
        EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    EVP_CIPHER_CTX_reset(ctx);
    EVP_CIPHER_CTX_free(ctx);

    printf("Test successful!\n");
    return 0;
}

/*int main()
{
    int which = 1;
    const EVP_CIPHER *cipher;
    unsigned char key[24], iv[8], in[100], out[108], de[100];
    int i, inl;

    for (i = 0; i < 24; i++)
    {
        memset(&key[i], i, 1);
    }
    for (i = 0; i < 8; i++)
    {
        memset(&iv[i], i, 1);
    }
    for (i = 0; i < 100; i++)
    {
        memset(&in[i], i, 1);
    }

    printf("Please select:\n");
    printf("1: EVP_des_ede3_ofb\n");
    printf("2: EVP_des_ede3_cbc\n");
    scanf("%d", &which);

    if (which == 1)
        cipher = EVP_des_ede3_ofb();
    else
        cipher = EVP_des_ede3_cbc();

    inl = 50;

    performEncryptionDecryption(cipher, key, iv, in, inl, out, de);

    return 0;
}*/
/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

int main()
{
    int ret, which = 1;
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    const EVP_CIPHER *cipher;
    unsigned char key[24], iv[8], in[100], out[108], de[100];
    int i, len, inl, outl, total = 0;

    for (i = 0; i < 24; i++)
    {
        memset(&key[i], i, 1);
    }
    for (i = 0; i < 8; i++)
    {
        memset(&iv[i], i, 1);
    }
    for (i = 0; i < 100; i++)
    {
        memset(&in[i], i, 1);
    }

    EVP_CIPHER_CTX_init(ctx);

    printf("Please select:\n");
    printf("1: EVP_des_ede3_ofb\n");
    printf("2: EVP_des_ede3_cbc\n");
    scanf("%d", &which);

    if (which == 1)
        cipher = EVP_des_ede3_ofb();
    else
        cipher = EVP_des_ede3_cbc();

    ret = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);
    if (ret != 1)
    {
        printf("EVP_EncryptInit_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    inl = 50;
    len = 0;
    EVP_EncryptUpdate(ctx, out + len, &outl, in, inl);
    len += outl;
    EVP_EncryptUpdate(ctx, out + len, &outl, in + 50, inl);
    len += outl;
    EVP_EncryptFinal_ex(ctx, out + len, &outl);
    len += outl;

    printf("加密结果长度：%d\n", len);

    EVP_CIPHER_CTX_reset(ctx);

    ret = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv);
    if (ret != 1)
    {
        printf("EVP_DecryptInit_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    total = 0;
    EVP_DecryptUpdate(ctx, de + total, &outl, out, 44);
    total += outl;
    EVP_DecryptUpdate(ctx, de + total, &outl, out + 44, len - 44);
    total += outl;
    ret = EVP_DecryptFinal_ex(ctx, de + total, &outl);
    total += outl;

    if (ret != 1)
    {
        EVP_CIPHER_CTX_reset(ctx);
        printf("EVP_DecryptFinal_ex error!\n");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    if ((total != 100) || (memcmp(de, in, 100) != 0))
    {
        printf("Error: Decryption failed!\n");
        EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    EVP_CIPHER_CTX_reset(ctx);
    EVP_CIPHER_CTX_free(ctx);

    printf("Test successful!\n");
    return 0;
}*/
/*#include <string.h>
#include <openssl/evp.h>
int main()
{
int ret,which=1;
EVP_CIPHER_CTX *ctx;
const EVP_CIPHER *cipher;
unsigned char key[24],iv[8],in[100],out[108],de[100];
int i,len,inl,outl,total=0;
for(i=0;i<24;i++)
{
memset(&key[i],i,1);
}
for(i=0;i<8;i++)
{
memset(&iv[i],i,1);
}
for(i=0;i<100;i++)
{
memset(&in[i],i,1);
}
EVP_CIPHER_CTX_init(ctx);
printf("please select :\n");
printf("1: EVP_des_ede3_ofb\n");
printf("2: EVP_des_ede3_cbc\n");
scanf("%d",&which);
if(which==1)
cipher=EVP_des_ede3_ofb();
else
cipher=EVP_des_ede3_cbc(); 
ret=EVP_EncryptInit_ex(ctx,cipher,NULL,key,iv);
if(ret!=1)
{
printf("EVP_EncryptInit_ex err1!\n");
return -1;
}
inl=50;
len=0;
EVP_EncryptUpdate(ctx,out+len,&outl,in,inl);
len+=outl;
EVP_EncryptUpdate(ctx,out+len,&outl,in+50,inl);
len+=outl;
EVP_EncryptFinal_ex(ctx,out+len,&outl);
len+=outl;
printf("加密结果长度：%d\n",len);
// 解密 
EVP_CIPHER_CTX_cleanup(ctx);
EVP_CIPHER_CTX_init(ctx);
ret=EVP_DecryptInit_ex(ctx,cipher,NULL,key,iv);
if(ret!=1)
{
printf("EVP_DecryptInit_ex err1!\n");
return -1;
}
total=0;
EVP_DecryptUpdate(ctx,de+total,&outl,out,44);
total+=outl;
EVP_DecryptUpdate(ctx,de+total,&outl,out+44,len-44);
total+=outl;
ret=EVP_DecryptFinal_ex(ctx,de+total,&outl);
total+=outl;
if(ret!=1)
{
EVP_CIPHER_CTX_cleanup(ctx);
printf("EVP_DecryptFinal_ex err\n");
return -1;
}
if((total!=100) || (memcmp(de,in,100)))
{
printf("err!\n");
return -1;
}
EVP_CIPHER_CTX_cleanup(ctx);
printf("test ok!\n");
return 0;
}
*/
