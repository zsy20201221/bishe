#include <CUnit/Basic.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

void test_performEncryptionDecryption()
{
    const EVP_CIPHER *cipher = EVP_des_ede3_ofb();
    unsigned char key[24], iv[8], in[100], out[108], de[100];
    int i, inl;

    for (i = 0; i < 24; i++)
    {
        memset(&key[i], i, 1);
    }
    for (i = 0; i < 8; i++)
    {
        memset(&iv[i], i, 1);
    }
    for (i = 0; i < 100; i++)
    {
        memset(&in[i], i, 1);
    }

    inl = 50;

    int result = performEncryptionDecryption(cipher, key, iv, in, inl, out, de);

    CU_ASSERT_EQUAL(result, 0);
}

void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("PerformEncryptionDecryptionTests", NULL, NULL);
    CU_add_test(suite, "test_performEncryptionDecryption", test_performEncryptionDecryption);
}

int main()
{
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    add_test_suite();

    CU_basic_run_tests();

    CU_cleanup_registry();

    return CU_get_error();
}
