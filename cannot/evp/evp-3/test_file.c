#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>

int verifySignature(const char *data, const EVP_MD *md, const EVP_PKEY *pkey);

void testVerifySignature()
{
    const char *data = "20201221zsy";
    EVP_PKEY *pkey;
    RSA *rkey;
    BIGNUM *bne;

    // 生成 RSA 密钥
    bne = BN_new();
    BN_set_word(bne, RSA_F4);
    rkey = RSA_new();
    RSA_generate_key_ex(rkey, 1024, bne, NULL);

    pkey = EVP_PKEY_new();
    EVP_PKEY_assign_RSA(pkey, rkey);

    // 验证签名
    int ret = verifySignature(data, EVP_md5(), pkey);

    CU_ASSERT_EQUAL(ret, 1);

    EVP_PKEY_free(pkey);
    BN_free(bne);
}

int main()
{
    // 初始化 CUnit
    CU_initialize_registry();

    // 创建一个测试套件
    CU_pSuite suite = CU_add_suite("Suite", NULL, NULL);

    // 添加测试用例到测试套件
    CU_add_test(suite, "testVerifySignature", testVerifySignature);

    // 运行所有的测试
    CU_basic_run_tests();

    // 清理 CUnit
    CU_cleanup_registry();

    return 0;
}
