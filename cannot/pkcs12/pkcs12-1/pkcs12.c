#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <stdio.h>
#include <stdlib.h>

int generatePKCS12(const char* certFilePath, const char* keyFilePath, const char* outputFilePath) {
    int len;
    PKCS12 *p12 = NULL;
    FILE *cert_fp, *key_fp;
    unsigned char *buf = NULL, *p;
    X509 *cert = NULL;
    EVP_PKEY *pkey = NULL;
    RSA *rsa = NULL;

    OpenSSL_add_all_algorithms();

    cert_fp = fopen(certFilePath, "rb");
    if (!cert_fp) {
        perror("Error opening certificate file");
        return 1;
    }
    
    cert = PEM_read_X509(cert_fp, NULL, NULL, NULL);

    if (!cert) {
        fprintf(stderr, "Error reading X.509 certificate\n");
        fclose(cert_fp);
        return 1;
    }

    key_fp = fopen(keyFilePath, "rb");
    if (!key_fp) {
        perror("Error opening private key file");
        X509_free(cert);
        fclose(cert_fp);
        return 1;
    }

    rsa = PEM_read_RSAPrivateKey(key_fp, NULL, NULL, NULL);
    fclose(key_fp);

    if (!rsa) {
        fprintf(stderr, "Error reading RSA private key\n");
        X509_free(cert);
        fclose(cert_fp);
        return 1;
    }

    pkey = EVP_PKEY_new();
    if (!pkey) {
        fprintf(stderr, "Error creating EVP_PKEY structure\n");
        X509_free(cert);
        RSA_free(rsa);
        return 1;
    }

    if (EVP_PKEY_assign_RSA(pkey, rsa) != 1) {
        fprintf(stderr, "Error assigning RSA private key to EVP_PKEY\n");
        X509_free(cert);
        EVP_PKEY_free(pkey);
        return 1;
    }

    p12 = PKCS12_create(
        "password",
        "friendlyName",
        pkey,
        cert,
        NULL,
        NID_pbe_WithSHA1And3_Key_TripleDES_CBC,
        NID_pbe_WithSHA1And3_Key_TripleDES_CBC,
        PKCS12_DEFAULT_ITER,
        -1,
        0
    );

    if (!p12) {
        fprintf(stderr, "Error creating PKCS#12 structure\n");
        ERR_print_errors_fp(stderr);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    len = i2d_PKCS12(p12, NULL);
    if (len <= 0) {
        fprintf(stderr, "Error calculating DER encoding length\n");
        ERR_print_errors_fp(stderr);
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    buf = p = malloc(len);
    if (!buf) {
        fprintf(stderr, "Error allocating memory for DER encoding\n");
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    len = i2d_PKCS12(p12, &p);

    FILE *out_fp = fopen(outputFilePath, "wb");
    if (!out_fp) {
        perror("Error opening output file");
        free(buf);
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    fwrite(buf, 1, len, out_fp);
    fclose(out_fp);

    free(buf);
    PKCS12_free(p12);
    EVP_PKEY_free(pkey);
    X509_free(cert);

    printf("OK\n");

    return 0;
}

/*int main() {
    const char* certFilePath = "time.cer";
    const char* keyFilePath = "prikey.pem";
    const char* outputFilePath = "myp12.pfx";

    int result = generatePKCS12(certFilePath, keyFilePath, outputFilePath);

    if (result != 0) {
        fprintf(stderr, "PKCS12 generation failed\n");
        return 1;
    }

    return 0;
}*/
/*#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/x509.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    int len;
    PKCS12 *p12 = NULL;
    FILE *cert_fp, *key_fp;  // 不同的文件指针
    unsigned char *buf = NULL, *p, tmp[5000];
    X509 *cert = NULL;
    EVP_PKEY *pkey = NULL;
    RSA *rsa = NULL;

    OpenSSL_add_all_algorithms();

    // 使用 PEM_read_X509 函数从文件中读取 X.509 证书
    cert_fp = fopen("/home/linux/keshe/cannot/pkcs12/time.cer", "rb");
    if (!cert_fp) {
        perror("Error opening certificate file");
        return 1;
    }
    
    cert = PEM_read_X509(cert_fp, NULL, NULL, NULL);

    if (!cert) {
        fprintf(stderr, "Error reading X.509 certificate\n");
        fclose(cert_fp);
        return 1;
    }

    // 使用 PEM_read_RSAPrivateKey 函数读取 RSA 私钥
    key_fp = fopen("prikey.pem", "rb");  // 请替换为您的私钥文件路径
    if (!key_fp) {
        perror("Error opening private key file");
        X509_free(cert);
        fclose(cert_fp);
        return 1;
    }

    rsa = PEM_read_RSAPrivateKey(key_fp, NULL, NULL, NULL);
    fclose(key_fp);

    if (!rsa) {
        fprintf(stderr, "Error reading RSA private key\n");
        X509_free(cert);
        fclose(cert_fp);
        return 1;
    }

    pkey = EVP_PKEY_new();
    if (!pkey) {
        fprintf(stderr, "Error creating EVP_PKEY structure\n");
        X509_free(cert);
        RSA_free(rsa);
        return 1;
    }

    // 将 RSA 密钥设置到 EVP_PKEY 结构中
    if (EVP_PKEY_assign_RSA(pkey, rsa) != 1) {
        fprintf(stderr, "Error assigning RSA private key to EVP_PKEY\n");
        X509_free(cert);
        EVP_PKEY_free(pkey);
        return 1;
    }

    // 创建 PKCS12 结构
    p12 = PKCS12_create(
    "password",
    "friendlyName",
    pkey,
    cert,
    NULL,
    NID_pbe_WithSHA1And3_Key_TripleDES_CBC,  // 更改为支持的算法，例如 NID_pbe_WithSHA1And3_Key_AES128_CBC
    NID_pbe_WithSHA1And3_Key_TripleDES_CBC,  
    PKCS12_DEFAULT_ITER,
    -1,
    0);

    if (!p12) {
    fprintf(stderr, "Error creating PKCS#12 structure\n");
    ERR_print_errors_fp(stderr);  // 输出详细错误信息
    EVP_PKEY_free(pkey);
    X509_free(cert);
    return 1;
}


    // 计算 DER 编码的长度
    len = i2d_PKCS12(p12, NULL);
    if (len <= 0) {
        fprintf(stderr, "Error calculating DER encoding length\n");
        ERR_print_errors_fp(stderr);
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    // 分配足够的内存
    buf = p = malloc(len);
    if (!buf) {
        fprintf(stderr, "Error allocating memory for DER encoding\n");
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    // 实际进行 DER 编码
    len = i2d_PKCS12(p12, &p);

    // 保存 PKCS#12 结果到文件
    FILE *out_fp = fopen("myp12.pfx", "wb");
    if (!out_fp) {
        perror("Error opening output file");
        free(buf);
        PKCS12_free(p12);
        EVP_PKEY_free(pkey);
        X509_free(cert);
        return 1;
    }

    fwrite(buf, 1, len, out_fp);
    fclose(out_fp);

    // 释放资源
    free(buf);
    PKCS12_free(p12);
    EVP_PKEY_free(pkey);
    X509_free(cert);

    printf("ok\n");

    return 0;
}
*/

/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/asn1.h>

int main() {
    int ret, len;
    PKCS12 *p12;
    PKCS7 *p7;
    STACK_OF(PKCS7) *safes;
    FILE *fp;
    unsigned char *buf, *p, tmp[5000];
    X509 *cert = NULL;
    EVP_PKEY *pkey = NULL;

    OpenSSL_add_all_algorithms();

    // Set a password for the PKCS12 structure
    char password[] = "password";

    // Load certificate from file
    fp = fopen("time1.cer", "rb");
    if (!fp) {
        fprintf(stderr, "Error: Failed to open certificate file\n");
        return -1;
    }

    len = fread(tmp, 1, 5000, fp);
    fclose(fp);

    // 打印读取的证书数据（用于调试）
    printf("Read %d bytes from certificate file:\n", len);
    for (int i = 0; i < len; ++i) {
        printf("%02X ", tmp[i]);
    }
    printf("\n");

    p = tmp;
    cert = d2i_X509(NULL, &p, len);



// 检查是否成功加载证书
if (!cert) {
    fprintf(stderr, "Error: Failed to parse X509 certificate\n");
    return -1;
}



    // Load private key from file
    fp = fopen("prikey.cer", "rb");
    if (!fp) {
        fprintf(stderr, "Error: Failed to open private key file\n");
        X509_free(cert);
        return -1;
    }

    len = fread(tmp, 1, 5000, fp);
    fclose(fp);
    p = tmp;
    pkey = d2i_PrivateKey(EVP_PKEY_RSA, NULL, &p, len);

    // 检查是否成功加载私钥
    if (!pkey) {
        fprintf(stderr, "Error: Failed to parse private key\n");
        X509_free(cert);
        return -1;
    }

    // 创建 PKCS12 结构
    p12 = PKCS12_create(password, "My PKCS12 Friendly Name", pkey, cert, NULL, 0, 0, 0, 0, 0);

    // 检查是否成功创建 PKCS12 结构
    if (!p12) {
        fprintf(stderr, "Error: Failed to create PKCS12 structure\n");
        X509_free(cert);
        EVP_PKEY_free(pkey);
        return -1;
    }

    // 确定 PKCS12 结构的大小
    len = i2d_PKCS12(p12, NULL);

    // 分配内存用于 PKCS12 结构
    buf = p = malloc(len);

    // 编码 PKCS12 结构
    len = i2d_PKCS12(p12, &p);

    // 写入 PKCS12 结构到文件
    fp = fopen("myp12.pfx", "wb");
    fwrite(buf, 1, len, fp);
    fclose(fp);

    printf("ok\n");

    // 清理资源
    PKCS12_free(p12);
    EVP_PKEY_free(pkey);
    X509_free(cert);
    free(buf);

    return 0;
}

*/
/*
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
int main()
{
int ret,len,key_usage,iter,key_nid;
PKCS12 *p12;
PKCS7 *p7;
STACK_OF(PKCS7) *safes;
STACK_OF(PKCS12_SAFEBAG) *bags;
PKCS12_SAFEBAG *bag;
FILE *fp;
unsigned char *buf,*p,tmp[5000];
X509 *cert=NULL;
EVP_PKEY *pkey=NULL;
OpenSSL_add_all_algorithms();
p12=PKCS12_init(NID_pkcs7_data);

//p12->mac=PKCS12_MAC_DATA_new();
//p12->mac->dinfo->algor->algorithm=OBJ_nid2obj(NID_sha1);
//ASN1_STRING_set(p12->mac->dinfo->digest,"aaa",3);
//ASN1_STRING_set(p12->mac->salt,"test",4);
//p12->mac->iter=ASN1_INTEGER_new();
//ASN1_INTEGER_set(p12->mac->iter,3);

// pkcs7 
bags=sk_PKCS12_SAFEBAG_new_null();
fp=fopen("time.cer","rb");
len=fread(tmp,1,5000,fp);
fclose(fp);
p=tmp;
// cert 
d2i_X509(&cert,&p,len);
bag=PKCS12_x5092certbag(cert);
sk_PKCS12_SAFEBAG_push(bags,bag);
// private key 
fp=fopen("prikey.cer","rb");
len=fread(tmp,1,5000,fp);
fclose(fp);
p=tmp;
pkey=d2i_PrivateKey(EVP_PKEY_RSA,NULL,&p,len);
PKCS12_add_key(&bags,pkey,KEY_EX,PKCS12_DEFAULT_ITER,NID_pbe_WithSHA1
And3_Key_TripleDES_CBC,"openssl");
p7=PKCS12_pack_p7data(bags);
safes=sk_PKCS7_new_null();
sk_PKCS7_push(safes,p7);
ret=PKCS12_pack_authsafes(p12,safes);
len=i2d_PKCS12(p12,NULL);
buf=p=malloc(len);
len=i2d_PKCS12(p12,&p);
fp=fopen("myp12.pfx","wb");
fwrite(buf,1,len,fp);
fclose(fp);
printf("ok\n");
return 0;
}
*/
