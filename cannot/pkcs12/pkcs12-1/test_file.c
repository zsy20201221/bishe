#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdio.h>
#include <stdlib.h>


void test_generatePKCS12() {
    // 为测试准备输入文件路径
    const char* certFilePath = "time.cer";
    const char* keyFilePath = "prikey.pem";
    const char* outputFilePath = "test_output.p12";

    // 运行被测试的函数
    int result = generatePKCS12(certFilePath, keyFilePath, outputFilePath);

    // 添加断言，验证函数的返回值是否为 0，表示成功
    CU_ASSERT_EQUAL(result, 0);

    // 添加更多断言，根据实际情况验证生成的 PKCS#12 文件内容是否符合预期
    // 这可能涉及到读取和解析生成的 PKCS#12 文件的内容，以确保其中包含了正确的数据

    // 删除生成的测试文件
    remove(outputFilePath);

    // 如果函数输出的结果是通过测试的，CU_PASS 可以用于表示测试通过
    CU_PASS("test_generatePKCS12 passed");
}

int main() {
    CU_initialize_registry();
    CU_pSuite suite = CU_add_suite("Suite", NULL, NULL);
    if (suite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (CU_add_test(suite, "test_generatePKCS12", test_generatePKCS12) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE | CU_BRM_SILENT);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}

