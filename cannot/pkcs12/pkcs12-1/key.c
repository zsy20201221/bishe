#include <openssl/rsa.h>
#include <openssl/pem.h>

void generatePrivateKey(const char *filename) {
    RSA *rsa = RSA_new();
    BIGNUM *e = BN_new();
    EVP_PKEY *pkey = EVP_PKEY_new();

    // Set public exponent (65537 in most cases)
    BN_set_word(e, RSA_F4);

    // Generate RSA key pair
    RSA_generate_key_ex(rsa, 2048, e, NULL);

    // Set RSA key pair to EVP_PKEY structure
    EVP_PKEY_assign_RSA(pkey, rsa);

    // Write private key to file
    FILE *fp = fopen(filename, "wb");
    if (fp) {
        PEM_write_PrivateKey(fp, pkey, NULL, NULL, 0, NULL, NULL);
        fclose(fp);
    } else {
        perror("Unable to write private key to file");
    }

    // Cleanup
    EVP_PKEY_free(pkey);
    BN_free(e);
}

int main() {
    // Generate private key and save to file
    generatePrivateKey("prikey.cer");

    // Rest of your PKCS12 generation code...

    return 0;
}

