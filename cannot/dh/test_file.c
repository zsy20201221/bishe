#include <CUnit/Basic.h>
#include <openssl/dh.h>
#include <openssl/bn.h>
#include <openssl/err.h>
void test_generate_dh_params()
{
    // 最基本的测试示例，总是通过
    CU_ASSERT_TRUE(1);
}

void add_test_suite()
{
    CU_pSuite suite = CU_add_suite("SampleTests", NULL, NULL);
    CU_add_test(suite, "test_sample", test_generate_dh_params);
}

int main()
{
    if (CU_initialize_registry() != CUE_SUCCESS)
    {
        return CU_get_error();
    }

    add_test_suite();

    CU_basic_run_tests();

    CU_cleanup_registry();

    return CU_get_error();
}
