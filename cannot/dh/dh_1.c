#include <stdio.h>
#include <openssl/dh.h>
#include <openssl/bn.h>
#include <openssl/err.h>

DH* generate_dh_params() {
    DH* dh = DH_new();
    if (!dh) {
        perror("DH_new");
        return NULL;
    }

    if (!DH_generate_parameters_ex(dh, 2048, DH_GENERATOR_2, NULL)) {
        perror("DH_generate_parameters_ex");
        DH_free(dh);
        return NULL;
    }

    // Output DH parameters for debugging
    printf("DH Parameters:\n");
    DHparams_print_fp(stdout, dh);

    printf("DH Parameters after check:\n");
    DHparams_print_fp(stdout, dh);

    return dh;
}

int generate_shared_key(DH* d1) {
    DH* d2 = DH_new();
    if (!d2) {
        perror("DH_new");
        DH_free(d1);
        return -1;
    }

    const BIGNUM* p, * q, * g;
    DH_get0_pqg(d1, &p, &q, &g);

    if (!DH_set0_pqg(d2, BN_dup(p), BN_dup(q), BN_dup(g))) {
        perror("DH_set0_pqg");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    if (!DH_generate_key(d1)) {
        perror("DH_generate_key (d1)");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    if (!DH_generate_key(d2)) {
        perror("DH_generate_key (d2)");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    const BIGNUM* pub_key1, * pub_key2;
    DH_get0_key(d1, &pub_key1, NULL);
    DH_get0_key(d2, &pub_key2, NULL);

    char sharekey1[DH_size(d1)], sharekey2[DH_size(d2)];
    int len1, len2;

    len1 = DH_compute_key_padded(sharekey1, pub_key2, d1);
    len2 = DH_compute_key_padded(sharekey2, pub_key1, d2);

    if (len1 != len2 || memcmp(sharekey1, sharekey2, len1) != 0) {
        perror("生成共享密钥失败");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    printf("生成共享密钥成功\n");

    BIO* b = BIO_new(BIO_s_file());
    BIO_set_fp(b, stdout, BIO_NOCLOSE);

    // 打印密钥
    DHparams_print(b, d1);

    BIO_free(b);
    DH_free(d1);
    DH_free(d2);

    return 0;
}

/*int main() {
    DH* d1 = generate_dh_params();
    if (!d1) {
        return -1;
    }

    int result = generate_shared_key(d1);

    return result;
}*/
/*#include <stdio.h>
#include <openssl/dh.h>
#include <openssl/bn.h>
#include <openssl/err.h>

DH *generate_dh_params() {
    DH *dh = DH_new();
    if (!dh) {
        perror("DH_new");
        return NULL;
    }

    if (!DH_generate_parameters_ex(dh, 2048, DH_GENERATOR_2, NULL)) {
        perror("DH_generate_parameters_ex");
        DH_free(dh);
        return NULL;
    }

    // Output DH parameters for debugging
    printf("DH Parameters:\n");
    DHparams_print_fp(stdout, dh);

    

    printf("DH Parameters after check:\n");
    DHparams_print_fp(stdout, dh);

    return dh;
}

int main() {
    DH *d1 = generate_dh_params();
    if (!d1) {
        return -1;
    }

    DH *d2 = DH_new();
    if (!d2) {
        perror("DH_new");
        DH_free(d1);
        return -1;
    }

    const BIGNUM *p, *q, *g;
    DH_get0_pqg(d1, &p, &q, &g);

    if (!DH_set0_pqg(d2, BN_dup(p), BN_dup(q), BN_dup(g))) {
        perror("DH_set0_pqg");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    if (!DH_generate_key(d1)) {
        perror("DH_generate_key (d1)");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    if (!DH_generate_key(d2)) {
        perror("DH_generate_key (d2)");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    const BIGNUM *pub_key1, *pub_key2;
    DH_get0_key(d1, &pub_key1, NULL);
    DH_get0_key(d2, &pub_key2, NULL);

    char sharekey1[DH_size(d1)], sharekey2[DH_size(d2)];
    int len1, len2;

    len1 = DH_compute_key_padded(sharekey1, pub_key2, d1);
    len2 = DH_compute_key_padded(sharekey2, pub_key1, d2);

    if (len1 != len2 || memcmp(sharekey1, sharekey2, len1) != 0) {
        perror("生成共享密钥失败");
        DH_free(d1);
        DH_free(d2);
        return -1;
    }

    printf("生成共享密钥成功\n");

    BIO *b = BIO_new(BIO_s_file());
    BIO_set_fp(b, stdout, BIO_NOCLOSE);

    // 打印密钥 
    DHparams_print(b, d1);

    BIO_free(b);
    DH_free(d1);
    DH_free(d2);

    return 0;
}
*/
/*#include <openssl/dh.h>
#include <memory.h>
int main()
{
	DH* d1, * d2;
	BIO* b;
	int ret, size, i, len1, len2;
	char sharekey1[128], sharekey2[128];
	// 构造 DH 数据结构 
	d1 = DH_new();
	d2 = DH_new();
	// 生成 d1 的密钥参数，该密钥参数是可以公开的 
	ret = DH_generate_parameters_ex(d1, 64, DH_GENERATOR_2, NULL);
	if (ret != 1)
	{
		printf("DH_generate_parameters_ex err!\n");
		return -1;
	}
	// 检查密钥参数 
	ret = DH_check(d1, &i);
	if (ret != 1)
	{
		printf("DH_check err!\n");
		if (i & DH_CHECK_P_NOT_PRIME)
			printf("p value is not prime\n");
		if (i & DH_CHECK_P_NOT_SAFE_PRIME)
			printf("p value is not a safe prime\n");
		if (i & DH_UNABLE_TO_CHECK_GENERATOR)
			printf("unable to check the generator value\n");
		if (i & DH_NOT_SUITABLE_GENERATOR)
			printf("the g value is not a generator\n");
	}
	printf("DH parameters appear to be ok.\n");
	// 密钥大小 
	size = DH_size(d1);
	printf("DH key1 size : %d\n", size);
	// 生成公私钥 
	ret = DH_generate_key(d1);
	if (ret != 1) {
		printf("DH_generate_key err!\n");
		return -1;
	}
	// p
	和
	g 为公开的密钥参数，因此可以拷贝 
	d2->p = BN_dup(d1->p);
	d2->g = BN_dup(d1->g);
	// 生成公私钥
	,用于测试生成共享密钥 
	ret = DH_generate_key(d2);
	if (ret != 1) {
		printf("DH_generate_key err!\n");
		return -1;
	}
	// 检查公钥 
	ret = DH_check_pub_key(d1, d1->pub_key, &i);
	if (ret != 1) {
		if (i & DH_CHECK_PUBKEY_TOO_SMALL)
			printf("pub key too small \n");
		if (i & DH_CHECK_PUBKEY_TOO_LARGE)
			printf("pub key too large \n");
	}
	// 计算共享密钥 
	len1 = DH_compute_key(sharekey1, d2->pub_key, d1);
	len2 = DH_compute_key(sharekey2, d1->pub_key, d2);
	if (len1 != len2) {
		printf("生成共享密钥失败1\n");
		return -1;
	}
	if (memcmp(sharekey1, sharekey2, len1) != 0) {
		printf("生成共享密钥失败2\n");
		return -1;
	}
	printf("生成共享密钥成功\n");
	b = BIO_new(BIO_s_file());
	BIO_set_fp(b, stdout, BIO_NOCLOSE);
	// 打印密钥 
	DHparams_print(b, d1);
	BIO_free(b);
	DH_free(d1);
	DH_free(d2);
	return 0;
}*/
